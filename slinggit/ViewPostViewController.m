//
//  ViewPostViewController.m
//  slinggit
//
//  Created by Phil Beadle on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewPostViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SVProgressHUD.h"
#import "Constants.h"
#import "Utilities.h"
#import "PullToRefreshView.h"
#import "AFImageRequestOperation.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"
#import "KeychainWrapper.h"
#import "SendMessageViewController.h"
#import "TransparentToolbar.h"

#define CELL_IMAGE_TAG 10
#define CELL_TITLE_TAG 11
#define CELL_SUBTITLE_TAG 12
#define CELL_PRICE_TAG 13
#define CELL_DATE_TAG 14
#define CELL_COMMENT_ICON_TAG 15

#define TAG_LOADING_ACTIVITY_INDICATOR 16
#define TAG_LOADING_LABEL 17

#define TAG_OPTIONS_NOT_POST_OWNER_ACTION_SHEET 1
#define TAG_CLOSE_POST_ACTION_SHEET 2
#define TAG_FLAG_INAPPROPRIATE_ACTIONSHEET 3

#define kShowMoreCellHeight 50

#define kAddCommentPlaceholderText @"Add a comment"

@interface ViewPostViewController (){
    UITableView *commentsTableView;
    NSMutableArray *sourceData;
    float headerViewHeight;
    UIImageView *photoButtonBackground;
    UIButton *photoButton;
    UIFont *subTitleFont;
    UIActivityIndicatorView *mediumThumbnailLoadingSpinner;
    BOOL mediumThumbnailIsLoaded;
    PullToRefreshView *pullRefreshView;
    BOOL shouldShowMoreButton;
    //UITextField *addCommentTextField;
    UISearchBar *addCommentSearchBar;
}

@end

@implementation ViewPostViewController

@synthesize shouldShowWatchPostButton;
@synthesize postDictionary;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    [super loadView];
    
    subTitleFont = [UIFont systemFontOfSize:13.0];
    mediumThumbnailIsLoaded = NO;
    
    sourceData = [[NSMutableArray alloc] init];
    headerViewHeight = 160.0;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.5];
    [view addSubview:backgroundImageView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    [singleTap setDelegate:self];
    [singleTap setCancelsTouchesInView:NO];
    [view addGestureRecognizer:singleTap];
    
    commentsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight-44) style:UITableViewStyleGrouped];
    [commentsTableView setDelegate:self];
	[commentsTableView setDataSource:self];
    [commentsTableView setBackgroundColor:[UIColor clearColor]];
    pullRefreshView = [[PullToRefreshView alloc] initWithScrollView:(UIScrollView *)commentsTableView];
    [pullRefreshView setDelegate:self];
    [commentsTableView addSubview:pullRefreshView];
    
	[view addSubview:commentsTableView];
    
    mediumThumbnailLoadingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [mediumThumbnailLoadingSpinner setFrame:CGRectMake(130, 120, 60, 60)];
	[view addSubview:mediumThumbnailLoadingSpinner];
    
    addCommentSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight-44, kScreenWidth, 44)];
    //[addCommentSearchBar setTintColor:COLOR_SLINGGIT_BLUE];
    [addCommentSearchBar setPlaceholder:@"Add a comment"];
    [addCommentSearchBar setDelegate:self];
    [addCommentSearchBar setBarStyle:UIBarStyleBlackTranslucent];
    //[addCommentSearchBar setTranslucent:YES];
    [addCommentSearchBar setImage:[UIImage imageNamed:@"comment_icon.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    for (UIView *searchBarSubview in [addCommentSearchBar subviews]) {
        
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            
            @try {
                
                [(UITextField *)searchBarSubview setReturnKeyType:UIReturnKeySend];
                [(UITextField *)searchBarSubview setKeyboardAppearance:UIKeyboardAppearanceAlert];
            }
            @catch (NSException * e) {
                
                // ignore exception
            }
        }
    }
    [view addSubview:addCommentSearchBar];
    
    self.view = view;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:[postDictionary objectForKey:kJSONHashTagPrefixKeyName]];
    
    if([[postDictionary objectForKey:kJSONUserIdKeyName] intValue] != [[KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USER_ID] intValue]){
        //UIBarButtonItem *flagInappropriateButton = [[UIBarButtonItem alloc] initWithTitle:@"❕" style:UIBarButtonItemStyleBordered target:self action:@selector(flagInappropriateButtonPressed)];
        
        UIBarButtonItem *flagInappropriateButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"flag.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(flagInappropriateButtonPressed)];
        [self.navigationItem setRightBarButtonItem:flagInappropriateButton];
    }
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] init];
    [backButton setTitle:@"Back"];
    [self.navigationItem setBackBarButtonItem:backButton];
    
    /*
     if([[postDictionary objectForKey:kJSONUserIdKeyName] intValue] != [[KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USER_ID] intValue]){
     TransparentToolbar *watchToolbar = [[TransparentToolbar alloc] initWithFrame:CGRectMake(0, -3, 50, 44)];
     UIBarButtonItem *watchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"watch_icon.png"]  style:UIBarButtonItemStyleBordered target:self action:@selector(watchButtonPressed)];
     [watchButton setTintColor:COLOR_SLINGGIT_BLUE];
     [watchToolbar setItems:[NSArray arrayWithObject:watchButton]];
     
     [self.navigationItem setTitleView:watchToolbar];
     }
     */
    
    [self loadTableDataFromNewestComment];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - My Methods

-(void)closeAddCommentKeyboard{
    if([addCommentSearchBar isFirstResponder]){
        [addCommentSearchBar resignFirstResponder];
        [self addCommentTextFieldAnimateDown];
    }
}

-(void)addCommentTextFieldAnimateUp{
    
    [UIView animateWithDuration:0.25 animations:^{
        if(kLoggingEnabled)NSLog(@"headerview height: %f", headerViewHeight);
        //[self.view setFrame:CGRectMake(0, -headerViewHeight+190, kScreenWidth, kScreenHeightNoStatusBar)];
        [addCommentSearchBar setFrame:CGRectMake(0, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight-44-167, kScreenWidth, 44)];
        [commentsTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }];
    
}

-(void)addCommentTextFieldAnimateDown{
    
    [UIView animateWithDuration:0.25 animations:^{
        //[self.view setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
        [addCommentSearchBar setFrame:CGRectMake(0, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight-44, kScreenWidth, 44)];
    }];
    
}

-(void)stopLoadingAnimation{
    [pullRefreshView finishedLoading]; 
}

-(void)loadTableDataFromNewestComment{
    [sourceData removeAllObjects]; 
    [self getPostCommentsWithStartingCommentId:@"" withLimit:[NSNumber numberWithInt:kCommentShowLimit]];
}

-(void)getPostCommentsWithStartingCommentId:startingCommentId withLimit:limit{
    
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    NSURL *editedGetPostsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@&%@=%@", kGetPostCommentsURL, [Utilities commonParams], kJSONPostId, postId, kJSONLimitKeyName, limit, kJSONStartingCommentId, startingCommentId]];
    
    if(kLoggingEnabled)NSLog(@"get post comments URL: %@", editedGetPostsUrl);
    NSMutableURLRequest *getPostsRequest = [NSMutableURLRequest requestWithURL:editedGetPostsUrl];
    [getPostsRequest setTimeoutInterval:kUrlRequestTimeout];
    [getPostsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getPostsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getPostsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"get post comments response: %@", JSON);
        [self stopLoadingAnimation];
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
            int rowsFound = [[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue];
            
            if(rowsFound > 0){
                [sourceData addObjectsFromArray:[resultDictionary objectForKey:kJSONCommentsKeyName]];
                //if(kLoggingEnabled)NSLog(@"new source data was added: %@", sourceData);
                if(rowsFound < kFeedShowLimit){
                    shouldShowMoreButton = NO;
                }else{
                    shouldShowMoreButton = YES;
                }
                
            }else{
                shouldShowMoreButton = NO;
            }
            
            [commentsTableView reloadData];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            shouldShowMoreButton = NO;
            [commentsTableView reloadData];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kGeneralProblemErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            shouldShowMoreButton = NO;
            [commentsTableView reloadData];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [self stopLoadingAnimation];
        if(kLoggingEnabled)NSLog(@"reload table connection failed: %@", error);
        shouldShowMoreButton = NO;
        [commentsTableView reloadData];
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [getPostsOperation start];
}

-(void)addCommentAlertView{
    UIAlertView *addCommentAlertView = [[UIAlertView alloc] initWithTitle:@"Comment" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [addCommentAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[addCommentAlertView textFieldAtIndex:0] setDelegate:self];
    [addCommentAlertView show];
}

-(void)addComment:(NSString *)comment{
    if(kLoggingEnabled)NSLog(@"comment: %@", comment);
    [addCommentSearchBar setText:@""];
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    NSURL *editedAddCommentUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@", kCreatePostCommentURL, [Utilities commonParams], kJSONPostId, postId, kJSONCommentBodyKeyName, [Utilities stringByEncodingHTMLEntities:comment]]];
    
    if(kLoggingEnabled)NSLog(@"add comment URL: %@", editedAddCommentUrl);
    NSMutableURLRequest *getPostsRequest = [NSMutableURLRequest requestWithURL:editedAddCommentUrl];
    [getPostsRequest setTimeoutInterval:kUrlRequestTimeout];
    [getPostsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *addCommentOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getPostsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"add comment response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
            
            
            [self loadTableDataFromNewestComment];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kAddCommentErrorTitle message:[resultDictionary objectForKey:kJSONFriendlyError] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
            
            
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(kLoggingEnabled)NSLog(@"add comment connection failed: %@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [addCommentOperation start];
}

-(void)deleteCommentForRow:(NSNumber *)row{
    
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    NSURL *editedDeleteCommentUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@", kDeletePostCommentURL, [Utilities commonParams], kJSONPostId, postId, kJSONCommentIdKeyName, [[sourceData objectAtIndex:[row intValue]] objectForKey:kJSONIdKeyName]]];
    
    if(kLoggingEnabled)NSLog(@"delete comment URL: %@", editedDeleteCommentUrl);
    NSMutableURLRequest *deleteCommentRequest = [NSMutableURLRequest requestWithURL:editedDeleteCommentUrl];
    [deleteCommentRequest setTimeoutInterval:kUrlRequestTimeout];
    [deleteCommentRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *deleteCommentOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:deleteCommentRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"delete comment response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
            
        }else{
            //error with request
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if(kLoggingEnabled)NSLog(@"delete comment connection failed: %@", error);
        
    }];
    
    [deleteCommentOperation start];
}

-(void)showClosePostAlertView{
    [self closeAddCommentKeyboard];
    
    UIActionSheet *optionsActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Archive", @"Delete", nil];
    [optionsActionSheet setDestructiveButtonIndex:1];
    [optionsActionSheet setTag:TAG_CLOSE_POST_ACTION_SHEET];
    [optionsActionSheet showInView:self.tabBarController.view];
}

-(void)downloadMediumThumbnail{
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    [mediumThumbnailLoadingSpinner startAnimating];
    [self performSelector:@selector(stopImageLoadingAnimation) withObject:nil afterDelay:10.0];
    NSURL *editedGetImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@", kGetPostImageURL, [Utilities commonParams], kJSONImageStyleKeyName, kJSONImageStyleMedium, kJSONPostId, postId]];
    
    if(kLoggingEnabled)NSLog(@"get medium image URL: %@", editedGetImageUrl);
    NSMutableURLRequest *getImageRequest = [NSMutableURLRequest requestWithURL:editedGetImageUrl];
    [getImageRequest setTimeoutInterval:kUrlRequestTimeout];
    [getImageRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getImageOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getImageRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"get medium image response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSURL *editedDownloadImageUrl = [NSURL URLWithString:[resultDictionary objectForKey:kJSONImageUriKeyName]];
            if(kLoggingEnabled)NSLog(@"download medium image uri: %@", editedDownloadImageUrl);
            
            NSMutableURLRequest *downloadRequest = [NSMutableURLRequest requestWithURL:editedDownloadImageUrl];
            [downloadRequest setTimeoutInterval:kUrlRequestTimeout];
            [downloadRequest setHTTPMethod:@"GET"];
            
            AFImageRequestOperation *downloadOperation = [AFImageRequestOperation imageRequestOperationWithRequest:downloadRequest success:^(UIImage *image) {
                
                [mediumThumbnailLoadingSpinner stopAnimating];
                mediumThumbnailIsLoaded = YES;
                [photoButton setBackgroundImage:image forState:UIControlStateNormal];
                
            }];
            
            [downloadOperation start];
            
            
        }else{
            //error with request
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [mediumThumbnailLoadingSpinner stopAnimating];
        if(kLoggingEnabled)NSLog(@"get default image connection failed: %@", error);
        
    }];
    
    [getImageOperation start];
    
}

-(void)stopImageLoadingAnimation{
    [mediumThumbnailLoadingSpinner stopAnimating];
}

#pragma mark - Button Presses
-(void)photoButtonPressed:(UIButton *)button{
    [self closeAddCommentKeyboard];
    
    if([button isSelected]){
        [mediumThumbnailLoadingSpinner stopAnimating];
        [button setSelected:NO];
        [UIView animateWithDuration:0.1 animations:^{
            [photoButtonBackground setFrame:CGRectMake(208, 12, 96, 96)];
            [button setFrame:CGRectMake(212, 16, 88, 88)];
            if([[postDictionary objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
                [button.layer setCornerRadius:8.0];
            }else{
                [button.layer setCornerRadius:7.0];
            }
        }];
    }else{
        [button setSelected:YES];
        if(!mediumThumbnailIsLoaded){
            if(![[postDictionary objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
                [self downloadMediumThumbnail];
            }
        }
        [UIView animateWithDuration:0.1 animations:^{
            [photoButtonBackground setFrame:CGRectMake(6, 2, 308, 308)];
            [button setFrame:CGRectMake(10, 6, 300, 300)];
            if([[postDictionary objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
                [button.layer setCornerRadius:39.0];
            }else{
                [button.layer setCornerRadius:3.0];
            }
        } completion:^(BOOL finished) {
            if(!mediumThumbnailIsLoaded){
                if(![[postDictionary objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
                    [mediumThumbnailLoadingSpinner startAnimating];
                }
            }
        }];
    }
}

-(void)contactButtonPressed{
    
    [self closeAddCommentKeyboard];
    
    [self sendMessageButtonPressed];
    /*
    UIActionSheet *contactActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Send Message", nil];
    [contactActionSheet setTag:TAG_OPTIONS_NOT_POST_OWNER_ACTION_SHEET];
    [contactActionSheet showInView:self.tabBarController.view];
     */
}

-(void)watchButtonPressed:(UIButton *)button{
    
    [self closeAddCommentKeyboard];
    
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    [button setEnabled:NO];
    NSURL *editedUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kAddWatchedPostURL, [Utilities commonParams], kJSONPostId, postId]];
    
    if(kLoggingEnabled)NSLog(@"watch post URL: %@", editedUrl);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:editedUrl];
    [urlRequest setTimeoutInterval:kUrlRequestTimeout];
    [urlRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"watch post response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            [button setEnabled:YES];
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kAddWatchPostErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            //error with request
            [button setEnabled:YES];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kAddWatchPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [button setEnabled:YES];
        if(kLoggingEnabled)NSLog(@"watch post connection failed: %@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kAddWatchPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [requestOperation start];
}

-(void)sendMessageButtonPressed{
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    SendMessageViewController *sendMessageViewController = [[SendMessageViewController alloc] init];
    [sendMessageViewController setPostId:postId];
    [sendMessageViewController setRecipientUserId:[postDictionary objectForKey:kJSONUserIdKeyName]];
    [self.navigationController pushViewController:sendMessageViewController animated:YES];
}

-(void)flagInappropriateButtonPressed{
    
    UIActionSheet *optionsActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Flag Inappropriate", nil];
    [optionsActionSheet setTag:TAG_FLAG_INAPPROPRIATE_ACTIONSHEET];
    [optionsActionSheet showInView:self.tabBarController.view];
    
}

-(void)reportAbuseButtonPressed{
    [SVProgressHUD showWithStatus:@"Reporting" maskType:SVProgressHUDMaskTypeGradient];
    NSURL *editedClosePostUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@", kReportAbuseURL, [Utilities commonParams], kJSONSourceKeyName, kJSONPostKeyName, kJSONSourceIdKeyName, [postDictionary objectForKey:kJSONPostId]]];
    
    if(kLoggingEnabled)NSLog(@"report abuse URL: %@", editedClosePostUrl);
    NSMutableURLRequest *closePostRequest = [NSMutableURLRequest requestWithURL:editedClosePostUrl];
    [closePostRequest setTimeoutInterval:kUrlRequestTimeout];
    [closePostRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *closePostOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:closePostRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"close post response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Report Sent" message:@"Your report has been sent to Slinggit." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kReportAbuseErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            //error with request
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kReportAbuseErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"close post connection failed: %@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kReportAbuseErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [closePostOperation start];
}

-(void)closePostButtonPressed{
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    [SVProgressHUD showWithStatus:@"Closing" maskType:SVProgressHUDMaskTypeGradient];
    NSURL *editedClosePostUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kClosePostURL, [Utilities commonParams], kJSONPostId, postId]];
    
    if(kLoggingEnabled)NSLog(@"close post URL: %@", editedClosePostUrl);
    NSMutableURLRequest *closePostRequest = [NSMutableURLRequest requestWithURL:editedClosePostUrl];
    [closePostRequest setTimeoutInterval:kUrlRequestTimeout];
    [closePostRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *closePostOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:closePostRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"close post response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:NO];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kCloseErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            //error with request
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kCloseErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"close post connection failed: %@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kCloseErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [closePostOperation start];
    
}

-(void)deletePostButtonPressed{
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    [SVProgressHUD showWithStatus:@"Deleting" maskType:SVProgressHUDMaskTypeGradient];
    NSURL *editedDeletePostUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kDeletePostURL, [Utilities commonParams], kJSONPostId, postId]];
    
    if(kLoggingEnabled)NSLog(@"delete post URL: %@", editedDeletePostUrl);
    NSMutableURLRequest *deletePostRequest = [NSMutableURLRequest requestWithURL:editedDeletePostUrl];
    [deletePostRequest setTimeoutInterval:kUrlRequestTimeout];
    [deletePostRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *deletePostOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:deletePostRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"delete post response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:NO];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kDeleteErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            //error with request
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kDeleteErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"delete post connection failed: %@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kDeleteErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [deletePostOperation start];
}

-(void)showMoreButtonPressed{
    
    if(sourceData && [sourceData count] > 0){
        
        NSString *startingCommentId = [[sourceData objectAtIndex:([sourceData count]-1)] objectForKey:kJSONIdKeyName];
        
        //[SVProgressHUD showWithStatus:@"Loading More"];
        [self getPostCommentsWithStartingCommentId:startingCommentId withLimit:[NSNumber numberWithInt:kCommentShowLimit]];
    }
}

#pragma mark - PullToRefresh Delegate Methods
- (void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view{
    
    [self loadTableDataFromNewestComment];
}


#pragma mark - UITableview Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([sourceData count] > 0){
        if(shouldShowMoreButton){
            return [sourceData count]+1;
        }else{
            return [sourceData count];
        }
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([sourceData count] > 0){
        if(indexPath.row == [sourceData count]){
            return kShowMoreCellHeight;
        }
        
        NSDictionary *commentsDictionary = [sourceData objectAtIndex:indexPath.row];
        
        CGSize size = [[commentsDictionary objectForKey:kJSONBodyKeyName] sizeWithFont:subTitleFont constrainedToSize:CGSizeMake(280, 999) lineBreakMode:UILineBreakModeWordWrap];
        
        return size.height+40;
        
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return headerViewHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 280)];
    
    UIImageView *postBackgroundImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [headerView addSubview:postBackgroundImageView];
    
    //UITextView *topDescriptionLabel = [[UITextView alloc] initWithFrame:CGRectMake(10, 46, 198, 80)];
    UITextView *topDescriptionLabel = [[UITextView alloc] initWithFrame:CGRectMake(10, 108, 300, 80)];
    [topDescriptionLabel setFont:[UIFont systemFontOfSize:14.0]];
    [topDescriptionLabel setTextColor:[UIColor darkGrayColor]];
    [topDescriptionLabel setBackgroundColor:[UIColor clearColor]];
    [topDescriptionLabel setText:[postDictionary objectForKey:kJSONPostContentKeyName]];
    //[topDescriptionLabel setText:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nulla ipsum. Integer eu ante sem. Aliquam diam magna, auctor in adipiscing sagittis, tempus eu augue. In hac habitasse platea dictumst. Sed fermentum hendrerit viverra. Phasellus consectetur cursus blandit. Vivamus dignissim nullam."];
    //[topDescriptionLabel setText:@"I am going to use small words to desribe this item. It is big round and rolly but you can't call it that cause it doesn't like being called rolly. after you roll it up in a pie it becomes larger than life and bigger than a turke doll with poofers for ears. Don't tell it that though cause the poofers for the looking goods."];
    [topDescriptionLabel setUserInteractionEnabled:NO];
    [headerView addSubview:topDescriptionLabel];

    CGRect frame = topDescriptionLabel.frame;
    frame.size.height = topDescriptionLabel.contentSize.height;
    [topDescriptionLabel setFrame:frame];
    
    [postBackgroundImageView setFrame:CGRectMake(10, 6, 300, 172+topDescriptionLabel.contentSize.height)];
    
    headerViewHeight = 184+topDescriptionLabel.contentSize.height;
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 11, 100, 20)];
    [dateLabel setFont:[UIFont systemFontOfSize:kDateFontSize]];
    [dateLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [dateLabel setBackgroundColor:[UIColor clearColor]];
    [dateLabel setTextAlignment:UITextAlignmentLeft];
    [dateLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    dateLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    [dateLabel setText:[Utilities getElaspsedTimeFromPostDictionary:postDictionary]];
    [headerView addSubview:dateLabel];
    
    NSString *priceString = [[postDictionary objectForKey:kJSONPriceKeyName] stringValue];
    
    if([priceString length] > 3){
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setPositiveFormat:@"#,###"];
        
        priceString = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSNumber numberWithInt:[priceString intValue]]]];
    }else{
        priceString = [NSString stringWithFormat:@"%@", [[postDictionary objectForKey:kJSONPriceKeyName] stringValue]]; 
    }
    
    UILabel *dollarSignLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 37, 10, 12)];
    [dollarSignLabel setBackgroundColor:[UIColor clearColor]];
    [dollarSignLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [dollarSignLabel setTextAlignment:UITextAlignmentLeft];
    [dollarSignLabel setFont:[UIFont systemFontOfSize:13.0]];
    [dollarSignLabel setShadowColor:[UIColor whiteColor]];
    dollarSignLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    [dollarSignLabel setText:@"$"];
    [headerView addSubview:dollarSignLabel];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(28, 34, 88, 20)];
    [priceLabel setBackgroundColor:[UIColor clearColor]];
    [priceLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [priceLabel setTextAlignment:UITextAlignmentLeft];
    [priceLabel setFont:[UIFont boldSystemFontOfSize:17.0]];
    [priceLabel setShadowColor:[UIColor whiteColor]];
    priceLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    [priceLabel setText:priceString];
    [headerView addSubview:priceLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 56, 188, 20)];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
    [titleLabel setTextColor:[UIColor darkTextColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setShadowColor:[UIColor whiteColor]];
    [titleLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [titleLabel setText:[postDictionary objectForKey:kJSONHashTagPrefixKeyName]];
    //[titleLabel setText:@"mmmmmmmmmmmmmmm"];
    [headerView addSubview:titleLabel];
    
    UIImageView *locationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_icon.png"]];
    [locationIcon setContentMode:UIViewContentModeScaleAspectFit];
    //[locationIcon setFrame:CGRectMake(16, 74, 20, 20)];
    [locationIcon setFrame:CGRectMake(14, 78, 16, 16)];
    [headerView addSubview:locationIcon];
    
    //UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(38, 74, 170, 20)];
    UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 75, 179, 20)];
    [locationLabel setFont:[UIFont systemFontOfSize:14.0]];
    [locationLabel setTextColor:[UIColor darkTextColor]];
    [locationLabel setBackgroundColor:[UIColor clearColor]];
    [locationLabel setShadowColor:[UIColor whiteColor]];
    [locationLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [locationLabel setText:[postDictionary objectForKey:kJSONLocationKeyName]];
    //[locationLabel setText:@"Dmmmmmmmmmmmmmmm"];
    [headerView addSubview:locationLabel];
    
    if([[postDictionary objectForKey:kJSONUserIdKeyName] intValue] != [[KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USER_ID] intValue]){
        UIButton *contactButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [contactButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
        [contactButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
        CGRect ContactButtonFrame;
        if(shouldShowWatchPostButton){
            ContactButtonFrame = CGRectMake(20, headerViewHeight - 61, 220, 46);
        }else{
            ContactButtonFrame = CGRectMake(20, headerViewHeight - 61, 280, 46);
        }
        [contactButton setFrame:ContactButtonFrame];
        [contactButton setTitle:@"Contact Seller" forState:UIControlStateNormal];
        [contactButton addTarget:self action:@selector(contactButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [contactButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
        contactButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
        [headerView addSubview:contactButton];
        
        if(shouldShowWatchPostButton){
            UIButton *watchButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [watchButton setBackgroundImage:[[UIImage imageNamed:@"black_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"black_button.png"]]] forState:UIControlStateNormal];
            [watchButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
            [watchButton setFrame:CGRectMake(250, headerViewHeight - 61, 50, 46)];
            [watchButton setImage:[UIImage imageNamed:@"watch_icon.png"] forState:UIControlStateNormal];
            [watchButton addTarget:self action:@selector(watchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [watchButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
            watchButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
            [headerView addSubview:watchButton];
        }
        
    }else{
        UIButton *closePostButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closePostButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
        [closePostButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
        [closePostButton setFrame:CGRectMake((kScreenWidth - 280)/2, headerViewHeight - 61, 280, 46)];
        [closePostButton setTitle:@"Edit Post" forState:UIControlStateNormal];
        [closePostButton addTarget:self action:@selector(showClosePostAlertView) forControlEvents:UIControlEventTouchUpInside];
        [closePostButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
        closePostButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
        [headerView addSubview:closePostButton];
    }
    
    /*
    UIView *addCommentView = [[UIView alloc] initWithFrame:CGRectMake(10, headerViewHeight-53, 300, 50)];
    
    UIImageView *commentTextBackground = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [commentTextBackground setFrame:CGRectMake(0, 0, 300, addCommentView.frame.size.height)];
    [addCommentView addSubview:commentTextBackground];
    
    UIImageView *commentIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comment_icon.png"]];
    [commentIcon setFrame:CGRectMake(4, 15, 20, 20)];
    [addCommentView addSubview:commentIcon];
    
    addCommentTextField = [[UITextField alloc] initWithFrame:CGRectMake(30, 0, 270, 50)];
    [addCommentTextField setDelegate:self];
    [addCommentTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [addCommentTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [addCommentTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [addCommentTextField setPlaceholder:kAddCommentPlaceholderText];
    addCommentTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [addCommentTextField setReturnKeyType:UIReturnKeySend];
    [addCommentView addSubview:addCommentTextField];
    
    [headerView addSubview:addCommentView];
    */
    photoButtonBackground = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"photo_button_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"photo_button_background.png"]]]];
    [photoButtonBackground setFrame:CGRectMake(208, 12, 96, 96)];
    [headerView addSubview:photoButtonBackground];
    
    photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [photoButton setFrame:CGRectMake(212, 16, 88, 88)];
    [photoButton setBackgroundImage:[[UIImage imageNamed:@"photo_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"photo_button.png"]]] forState:UIControlStateNormal];
    [photoButton.layer setBorderWidth:1.0];
    [photoButton.layer setBorderColor:[[UIColor colorWithWhite:0.0 alpha:0.3] CGColor]];
    if([[postDictionary objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
        [photoButton.layer setCornerRadius:8.0];
    }else{
        [photoButton.layer setCornerRadius:7.0];
    }
    [photoButton.layer setMasksToBounds:YES];
    [photoButton addTarget:self action:@selector(photoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:photoButton];
    
    NSString *postId;
    if([postDictionary objectForKey:kJSONPostId]){
        postId = [postDictionary objectForKey:kJSONPostId];
    }else{
        postId = [postDictionary objectForKey:kJSONIdKeyName];
    }
    
    NSString *photoBase64 = [Utilities findPhotoWithPostId:[NSNumber numberWithInt:[postId intValue]]];
    if (photoBase64) {
        NSData *imageData = [Utilities dataFromBase64String:photoBase64];
        UIImage *cellImage = [UIImage imageWithData:imageData];
        [photoButton setBackgroundImage:cellImage forState:UIControlStateNormal];
    }else{
        
        if(![[postDictionary objectForKey:@"image_uri"] isEqual:[NSNull null]]){
            NSURL *editedGetImageUrl = [NSURL URLWithString:[postDictionary objectForKey:@"image_uri"]];
            
            if(kLoggingEnabled)NSLog(@"get image data URL: %@", editedGetImageUrl);
            NSMutableURLRequest *getImageRequest = [NSMutableURLRequest requestWithURL:editedGetImageUrl];
            [getImageRequest setTimeoutInterval:kUrlRequestTimeout];
            [getImageRequest setHTTPMethod:@"GET"];
            
            AFImageRequestOperation *imageRequestOperation = [AFImageRequestOperation imageRequestOperationWithRequest:getImageRequest success:^(UIImage *image) {
                
                [photoButton setBackgroundImage:image forState:UIControlStateNormal];
                
                NSString *postId;
                if([postDictionary objectForKey:kJSONPostId]){
                    postId = [postDictionary objectForKey:kJSONPostId];
                }else{
                    postId = [postDictionary objectForKey:kJSONIdKeyName];
                }
                
                NSData *tempImageData = UIImageJPEGRepresentation(image, 1.0);
                NSString *base64EncodedString = [Utilities newStringInBase64FromData:tempImageData];
                [Utilities saveData:base64EncodedString withPostId:[NSNumber numberWithInt:[postId intValue]]];
                
                if([Utilities numberOfPhotoRecords] > kPhotoDownloadStorageNumberLimit){
                    [Utilities deleteOldestPhotoRecord];
                }
                
            }];
            
            [imageRequestOperation start];
        }else if([Utilities getDefaultImage]){
            [photoButton setBackgroundImage:[Utilities getDefaultImage] forState:UIControlStateNormal];
            
        }else{
            [Utilities downloadDefaultPlaceholderImage];
        }
        
    }
    
    return headerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier;
    if(indexPath.row < [sourceData count]){
        CellIdentifier  = @"NormalCell";
    }else{
        CellIdentifier = @"FooterCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [self getCellContentView:CellIdentifier];
        
    }
    UIImageView *postBackgroundImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [postBackgroundImageView setFrame:CGRectZero];
    [cell.layer setMasksToBounds:YES];
    [cell setBackgroundView:postBackgroundImageView];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if(indexPath.row < [sourceData count]){
        NSDictionary *commentsDictionary = [sourceData objectAtIndex:indexPath.row];
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_DATE_TAG] setText:[Utilities getElaspsedTimeFromPostDictionary:commentsDictionary]];
        NSString *username = @"";
        if([commentsDictionary objectForKey:kJSONUsernameKeyName]){
            username = [commentsDictionary objectForKey:kJSONUsernameKeyName];
        }
        [(UIImageView *)[cell.contentView viewWithTag:CELL_COMMENT_ICON_TAG] setImage:[UIImage imageNamed:@"comment_icon.png"]];
        [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setText:username];
        
        CGSize size = [[commentsDictionary objectForKey:kJSONBodyKeyName] sizeWithFont:subTitleFont constrainedToSize:CGSizeMake(280, 999) lineBreakMode:UILineBreakModeWordWrap];
        
        [(UITextView *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setFrame:CGRectMake(0, 18, 300, size.height+40)];
        [(UITextView *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setText:[commentsDictionary objectForKey:kJSONBodyKeyName]];
        if([[postDictionary objectForKey:kJSONUserIdKeyName] intValue] == [[commentsDictionary objectForKey:kJSONUserIdKeyName] intValue]){
            [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setTextColor:COLOR_SLINGGIT_BLUE];
            
        }else{
            [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setTextColor:[UIColor darkTextColor]]; 
        }
    }else{
        if(indexPath.row == [sourceData count] && shouldShowMoreButton && !pullRefreshView.isLoading){
            [pullRefreshView beginLoading];
            [self showMoreButtonPressed];
            if(kLoggingEnabled)NSLog(@"shore more...");
        }
    }
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {	
	
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(editingStyle == UITableViewCellEditingStyleDelete){
        
        [self deleteCommentForRow:[NSNumber numberWithInt:indexPath.row]];
        [sourceData removeObjectAtIndex:indexPath.row];
        [commentsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
        [commentsTableView reloadData];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([sourceData count] > 0){
        if([KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USER_ID]){
            NSDictionary *commentsDictionary = [sourceData objectAtIndex:indexPath.row];
            
            if([[postDictionary objectForKey:kJSONUserIdKeyName] intValue] == [[KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USER_ID] intValue] || [[commentsDictionary objectForKey:kJSONUserIdKeyName] intValue] == [[KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USER_ID] intValue]){
                if(indexPath.row != [sourceData count]){
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellEditingStyleDelete;
    
}

-(UITableViewCell *)getCellContentView:(NSString *)CellIdentifier{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    if([CellIdentifier isEqualToString:@"NormalCell"]){
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(180, 6, 112, 18)];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setTextAlignment:UITextAlignmentRight];
        [date setTag:CELL_DATE_TAG];
        [date setTextColor:COLOR_SLINGGIT_BLUE];
        [date setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        date.shadowOffset = CGSizeMake(0.0, 1.0);
        [date setFont:[UIFont systemFontOfSize:kDateFontSize]];
        [cell.contentView addSubview:date];
        
        UIImageView *commentIconImage = [[UIImageView alloc] initWithFrame:CGRectMake(4, 3, 20, 20)];
        [commentIconImage setContentMode:UIViewContentModeScaleAspectFit];
        [commentIconImage setTag:CELL_COMMENT_ICON_TAG];
        commentIconImage.transform = CGAffineTransformMake(-1, 0, 0, 1, commentIconImage.transform.tx, 0);
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft 
                               forView:commentIconImage 
                                 cache:NO];
        [UIView commitAnimations];
        
        [cell.contentView addSubview:commentIconImage];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(24, 6, 220, 18)];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTag:CELL_TITLE_TAG];
        [title setTextColor:[UIColor darkTextColor]];
        [title setShadowColor:[UIColor whiteColor]];
        [title setShadowOffset:CGSizeMake(0.0, 1.0)];
        [title setFont:[UIFont boldSystemFontOfSize:15]];
        [cell.contentView addSubview:title];
        
        UITextView *subtitle = [[UITextView alloc] initWithFrame:CGRectMake(0, 18, 300, 60)];
        [subtitle setBackgroundColor:[UIColor clearColor]];
        [subtitle setUserInteractionEnabled:NO];
        [subtitle setTag:CELL_SUBTITLE_TAG];
        [subtitle setFont:subTitleFont];
        [subtitle setTextColor:[UIColor darkGrayColor]];
        [cell.contentView addSubview:subtitle];
        
    }else{
        
        UIActivityIndicatorView *loadingActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [loadingActivityIndicator setFrame:CGRectMake(80, 0, 50, kShowMoreCellHeight)];
        [loadingActivityIndicator setTag:TAG_LOADING_ACTIVITY_INDICATOR];
        [loadingActivityIndicator startAnimating];
        [cell.contentView addSubview:loadingActivityIndicator];
        
        UILabel *showMoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kShowMoreCellHeight)];
        [showMoreLabel setTag:TAG_LOADING_LABEL];
        [showMoreLabel setTextAlignment:UITextAlignmentCenter];
        [showMoreLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
        [showMoreLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        showMoreLabel.shadowOffset = CGSizeMake(0.0, 1.0);
        [showMoreLabel setTextColor:COLOR_SLINGGIT_BLUE];
        [showMoreLabel setText:@"Loading"];
        [showMoreLabel setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:showMoreLabel];
        
    }
    
    return cell;
}

#pragma mark - ActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (actionSheet.tag) {
            
        case TAG_OPTIONS_NOT_POST_OWNER_ACTION_SHEET:
            switch (buttonIndex) {
                case 0:
                    //add comment 
                    [self addCommentAlertView];
                    break;
                    
                case 1:
                    [self sendMessageButtonPressed];
                    break;
                default:
                    break;
            }
            
            break;
            
        case TAG_FLAG_INAPPROPRIATE_ACTIONSHEET:
            switch (buttonIndex) {
                case 0:
                    //report abuse
                    [self reportAbuseButtonPressed];
                    break;
                default:
                    break;
            }
            
            break;
            
        case TAG_CLOSE_POST_ACTION_SHEET:
            switch (buttonIndex) {
                case 0:
                    [self closePostButtonPressed];
                    break;
                    
                case 1:
                    [self deletePostButtonPressed];
                    break;
                default:
                    break;
            }
            
            break;
            
        default:
            
            break;
            
            
    }
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        UITextField *textField = [alertView textFieldAtIndex:0];
        [self addComment:textField.text];
    }
}
-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    UITextField *textField = [alertView textFieldAtIndex:0];
    if([textField.text length] == 0){
        return NO;
    }else{
        return YES;
    }
}

#pragma mark - UITextField Delegate Methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    int maxLength = kCommentFieldLimit;
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    return newLength <= maxLength;
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    [self addCommentTextFieldAnimateUp];

    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextView *)textField{
    
    [self addCommentTextFieldAnimateDown];
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self closeAddCommentKeyboard];
    [self addComment:textField.text];
    return NO;
}

#pragma mark - Gesture Recognizer Methods

-(void)backgroundTapped:(UITapGestureRecognizer *)gestureRecognizer{
    [self closeAddCommentKeyboard];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isKindOfClass:[UIControl class]]){
        return NO;
    }
    return YES;
}

#pragma mark - UISearchBar Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self closeAddCommentKeyboard];
    [self addComment:[searchBar text]]; 
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar setText:@""];
    [searchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self addCommentTextFieldAnimateUp];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}

@end
