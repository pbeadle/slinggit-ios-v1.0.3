#import "SignUpViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "AFJSONRequestOperation.h"
#import "Constants.h"
#import "KeychainWrapper.h"
#import "Utilities.h"
#import "WebViewController.h"
#import "MyTextField.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"

@interface SignUpViewController (){
    
    NSString *state;
    UIView *usernameEmailView;
    UIImageView *usernameEmailViewBackground;
    UIView *passwordsAndSubmitView;
    NSDictionary *twitterAccountParameters;
    //UIView *twitterAccountAddedBackground;
}
@end

@implementation SignUpViewController

@synthesize usernameTextField = _usernameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize passwordTextField = _passwordTextField;

#define TAG_USERNAME_TEXTFIELD 0
#define TAG_EMAIL_TEXTFIELD 1
#define TAG_PASSWORD_TEXTFIELD 2

#define TAG_ALERTVIEW_MISSING_USERNAME 0
#define TAG_ALERTVIEW_INVALID_USERNAME 1
#define TAG_ALERTVIEW_MISSING_EMAIL 2
#define TAG_ALERTVIEW_INVALID_EMAIL 3
#define TAG_ALERTVIEW_MISSING_PASSWORD 4
#define TAG_ALERTVIEW_INVALID_PASSWORD 5
#define TAG_NO_ERROR 6


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [super loadView];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_background.jpg"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, 480)];
    [backgroundImageView setContentMode:UIViewContentModeScaleAspectFill];
    [view addSubview:backgroundImageView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    [singleTap setDelegate:self];
    [singleTap setCancelsTouchesInView:NO];
    [view addGestureRecognizer:singleTap];
    
    /*
    twitterAccountAddedBackground = [[UIView alloc] initWithFrame:CGRectMake(10, 210, 300, 20)];
    //[twitterAccountAddedBackground setHidden:YES];
    [twitterAccountAddedBackground setBackgroundColor:[UIColor clearColor]];
    [twitterAccountAddedBackground.layer setCornerRadius:7.0];
    [view addSubview:twitterAccountAddedBackground];
    
    UILabel *twitterAccountAddedLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, 280, 20)];
    [twitterAccountAddedLabel setText:@"Twitter Account Added"];
    [twitterAccountAddedLabel setBackgroundColor:[UIColor clearColor]];
    [twitterAccountAddedLabel setTextAlignment:UITextAlignmentCenter];
    [twitterAccountAddedLabel setTextColor:[UIColor whiteColor]];
    [twitterAccountAddedLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4]];
    [twitterAccountAddedLabel setShadowOffset:CGSizeMake(0.0, -1.0)];
    [twitterAccountAddedBackground addSubview:twitterAccountAddedLabel];
    
    UIImageView *twitterIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"twitter_icon_white.png"]];
    [twitterIcon setFrame:CGRectMake(40, -3, 26, 26)];
    [twitterIcon setContentMode:UIViewContentModeScaleAspectFit];
    [twitterAccountAddedBackground addSubview:twitterIcon];
    */
     
    usernameEmailView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, 178)];
    [view addSubview:usernameEmailView];
    
    usernameEmailViewBackground = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [usernameEmailViewBackground setFrame:CGRectMake(10, 0, 300, 100)];
    [usernameEmailView addSubview:usernameEmailViewBackground];
    
    UIImageView *loginTextLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"text_input_line.png"]];
    [loginTextLine setFrame:CGRectMake(12, 49, 296, 2)];
    [usernameEmailView addSubview:loginTextLine];
    
    self.usernameTextField = [[MyTextField alloc] initWithFrame:CGRectMake(20, 0, 280, 50)];
    [self.usernameTextField setTag:TAG_USERNAME_TEXTFIELD];
    [self.usernameTextField setDelegate:self];
    [self.usernameTextField setReturnKeyType:UIReturnKeyNext];
    [self.usernameTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    //[self.usernameTextField setReturnKeyType:UIReturnKeyNext];
    [self.usernameTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.usernameTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    [self.usernameTextField setPlaceholder:@"Username"];
    self.usernameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [usernameEmailView addSubview:self.usernameTextField];
    
    self.emailTextField = [[MyTextField alloc] initWithFrame:CGRectMake(20, 50, 280, 50)];
    [self.emailTextField setTag:TAG_EMAIL_TEXTFIELD];
    [self.emailTextField setDelegate:self];
    [self.emailTextField setReturnKeyType:UIReturnKeyNext];
    [self.emailTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    //[self.emailTextField setReturnKeyType:UIReturnKeyNext];
    [self.emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.emailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.emailTextField setPlaceholder:@"Email"];
    
    self.emailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [usernameEmailView addSubview:self.emailTextField];
    
    UIButton *authenticateWithTwitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [authenticateWithTwitterButton setTag:10];
    [authenticateWithTwitterButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
    [authenticateWithTwitterButton setFrame:CGRectMake(20, 136, 154, 46)];
    [authenticateWithTwitterButton setTitle:@"Add Twitter" forState:UIControlStateNormal];
    [authenticateWithTwitterButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [authenticateWithTwitterButton addTarget:self action:@selector(authenticateWithTwitterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [authenticateWithTwitterButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    authenticateWithTwitterButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [usernameEmailView addSubview:authenticateWithTwitterButton];
    
    UIButton *noTwitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [noTwitterButton setTag:11];
    [noTwitterButton setBackgroundImage:[[UIImage imageNamed:@"black_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"black_button.png"]]] forState:UIControlStateNormal];
    [noTwitterButton setFrame:CGRectMake(184, 136, 116, 46)];
    [noTwitterButton setTitle:@"No Twitter" forState:UIControlStateNormal];
    [noTwitterButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [noTwitterButton addTarget:self action:@selector(noTwitterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [noTwitterButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    noTwitterButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [usernameEmailView addSubview:noTwitterButton];
    
    passwordsAndSubmitView = [[UIView alloc] initWithFrame:CGRectMake(0, usernameEmailView.frame.origin.y+usernameEmailView.frame.size.height-110, kScreenWidth, 280)];
    [passwordsAndSubmitView setClipsToBounds:YES];
    [passwordsAndSubmitView setHidden:YES];
    [view addSubview:passwordsAndSubmitView];
    
    UIImageView *passwordTextLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"text_input_line.png"]];
    [passwordTextLine setFrame:CGRectMake(12, 0, 296, 2)];
    [passwordsAndSubmitView addSubview:passwordTextLine];
    
    self.passwordTextField = [[MyTextField alloc] initWithFrame:CGRectMake(20, 0, 280, 50)];
    [self.passwordTextField setTag:TAG_PASSWORD_TEXTFIELD];
    [self.passwordTextField setDelegate:self];
    [self.passwordTextField setReturnKeyType:UIReturnKeyGo];
    [self.passwordTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.passwordTextField setSecureTextEntry:YES];
    [self.passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.passwordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.passwordTextField setPlaceholder:@"Slinggit Password"];
    self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [passwordsAndSubmitView addSubview:self.passwordTextField];
    
    UILabel *termsOfServiceLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 52, 280, 20)];
    [termsOfServiceLabel setLineBreakMode:UILineBreakModeWordWrap];
    [termsOfServiceLabel setNumberOfLines:0];
    [termsOfServiceLabel setTextAlignment:UITextAlignmentCenter];
    [termsOfServiceLabel setBackgroundColor:[UIColor clearColor]];
    [termsOfServiceLabel setFont:[UIFont systemFontOfSize:14.0]];
    [termsOfServiceLabel setText:@"By signing in to Slinggit, you agree to the"];
    [termsOfServiceLabel setTextColor:[UIColor whiteColor]];
    [termsOfServiceLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4]];
    [termsOfServiceLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [passwordsAndSubmitView addSubview:termsOfServiceLabel];
    
    UIButton *termsOfServiceButton = [[UIButton alloc] initWithFrame:CGRectMake((kScreenWidth-136)/2, 55, 136, 50)];
    [termsOfServiceButton setBackgroundColor:[UIColor clearColor]];
    [termsOfServiceButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
    [termsOfServiceButton setTitle:@"Terms of Service" forState:UIControlStateNormal];
    [termsOfServiceButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [termsOfServiceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [termsOfServiceButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    [termsOfServiceButton.titleLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [termsOfServiceButton setReversesTitleShadowWhenHighlighted:YES];
    [termsOfServiceButton addTarget:self action:@selector(termsOfServiceButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [passwordsAndSubmitView addSubview:termsOfServiceButton];
    
    /*
    UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
    [submitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [submitButton setFrame:CGRectMake((kScreenWidth-280)/2, 126, 280, 46)];
    [submitButton setTitle:@"Sign Up" forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [submitButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    submitButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [passwordsAndSubmitView addSubview:submitButton];
    */
    state = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_UNIQUE_ID];
    
    self.view = view;
    
}

- (void) viewWillAppear:(BOOL)animated{
    
    
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
    [super viewWillDisappear:animated];
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] 
                                   initWithTitle: @"Sign In" 
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [self setTitle:@"Sign Up"];
    self.navigationController.navigationBar.tintColor = COLOR_SLINGGIT_BLUE;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - My Methods
-(void)clearFirstResponders{
    [self.usernameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

-(void)signupUser{
    [SVProgressHUD showWithStatus:@"Signing Up" maskType:SVProgressHUDMaskTypeGradient];
    NSString *encodedUser = [Utilities stringByEncodingHTMLEntities:self.usernameTextField.text];
    NSString *encodedEmail = [Utilities stringByEncodingHTMLEntities:self.emailTextField.text];
    NSString *encodedPassword = [Utilities stringByEncodingHTMLEntities:self.passwordTextField.text];
    NSString *twitterAccessToken = @"";
    NSString *twitterAccessTokenSecret = @"";
    if(twitterAccountParameters){
        twitterAccessToken = [twitterAccountParameters objectForKey:kJSONTwitterAccessTokenKeyName];
        twitterAccessTokenSecret = [twitterAccountParameters objectForKey:kJSONTwitterAccessTokenSecretKeyName];
    }
    NSURL *editedSignUpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&user_name=%@&email=%@&password=%@&%@=%@&%@=%@", kSignUpURL, [Utilities commonParams], encodedUser, encodedEmail, encodedPassword, kJSONTwitterAccessTokenKeyName, twitterAccessToken, kJSONTwitterAccessTokenSecretKeyName, twitterAccessTokenSecret]];
    if(kLoggingEnabled)NSLog(@"edited Sing Up url: %@", editedSignUpUrl);
    NSMutableURLRequest *signupRequest = [NSMutableURLRequest requestWithURL:editedSignUpUrl];
    [signupRequest setTimeoutInterval:kUrlRequestTimeout];
    [signupRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *signupOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:signupRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            [Utilities forceLoadMessageDataOnViewWillAppear];
            [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:NO];
            if(kLoggingEnabled)NSLog(@"signup success response JSON: %@", JSON);
            [KeychainWrapper createKeychainValue:self.usernameTextField.text forIdentifier:PREFS_USERNAME];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            [KeychainWrapper createKeychainValue:[[resultDictionary valueForKey:kJSONUserIdKeyName] stringValue] forIdentifier:PREFS_USER_ID];
            NSString *mobileAuthToken = [[NSString alloc] initWithString:[resultDictionary valueForKey:kMobileAuthTokenKeyName]];
            [KeychainWrapper createKeychainValue:mobileAuthToken forIdentifier:PREFS_TOKEN_KEY];
            [self dismissModalViewControllerAnimated:YES];
        }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            if(kLoggingEnabled)NSLog(@"signup failure error JSON: %@", JSON);
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage;
            if([resultDictionary valueForKey:kJSONFriendlyError]){
                errorMessage = [NSString stringWithString:[resultDictionary valueForKey:kJSONFriendlyError]];
            }else{
                errorMessage = [NSString stringWithString:kConnectionErrorText];
            }
            
            UIAlertView *signupErrorAlertView = [[UIAlertView alloc] initWithTitle:kSignUpErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [signupErrorAlertView show];
        }else{
            
            UIAlertView *signupErrorAlertView = [[UIAlertView alloc] initWithTitle:kSignUpErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [signupErrorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"sign in error: %@", error);
        UIAlertView *signupErrorAlertView = [[UIAlertView alloc] initWithTitle:kSignUpErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [signupErrorAlertView show];
    }];
    
    [signupOperation start];
}

-(BOOL)usernameAndEmailValidated{
    NSUInteger alertViewErrorTag = TAG_NO_ERROR;
    if(self.emailTextField.text.length == 0){
        alertViewErrorTag = TAG_ALERTVIEW_MISSING_EMAIL;
        
    }else{
        NSError *error = NULL;
        NSRegularExpression *emailRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" options:NSRegularExpressionCaseInsensitive error:&error];
        
        //if(kLoggingEnabled)NSLog(@"about to hit email check");
        NSTextCheckingResult *emailTextCheckResult = [emailRegex firstMatchInString:self.emailTextField.text options:0 range:NSMakeRange(0, self.emailTextField.text.length)];
        
        if(emailTextCheckResult){
            //NSRange matchRange = [textCheckingResult rangeAtIndex:0];
            //NSString *match = [emailTextField.text substringWithRange:matchRange];
            //if(kLoggingEnabled)NSLog(@"email field definitely is valid: %@", match);
            
        }else{
            
            alertViewErrorTag = TAG_ALERTVIEW_INVALID_EMAIL;
        }
    }
    
    if(self.usernameTextField.text.length == 0){
        
        alertViewErrorTag = TAG_ALERTVIEW_MISSING_USERNAME;
        
    }else{
        NSError *error = NULL;
        NSRegularExpression *usernameRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z_-]{0,20}" options:NSRegularExpressionCaseInsensitive error:&error];
        
        //if(kLoggingEnabled)NSLog(@"about to hit username check");
        NSTextCheckingResult *usernameTextCheckResult = [usernameRegex firstMatchInString:self.usernameTextField.text options:0 range:NSMakeRange(0, self.usernameTextField.text.length)];
        
        if(!usernameTextCheckResult){
            
            alertViewErrorTag = TAG_ALERTVIEW_INVALID_USERNAME;
        }    
    }
    
    if(alertViewErrorTag != TAG_NO_ERROR){
        [self showErrorAlertViewWithTag:alertViewErrorTag];
        
        return NO;
    }
    
    return YES;
}

-(void)showErrorAlertViewWithTag:(NSUInteger)alertViewTag{
    NSString *errorMessage;
    switch (alertViewTag) {
        case TAG_ALERTVIEW_MISSING_USERNAME:
            errorMessage = [NSString stringWithString:@"Please enter a username."];
            break;
        case TAG_ALERTVIEW_INVALID_USERNAME:
            errorMessage = [NSString stringWithString:@"The username is invalid."];
            break;
            
        case TAG_ALERTVIEW_MISSING_EMAIL:
            errorMessage = [NSString stringWithString:@"Please enter an email address."];
            break;
            
        case TAG_ALERTVIEW_INVALID_EMAIL:
            errorMessage = [NSString stringWithString:@"The email address is invalid."];
            break;
            
        case TAG_ALERTVIEW_MISSING_PASSWORD:
            errorMessage = [NSString stringWithString:@"Please enter a password."];
            break;
            
        case TAG_ALERTVIEW_INVALID_PASSWORD:
            errorMessage = [NSString stringWithString:@"The password is invalid."];
            break;
        default:
            break;
    }
    
    if(errorMessage){
        UIAlertView *signupErrorAlertView = [[UIAlertView alloc] initWithTitle:kSignUpErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [signupErrorAlertView setTag:alertViewTag];
        [signupErrorAlertView setDelegate:self];
        [signupErrorAlertView show]; 
    }
}

-(void)validateAllSignUpData{
    
    NSUInteger alertViewErrorTag = TAG_NO_ERROR;
    
    if([self.passwordTextField.text length] == 0){
        
        alertViewErrorTag = TAG_ALERTVIEW_MISSING_PASSWORD;
        
    }
    
    if([self.emailTextField.text length] == 0){
        
        alertViewErrorTag = TAG_ALERTVIEW_MISSING_EMAIL;
        
    }else{
        NSError *error = NULL;
        NSRegularExpression *emailRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSTextCheckingResult *emailTextCheckResult = [emailRegex firstMatchInString:self.emailTextField.text options:0 range:NSMakeRange(0, self.emailTextField.text.length)];
        
        if(emailTextCheckResult){
            //NSRange matchRange = [textCheckingResult rangeAtIndex:0];
            //NSString *match = [emailTextField.text substringWithRange:matchRange];
            //if(kLoggingEnabled)NSLog(@"email field definitely is valid: %@", match);
            
        }else{
            
            alertViewErrorTag = TAG_ALERTVIEW_INVALID_EMAIL;
        }
    }
    
    if([self.usernameTextField.text length] == 0){
        
        alertViewErrorTag = TAG_ALERTVIEW_MISSING_USERNAME;
        
    }else{
        NSError *error = NULL;
        NSRegularExpression *usernameRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z_-]{0,20}" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSTextCheckingResult *usernameTextCheckResult = [usernameRegex firstMatchInString:self.usernameTextField.text options:0 range:NSMakeRange(0, self.usernameTextField.text.length)];
        
        if(!usernameTextCheckResult){
            
            alertViewErrorTag = TAG_ALERTVIEW_INVALID_USERNAME;
        }    
    }
    
    if(alertViewErrorTag != TAG_NO_ERROR){
        [self showErrorAlertViewWithTag:alertViewErrorTag];
        
        return;
    }
    
    [self signupUser];
}

#pragma mark - Button Presses
-(void)authenticateWithTwitterButtonPressed:(UIButton *)button{
    [self animateViews];
    
    if([self usernameAndEmailValidated]){
        
        WebViewController *webView = [[WebViewController alloc] init];
        [webView setDelegate:self];
        [self.navigationController pushViewController:webView animated:YES];
    }


}

-(void)noTwitterButtonPressed:(UIButton *)button{
    [self animateViews];
    
}

-(void)animateViews{
    if([self usernameAndEmailValidated]){
        
        UIBarButtonItem *addApiAccountButton = [[UIBarButtonItem alloc] 
                                                initWithTitle:@"Sign Up" style:UIBarButtonItemStyleBordered target:self action:@selector(submitButtonPressed)];
        [self.navigationItem setRightBarButtonItem: addApiAccountButton];
        
        [passwordsAndSubmitView setHidden:NO];
        [UIView animateWithDuration:0.1 animations:^{
            [usernameEmailViewBackground setFrame:CGRectMake(10, 0, 300, 150)];
            [passwordsAndSubmitView setFrame:CGRectMake(0, 108, kScreenWidth, 280)];
        }];
        //hide both buttons
        [[usernameEmailView viewWithTag:10] setHidden:YES];
        [[usernameEmailView viewWithTag:11] setHidden:YES];
    }
}

-(void)submitButtonPressed{
    [self clearFirstResponders];
    [self validateAllSignUpData];
}

-(void)termsOfServiceButtonPressed{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kTermsOfServiceURL]];
}

#pragma mark - Gesture Recognizer Methods

-(void)backgroundTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    [self clearFirstResponders];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isKindOfClass:[UIControl class]]){
        return NO;
    }
    return YES;
}

#pragma mark - UITextField Delegate Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    //NSLog(@"tag %i began editing", textField.tag);
    if(textField.tag == 2 || textField.tag == 3){
        //[self animateTextField:textField moveUp:YES];
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    //NSLog(@"tag %i ended editing", textField.tag);
    if(textField.tag == 2 || textField.tag == 3){
        //[self animateTextField:textField moveUp:NO];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //[self clearFirstResponders];
    switch (textField.tag) {
        case TAG_USERNAME_TEXTFIELD:
            [self.emailTextField becomeFirstResponder];
            break;
        case TAG_EMAIL_TEXTFIELD:
            if([passwordsAndSubmitView isHidden]){
                [self clearFirstResponders];
            }else{
                [self.passwordTextField becomeFirstResponder];
            }
            break;
            
        case TAG_PASSWORD_TEXTFIELD:
            [self submitButtonPressed];
            break;
        default:
            return YES;
    }
    return NO;
}

-(void)animateTextField:(UITextField *)textField moveUp:(BOOL)moveUp{

    int dy = 172;
    
    float duration = 0.3f;
    
    int movement = (moveUp ? -dy : dy);
    
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:duration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case TAG_ALERTVIEW_MISSING_USERNAME:
            [self.usernameTextField becomeFirstResponder];
            break;
        case TAG_ALERTVIEW_INVALID_USERNAME:
            [self.usernameTextField becomeFirstResponder];
            break;
            
        case TAG_ALERTVIEW_MISSING_EMAIL:
            [self.emailTextField becomeFirstResponder];
            break;
            
        case TAG_ALERTVIEW_INVALID_EMAIL:
            [self.emailTextField becomeFirstResponder];
            break;
            
        case TAG_ALERTVIEW_MISSING_PASSWORD:
            [self.passwordTextField becomeFirstResponder];
            break;
            
        case TAG_ALERTVIEW_INVALID_PASSWORD:
           [self.passwordTextField becomeFirstResponder];
            break;
        default:
            break;
    }
    
}

#pragma mark - WebViewDelegate Methods
- (void)webViewController:(UIViewController *)controller didFinishAddingAccountWithInformation:(NSDictionary *)twitterParams{
    if([[twitterParams objectForKey:@"result_status"] isEqualToString:kJSONStatusSuccess]){
        twitterAccountParameters = [NSDictionary dictionaryWithDictionary:twitterParams];
        //[twitterAccountAddedBackground setHidden:NO];
    }else if([[twitterParams objectForKey:@"result_status"] isEqualToString:kJSONStatusError]){
        
        UIAlertView *signupErrorAlertView = [[UIAlertView alloc] initWithTitle:kSignUpErrorTitle message:[twitterParams objectForKey:kJSONFriendlyError] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [signupErrorAlertView show];
    }
}



@end
