//
//  Photo.m
//  slinggit
//
//  Created by Phil Beadle on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Photo.h"


@implementation Photo

@dynamic base64;
@dynamic post_id;
@dynamic creation_date;

@end
