//
//  SendMessageViewController.h
//  slinggit
//
//  Created by Phil Beadle on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendMessageViewController : UIViewController<UITextViewDelegate>{
    
}

@property (nonatomic, strong) UITextView *messageTextField;
@property (nonatomic, strong) NSString *recipientUserId;
@property (nonatomic, strong) NSString *postId;

@end
