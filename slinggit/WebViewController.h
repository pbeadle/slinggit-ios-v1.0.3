//
//  WebViewController.h
//  slinggit
//
//  Created by Phil Beadle on 5/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WebViewControllerDelegate <NSObject>
- (void)webViewController:(UIViewController *)controller didFinishAddingAccountWithInformation:(NSDictionary *)accountParams;
@end

@interface WebViewController : UIViewController<UIWebViewDelegate>{
    
}

@property (nonatomic, assign) int accountType;
@property (nonatomic, weak) id <WebViewControllerDelegate> delegate;

@end
