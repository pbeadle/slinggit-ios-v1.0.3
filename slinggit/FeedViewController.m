//
//  FeedViewController.m
//  slinggit
//
//  Created by Phil Beadle on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FeedViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "PullToRefreshView.h"
#import "Utilities.h"
#import "KeychainWrapper.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"
#import <CoreData/CoreData.h>
#import "Photo.h"
#import "ViewPostViewController.h"
#import "SVProgressHUD.h"

#define CELL_IMAGE_TAG 10
#define CELL_TITLE_TAG 11
#define CELL_SUBTITLE_TAG 12
#define CELL_PRICE_TAG 13
#define CELL_DATE_TAG 14
#define CELL_LOCATION_TAG 15
#define CELL_UNWATCH_BUTTON_TAG 16

#define TAG_LOADING_ACTIVITY_INDICATOR 16
#define TAG_LOADING_LABEL 17

#define kCellHeight 92
#define kShowMoreCellHeight 50

@interface FeedViewController (){
    BOOL reloading;
    BOOL checkForRefresh;
    PullToRefreshView *pullRefreshView;
    NSMutableArray *sourceData;
    BOOL shouldShowMoreButton;
    UIActivityIndicatorView *loadingIndicatorView;
    UIView *noPostsView;
    UILabel *noPostsLabel;
}
@end

@implementation FeedViewController

@synthesize feedTableView = _feedTableView;
@synthesize feedSegmentedControl = _feedSegmentedControl;
@synthesize searchBar = _searchBar;
@synthesize shouldLoadDataOnViewWillAppear;
@synthesize searchByUsername;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    [super loadView];
    shouldLoadDataOnViewWillAppear = YES;
    searchByUsername = NO;
    
    sourceData = [[NSMutableArray alloc] init];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.5];
    [view addSubview:backgroundImageView];
    
    loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicatorView setFrame:CGRectZero];
    [loadingIndicatorView setCenter:CGPointMake((kScreenWidth-loadingIndicatorView.frame.size.width)/2, 170)];
    [view addSubview:loadingIndicatorView];
    
    self.feedSegmentedControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 260, 32)];

    [self.feedSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"posts_icon.png"] atIndex:0 animated:NO];
    [self.feedSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"my_posts_icon.png"] atIndex:1 animated:NO];
    [self.feedSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"watch_icon.png"] atIndex:2 animated:NO];
    [self.feedSegmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    NSDictionary *categoryAttributes = [NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:13.0f] forKey:UITextAttributeFont];
    [self.feedSegmentedControl setTitleTextAttributes:categoryAttributes forState:UIControlStateNormal];
    [self.feedSegmentedControl addTarget:self
                                  action:@selector(feedSegmentedControlChanged:)
                        forControlEvents:UIControlEventValueChanged];
    [self.feedSegmentedControl setSelectedSegmentIndex:0];
    
    self.navigationItem.titleView = self.feedSegmentedControl;
    
    self.feedTableView = [[UITableView alloc] initWithFrame:CGRectMake(2, 44, kScreenWidth-4, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight-44) style:UITableViewStylePlain];
    [self.feedTableView setDelegate:self];
	[self.feedTableView setDataSource:self];
    [self.feedTableView setBackgroundColor:[UIColor clearColor]];
    [self.feedTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    pullRefreshView = [[PullToRefreshView alloc] initWithScrollView:(UIScrollView *)self.feedTableView];
    [pullRefreshView setDelegate:self];
    [self.feedTableView addSubview:pullRefreshView];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    [v setBackgroundColor:[UIColor clearColor]];
    [self.feedTableView setTableFooterView:v];
	
	[view addSubview:self.feedTableView];
    
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,kScreenWidth,44)];
    [self.searchBar setPlaceholder:@"Search by item and location"];
    [self.searchBar setTintColor:COLOR_SLINGGIT_BLUE];
    //[sBar setTranslucent:YES];
    //self.searchBar.tintColor = TEXT_BACKGROUND_COLOR;
    self.searchBar.delegate = self;
    [view addSubview:self.searchBar];
    
    noPostsView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, kScreenWidth, 100)];
    [noPostsView setUserInteractionEnabled:NO];
    [noPostsView setHidden:YES];
    
    UIImageView *postsIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"posts_tab_icon"]];
    postsIcon.contentMode = UIViewContentModeScaleAspectFit;
    [postsIcon setFrame:CGRectMake((kScreenWidth-48)/2, 30, 48, 33)];
    [postsIcon setAlpha:0.6];
    [noPostsView addSubview:postsIcon];
    
    noPostsLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 80, 280, 20)];
    [noPostsLabel setBackgroundColor:[UIColor clearColor]];
    [noPostsLabel setFont:[UIFont systemFontOfSize:18]];
    [noPostsLabel setTextAlignment:UITextAlignmentCenter];
    [noPostsLabel setText:@"No posts were found."];
    [noPostsLabel setTextColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
    [noPostsLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    [noPostsLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [noPostsView addSubview:noPostsLabel];
    
    [view addSubview:noPostsView];
    
    self.view = view;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationController.navigationBar.tintColor = COLOR_SLINGGIT_BLUE;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] init];
    [backButton setTitle:@"Posts"];
    [self.navigationItem setBackBarButtonItem:backButton];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setNoPostsLabelText];
    
    if(self.shouldLoadDataOnViewWillAppear){
        [self loadTableDataFromNewestPost];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - My Methods

-(void)animateSearchBarUp{
    [UIView animateWithDuration:0.1 animations:^{
        [self.searchBar setFrame:CGRectMake(0, -44, kScreenWidth, 44)];
    }];
}

-(void)animateSearchBarDown{
    [UIView animateWithDuration:0.1 animations:^{
        [self.searchBar setFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    }];
}

-(void)setNoPostsLabelText{
    
    switch ([self.feedSegmentedControl selectedSegmentIndex]) {
        case 0:
            [noPostsLabel setText:@"No posts were found."];
            break;
        case 1:
            if([self.searchBar text].length > 0){
                [noPostsLabel setText:@"No posts were found."];
            }else{
                [noPostsLabel setText:@"You don't have any open posts."];
            }
            break;
            
        case 2:
            [noPostsLabel setText:@"You aren't watching any posts."];
            break;
        default:
            break;
    }
}

-(void)showMoreButtonPressed{
    
    if(sourceData && [sourceData count] > 0){
        
        NSString *username = @"";
        if(searchByUsername){
            username = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USERNAME];
        }
        
        NSString *searchTerm = @"";
        if(self.searchBar.text != nil && [self.searchBar.text length] > 0){
            searchTerm = self.searchBar.text;
        }
        
        NSString *startingPostId;
        if([[sourceData objectAtIndex:([sourceData count]-1)] objectForKey:kJSONPostId]){
            startingPostId = [[sourceData objectAtIndex:([sourceData count]-1)] objectForKey:kJSONPostId];
        }else{
            startingPostId = [[sourceData objectAtIndex:([sourceData count]-1)] objectForKey:kJSONIdKeyName];
        }
        
        if(kLoggingEnabled)NSLog(@"show more with starting post id: %@", startingPostId);
        //[SVProgressHUD showWithStatus:@"Loading More"];
        [self reloadTableViewDataSourceWithOffset:[NSNumber numberWithInt:0] withStartingPostId:startingPostId withLimit:[NSNumber numberWithInt:kFeedShowLimit] forUsername:username withSearchTerm:searchTerm];
    }
}

-(void)loadTableDataFromNewestPost{
    
    NSString *username = @"";
    if(searchByUsername){
        username = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USERNAME];
    }
    
    NSString *searchTerm = @"";
    if(self.searchBar.text != nil && [self.searchBar.text length] > 0){
        searchTerm = self.searchBar.text;
    }
    
    [sourceData removeAllObjects]; 
    [self reloadTableViewDataSourceWithOffset:[NSNumber numberWithInt:0] withStartingPostId:@"" withLimit:[NSNumber numberWithInt:kFeedShowLimit] forUsername:username withSearchTerm:searchTerm];
}

-(void)loadWatchedPosts{
    [loadingIndicatorView startAnimating];
    
    [sourceData removeAllObjects]; 
    NSURL *editedGetWatchedPostsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kGetWatchedPostsURL, [Utilities commonParams]]];
    
    if(kLoggingEnabled)NSLog(@"get watched posts URL: %@", editedGetWatchedPostsUrl);
    NSMutableURLRequest *getWatchedPostsRequest = [NSMutableURLRequest requestWithURL:editedGetWatchedPostsUrl];
    [getWatchedPostsRequest setTimeoutInterval:kUrlRequestTimeout];
    [getWatchedPostsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getWatchedPostsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getWatchedPostsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        shouldShowMoreButton = NO;
        if(kLoggingEnabled)NSLog(@"get watched posts response: %@", JSON);
        [self stopLoadingAnimation];
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            self.shouldLoadDataOnViewWillAppear = NO;
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            int rowsFound = [[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue];
            if(rowsFound > 0){
                [sourceData addObjectsFromArray:[resultDictionary objectForKey:@"posts"]];
                //if(kLoggingEnabled)NSLog(@"new source data was added: %@", sourceData);
                [noPostsView setHidden:YES];
                
            }else{
                
                [noPostsView setHidden:NO];
                
            }
            
            [self.feedTableView reloadData];
            [self loadImagesForOnscreenRows];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            
            [noPostsView setHidden:NO];
            [self.feedTableView reloadData];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kGeneralProblemErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            [noPostsView setHidden:NO];
            [self.feedTableView reloadData];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        shouldShowMoreButton = NO;
        [noPostsView setHidden:NO];
        [self.feedTableView reloadData];
        [self stopLoadingAnimation];
        if(kLoggingEnabled)NSLog(@"reload watched posts connection failed: %@", error);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [getWatchedPostsOperation start];
}


- (void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar setText:@""];
    [self.searchBar resignFirstResponder];
    
    if([self.feedSegmentedControl selectedSegmentIndex] == 2){
        [self loadWatchedPosts];
    }else{
        [self loadTableDataFromNewestPost];
    }
}

- (void) reloadTableViewDataSourceWithOffset:offset withStartingPostId:startingPostId withLimit:limit forUsername:username withSearchTerm:searchTerm{
    [loadingIndicatorView startAnimating];
    NSURL *editedGetPostsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", kGetSlinggitPostDataURL, [Utilities commonParams], kJSONOffsetKeyName, offset, kJSONLimitKeyName, limit, kJSONUsernameKeyName, username, kJSONSearchTermKeyName, [Utilities stringByEncodingHTMLEntities:searchTerm], kJSONStartingPostId, startingPostId]];
    
    if(kLoggingEnabled)NSLog(@"get post data URL: %@", editedGetPostsUrl);
    NSMutableURLRequest *getPostsRequest = [NSMutableURLRequest requestWithURL:editedGetPostsUrl];
    [getPostsRequest setTimeoutInterval:kUrlRequestTimeout];
    [getPostsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getPostsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getPostsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        
        if(kLoggingEnabled)NSLog(@"get post data response: %@", JSON);
        [self stopLoadingAnimation];
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            self.shouldLoadDataOnViewWillAppear = NO;
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            int rowsFound = [[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue];
            if(rowsFound > 0){
                [sourceData addObjectsFromArray:[resultDictionary objectForKey:@"posts"]];
                //if(kLoggingEnabled)NSLog(@"new source data was added: %@", sourceData);
                [noPostsView setHidden:YES];
                
                if(rowsFound < kFeedShowLimit){
                    shouldShowMoreButton = NO;
                }else{
                    shouldShowMoreButton = YES;
                }
                
            }else{
                if([sourceData count] == 0){
                    [noPostsView setHidden:NO];
                }else{
                    [noPostsView setHidden:YES]; 
                }
                shouldShowMoreButton = NO;
            }
            
            [self.feedTableView reloadData];
            [self loadImagesForOnscreenRows];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            shouldShowMoreButton = NO;
            [noPostsView setHidden:NO];
            [self.feedTableView reloadData];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kGeneralProblemErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            shouldShowMoreButton = NO;
            [noPostsView setHidden:NO];
            [self.feedTableView reloadData];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        shouldShowMoreButton = NO;
        [noPostsView setHidden:NO];
        [self.feedTableView reloadData];
        [self stopLoadingAnimation];
        if(kLoggingEnabled)NSLog(@"reload table connection failed: %@", error);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [getPostsOperation start];
    
}

-(void)stopLoadingAnimation{
    [pullRefreshView finishedLoading]; 
    [loadingIndicatorView stopAnimating];
}

-(void)loadImagesForOnscreenRows{
    
    if(sourceData){
        for(NSIndexPath *indexPath in [self.feedTableView indexPathsForVisibleRows]){
            if(indexPath.row < [sourceData count]){
                UITableViewCell *cell = [self.feedTableView cellForRowAtIndexPath:indexPath];
                
                NSString *postId;
                if([[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONPostId]){
                    postId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONPostId];
                }else{
                    postId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName];
                }
                
                NSString *photoBase64 = [Utilities findPhotoWithPostId:[NSNumber numberWithInt:[postId intValue]]];
                if (photoBase64) {
                    NSData *imageData = [Utilities dataFromBase64String:photoBase64];
                    UIImage *cellImage = [UIImage imageWithData:imageData];
                    [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImage:cellImage];
                    //[(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImage:[UIImage imageNamed:@"arrow.png"]];
                    if(kLoggingEnabled)NSLog(@"image set from db for row: %i", indexPath.row);
                }else{
                    if(kLoggingEnabled)NSLog(@"load image data for onscreen rows - image uri: %@", [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONImageUriKeyName]);
                    if(![[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
                        NSURL *editedGetImageUrl = [NSURL URLWithString:[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONImageUriKeyName]];
                        
                        //NSURL *editedGetImageUrl = [NSURL URLWithString:@"http://netobjects.com/assets/images/icon-image-bank.png"];
                        
                        if(kLoggingEnabled)NSLog(@"get image data URL: %@", editedGetImageUrl);
                        NSMutableURLRequest *getImageRequest = [NSMutableURLRequest requestWithURL:editedGetImageUrl];
                        [getImageRequest setTimeoutInterval:kUrlRequestTimeout];
                        [getImageRequest setHTTPMethod:@"GET"];
                        
                        AFImageRequestOperation *imageRequestOperation = [AFImageRequestOperation imageRequestOperationWithRequest:getImageRequest success:^(UIImage *image) {
                            
                            [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImage:image];
                            
                            NSData *tempImageData = UIImageJPEGRepresentation(image, 1.0);
                            NSString *base64EncodedString = [Utilities newStringInBase64FromData:tempImageData];
                            NSString *postId;
                            if([[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONPostId]){
                                postId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONPostId];
                            }else{
                                postId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName];
                            }
                            
                            [Utilities saveData:base64EncodedString withPostId:[NSNumber numberWithInt:[postId intValue]]];
                            
                            if([Utilities numberOfPhotoRecords] > kPhotoDownloadStorageNumberLimit){
                                [Utilities deleteOldestPhotoRecord];
                            }
                            
                        }];
                        
                        [imageRequestOperation start];
                    }else if([Utilities getDefaultImage]){
                        [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG]  setImage:[Utilities getDefaultImage]];
                        
                    }else{
                        [Utilities downloadDefaultPlaceholderImage];
                    }
                    
                }
            }
        }
    }
    
}

-(void)unwatchPostButtonPressed:(id)sender{
    NSIndexPath *indexPath = [self.feedTableView indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    NSUInteger row = indexPath.row;
    
    [self reloadTableAndRemoveWatchedPostForRow:row];

}

-(void)reloadTableAndRemoveWatchedPostForRow:(int)row{
    [self removePostFromWatchedListForRow:row];
    [sourceData removeObjectAtIndex:row];
    [self.feedTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
    [self.feedTableView reloadData];
    [self loadImagesForOnscreenRows];
}

-(void)removePostFromWatchedListForRow:(int)row{
    
    NSString *postId;
    if([[sourceData objectAtIndex:row] objectForKey:kJSONPostId]){
        postId = [[sourceData objectAtIndex:row] objectForKey:kJSONPostId];
    }else{
        postId = [[sourceData objectAtIndex:row] objectForKey:kJSONIdKeyName];
    }
    
    NSURL *editedUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kRemoveWatchedPostURL, [Utilities commonParams], kJSONPostId, postId]];
    
    if(kLoggingEnabled)NSLog(@"remove watch post URL: %@", editedUrl);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:editedUrl];
    [urlRequest setTimeoutInterval:kUrlRequestTimeout];
    [urlRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"remove watch post response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kRemoveWatchPostErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            //error with request
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kRemoveWatchPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"remove watch post connection failed: %@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kRemoveWatchPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [requestOperation start];
}



#pragma mark - UITableview Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(sourceData && [sourceData count] > 0){
        
        if(shouldShowMoreButton){
            return [sourceData count]+1;
        }else{
            return [sourceData count];
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([sourceData count] > 0){
        if(indexPath.row == [sourceData count]){
            return kShowMoreCellHeight;
        }
        return kCellHeight;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier;
    if(indexPath.row < [sourceData count]){
        if([self.feedSegmentedControl selectedSegmentIndex] != 2){
            CellIdentifier  = @"NormalCell";
        }else{
            CellIdentifier = @"WatchedPostCell";
        }
    }else{
        CellIdentifier = @"FooterCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [self getCellContentView:CellIdentifier];
        
    }
    
    UIImageView *postBackgroundImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [postBackgroundImageView setFrame:CGRectZero];
    [cell.layer setMasksToBounds:YES];
    [cell setBackgroundView:postBackgroundImageView];
    
    if(indexPath.row < [sourceData count]){
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        /*
         UIView *selectedBackgroundView = [[UIView alloc] init];
         [selectedBackgroundView setBackgroundColor:COLOR_SLINGGIT_BLUE];
         [selectedBackgroundView.layer setCornerRadius:3.0];
         [cell setSelectedBackgroundView:selectedBackgroundView];
         */
        NSDictionary *postDictionary = [sourceData objectAtIndex:indexPath.row];
        
        if(![[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONImageUriKeyName] isEqual:[NSNull null]]){
            
            NSString *postId;
            if([[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONPostId]){
                postId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONPostId];
            }else{
                postId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName];
            }
            
            NSString *photoBase64 = [Utilities findPhotoWithPostId:[NSNumber numberWithInt:[postId intValue]]];
            if (photoBase64) {
                NSData *imageData = [Utilities dataFromBase64String:photoBase64];
                UIImage *cellImage = [UIImage imageWithData:imageData];
                [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImage:cellImage];
                //[(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImage:[UIImage imageNamed:@"arrow.png"]];
                if(kLoggingEnabled)NSLog(@"image set from db for row: %i", indexPath.row);
            }else if([Utilities getDefaultImage]){
                [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG]  setImage:[Utilities getDefaultImage]];
                
            }else{
                [Utilities downloadDefaultPlaceholderImage];
            }
            
        }else if([Utilities getDefaultImage]){
            [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG]  setImage:[Utilities getDefaultImage]];
            
        }else{
            [Utilities downloadDefaultPlaceholderImage];
        }
        
        UILabel *dateTextView = (UILabel *)[cell.contentView viewWithTag:CELL_DATE_TAG];
        
        [dateTextView setText:[Utilities getElaspsedTimeFromPostDictionary:postDictionary]];
        
        NSString *priceString = [[postDictionary objectForKey:kJSONPriceKeyName] stringValue];
        
        if([priceString length] > 3){
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [numberFormatter setPositiveFormat:@"#,###"];
            
            priceString = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSNumber numberWithInt:[priceString intValue]]]];
        }
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_PRICE_TAG] setText:[NSString stringWithFormat:@"%@", priceString]];
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setText:[postDictionary objectForKey:kJSONHashTagPrefixKeyName]];
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_LOCATION_TAG] setText:[postDictionary objectForKey:kJSONLocationKeyName]];
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setText:[postDictionary objectForKey:kJSONPostContentKeyName]];
        
    }else{
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
        if(indexPath.row == [sourceData count] && shouldShowMoreButton && !pullRefreshView.isLoading){
            [pullRefreshView beginLoading];
            [self showMoreButtonPressed];
            if(kLoggingEnabled)NSLog(@"shore more...");
        }
    }
    
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {	
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row < [sourceData count]){
        ViewPostViewController *viewPostViewController = [[ViewPostViewController alloc] init];
        [viewPostViewController setPostDictionary:[sourceData objectAtIndex:indexPath.row]];
        if([self.feedSegmentedControl selectedSegmentIndex] == 2){
            [viewPostViewController setShouldShowWatchPostButton:NO];
        }else{
            [viewPostViewController setShouldShowWatchPostButton:YES];
        }
        [self.navigationController pushViewController:viewPostViewController animated:YES];
    }
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(editingStyle == UITableViewCellEditingStyleDelete){
        
        [self reloadTableAndRemoveWatchedPostForRow:indexPath.row];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self.feedSegmentedControl selectedSegmentIndex] == 2){
        
        return YES;
    }
    
    return NO;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellEditingStyleDelete;
    
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"Unwatch";
}

-(UITableViewCell *)getCellContentView:(NSString *)CellIdentifier{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if([CellIdentifier isEqualToString:@"NormalCell"] || [CellIdentifier isEqualToString:@"WatchedPostCell"]){
        
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(6, 6, 80, 80)];
        [image setTag:CELL_IMAGE_TAG];
        [image.layer setBorderWidth:1.0];
        [image.layer setBorderColor:[[UIColor colorWithWhite:0.0 alpha:0.3] CGColor]];
        [image.layer setCornerRadius:7.0];
        [image.layer setMasksToBounds:YES];
        [cell.contentView addSubview:image];
        
        UILabel *dollarSignLabel = [[UILabel alloc] initWithFrame:CGRectMake(96, 10, 10, 14)];
        [dollarSignLabel setBackgroundColor:[UIColor clearColor]];
        [dollarSignLabel setTextColor:COLOR_SLINGGIT_BLUE];
        [dollarSignLabel setTextAlignment:UITextAlignmentLeft];
        [dollarSignLabel setFont:[UIFont systemFontOfSize:13.0]];
        [dollarSignLabel setShadowColor:[UIColor whiteColor]];
        dollarSignLabel.shadowOffset = CGSizeMake(0.0, 1.0);
        [dollarSignLabel setText:@"$"];
        [cell.contentView addSubview:dollarSignLabel];
        
        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(104, 10, 68, 16)];
        [price setBackgroundColor:[UIColor clearColor]];
        [price setTextColor:COLOR_SLINGGIT_BLUE];
        [price setTextAlignment:UITextAlignmentLeft];
        [price setFont:[UIFont boldSystemFontOfSize:17.0]];
        [price setShadowColor:[UIColor whiteColor]];
        price.shadowOffset = CGSizeMake(0.0, 1.0);
        [price setTag:CELL_PRICE_TAG];
        [cell.contentView addSubview:price];
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(196, 10, 112, 16)];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setTextAlignment:UITextAlignmentRight];
        [date setTag:CELL_DATE_TAG];
        [date setTextColor:COLOR_SLINGGIT_BLUE];
        [date setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        date.shadowOffset = CGSizeMake(0.0, 1.0);
        [date setFont:[UIFont systemFontOfSize:kDateFontSize]];
        [cell.contentView addSubview:date];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(96, 28, 210, 20)];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTag:CELL_TITLE_TAG];
        [title setTextColor:[UIColor darkTextColor]];
        [title setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        [title setShadowOffset:CGSizeMake(0.0, 1.0)];
        [title setFont:[UIFont boldSystemFontOfSize:15]];
        [cell.contentView addSubview:title];
        
        UIImageView *locationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_icon.png"]];
        [locationIcon setContentMode:UIViewContentModeScaleAspectFit];
        [locationIcon setFrame:CGRectMake(92, 48, 16, 16)];
        [cell.contentView addSubview:locationIcon];
        
        UILabel *location = [[UILabel alloc] initWithFrame:CGRectMake(108, 46, 210, 20)];
        [location setBackgroundColor:[UIColor clearColor]];
        [location setTag:CELL_LOCATION_TAG];
        [location setTextColor:[UIColor darkTextColor]];
        [location setShadowColor:[UIColor whiteColor]];
        [location setShadowOffset:CGSizeMake(0.0, 1.0)];
        [location setFont:[UIFont systemFontOfSize:14]];
        [cell.contentView addSubview:location];
        
        UILabel *subtitle = [[UILabel alloc] initWithFrame:CGRectMake(96, 56, 218, 40)];
        [subtitle setBackgroundColor:[UIColor clearColor]];
        [subtitle setNumberOfLines:1];
        //[subtitle sizeToFit];
        [subtitle setTag:CELL_SUBTITLE_TAG];
        [subtitle setFont:[UIFont systemFontOfSize:13]];
        [subtitle setTextColor:[UIColor darkGrayColor]];
        [cell.contentView addSubview:subtitle];
        
        if([CellIdentifier isEqualToString:@"WatchedPostCell"]){
            UIButton *unwatchButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [unwatchButton setTag:CELL_UNWATCH_BUTTON_TAG];
            [unwatchButton setImage:[UIImage imageNamed:@"unwatch_icon.png"] forState:UIControlStateNormal];
            //[unwatchButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
            [unwatchButton setFrame:CGRectMake(kScreenWidth-48, (kCellHeight-48)/2, 48, 48)];
            [unwatchButton addTarget:self action:@selector(unwatchPostButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:unwatchButton];
        }
        
    }else{
        UIActivityIndicatorView *loadingActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [loadingActivityIndicator setFrame:CGRectMake(80, 0, 50, kShowMoreCellHeight)];
        [loadingActivityIndicator setTag:TAG_LOADING_ACTIVITY_INDICATOR];
        [loadingActivityIndicator startAnimating];
        [cell.contentView addSubview:loadingActivityIndicator];
        
        UILabel *showMoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kShowMoreCellHeight)];
        [showMoreLabel setTag:TAG_LOADING_LABEL];
        [showMoreLabel setTextAlignment:UITextAlignmentCenter];
        [showMoreLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
        [showMoreLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        showMoreLabel.shadowOffset = CGSizeMake(0.0, 1.0);
        [showMoreLabel setTextColor:COLOR_SLINGGIT_BLUE];
        [showMoreLabel setText:@"Loading"];
        [showMoreLabel setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:showMoreLabel];
        
    }
    
    return cell;
}

#pragma mark - UISegmentedControl methods
-(void)feedSegmentedControlChanged:(UISegmentedControl *)feedSegmentedControl{
    [noPostsView setHidden:YES];
    [sourceData removeAllObjects];
    [self.feedTableView reloadData];
    
    [self setNoPostsLabelText];
    
    switch ([feedSegmentedControl selectedSegmentIndex]) {
        case 0:
            [self selectAllPostsSegment];
            break;
        case 1:
            [self selectMyPostsSegment];
            break;
            
        case 2:
            [self selectWatchedPostsSegment];
            break;
            
        default:
            break;
    }
    
    
}
-(void)selectAllPostsSegment{
    searchByUsername = NO;
    //[self.searchBar setHidden:NO];
    [self animateSearchBarDown];
    [self loadTableDataFromNewestPost];
    [self.feedTableView setFrame:CGRectMake(0, 44, kScreenWidth, kScreenHeightNoStatusBar-kTabBarHeight-kNavBarHeight-44)];
}

-(void)selectMyPostsSegment{
    searchByUsername = YES;
    [self.searchBar resignFirstResponder];
    [self.searchBar setText:@""];
    //[self.searchBar setHidden:YES];
    [self animateSearchBarUp];
    [self loadTableDataFromNewestPost];
    [self.feedTableView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kTabBarHeight-kNavBarHeight)];
}

-(void)selectWatchedPostsSegment{
    searchByUsername = NO;
    [self.searchBar resignFirstResponder];
    [self.searchBar setText:@""];
    //[self.searchBar setHidden:YES];
    [self animateSearchBarUp];
    [self.feedTableView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kTabBarHeight-kNavBarHeight)];
    [self loadWatchedPosts];
}

#pragma mark - UISearchBar Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton:YES animated:YES];
    
    [sourceData removeAllObjects];
    [self.feedTableView reloadData];
    [self loadTableDataFromNewestPost];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar setText:@""];
    [self.searchBar resignFirstResponder];
    
    [sourceData removeAllObjects];
    [self.feedTableView reloadData];
    [self loadTableDataFromNewestPost];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}

#pragma mark -
#pragma mark ScrollView Overrides


// Load images for all onscreen rows when scrolling is finished

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(![self.feedTableView isDragging]){
        [self loadImagesForOnscreenRows];
    }
}


@end
