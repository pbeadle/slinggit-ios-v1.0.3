//
//  TransparentToolbar.h
//  WineChiller
//
//  Created by Render on 12/1/11.
//  Copyright (c) 2011 iWineGuild. All rights reserved.
//


#import "TransparentToolbar.h"

@implementation TransparentToolbar

-(void)drawRect:(CGRect)rect{
    [[UIColor clearColor] set];
    CGContextFillRect(UIGraphicsGetCurrentContext(), rect);
}

-(void) applyTranslucentBackground{
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    self.translucent = YES;
}

-(id)init{
    self = [super init];
    [self applyTranslucentBackground];
    return self;
}

-(id)initWithFrame:(CGRect) frame{
    self = [super initWithFrame:frame];
    [self applyTranslucentBackground];
    return self;
}



@end