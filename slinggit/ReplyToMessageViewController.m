//
//  SendMessageViewController.m
//  slinggit
//
//  Created by Phil Beadle on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ReplyToMessageViewController.h"
#import "Constants.h"
#import "Utilities.h"
#import "SVProgressHUD.h"
#import "AFJSONRequestOperation.h"

@interface ReplyToMessageViewController (){
    UIView *holderView;
}

@end

@implementation ReplyToMessageViewController

@synthesize messageTextField;
@synthesize messageDictionary;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kTabBarHeight-kNavBarHeight)];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    [singleTap setDelegate:self];
    [singleTap setCancelsTouchesInView:NO];
    [view addGestureRecognizer:singleTap];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.5];
    [view addSubview:backgroundImageView];
    
    holderView = [[UIView alloc] initWithFrame:CGRectMake(0, -126, kScreenWidth, view.frame.size.height+126)];
    
    UIView *replyMessageBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 6, 300, 120)];
    
    UIImageView *textInputImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [textInputImageView setFrame:CGRectMake(0, 0, replyMessageBackgroundView.frame.size.width, replyMessageBackgroundView.frame.size.height)];
    [replyMessageBackgroundView addSubview:textInputImageView];
    
    self.messageTextField = [[UITextView alloc] initWithFrame:CGRectMake(2, 2, replyMessageBackgroundView.frame.size.width-2, replyMessageBackgroundView.frame.size.height-4)];
    [self.messageTextField setDelegate:self];
    [self.messageTextField setText:@"Reply Message"];
    [self.messageTextField setTextColor:[UIColor lightGrayColor]];
    [self.messageTextField setBackgroundColor:[UIColor clearColor]];
    [self.messageTextField setFont:[UIFont systemFontOfSize:14.0]];
    [self.messageTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
    [self.messageTextField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [replyMessageBackgroundView addSubview:self.messageTextField];
    
    [holderView addSubview:replyMessageBackgroundView];
    
    UIView *fromMessageBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 132, 300, 230)];
    
    UIImageView *textBackgroundImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [textBackgroundImageView setFrame:CGRectMake(0, 0, fromMessageBackgroundView.frame.size.width, fromMessageBackgroundView.frame.size.height+2)];
    [fromMessageBackgroundView addSubview:textBackgroundImageView];
    
    UILabel *fromDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 24)];
    if([messageDictionary objectForKey:kJSONContactInfoKeyName] && ![[messageDictionary objectForKey:kJSONContactInfoKeyName] isEqual:[NSNull null]]){
        NSDictionary *contactInfoArray = [messageDictionary objectForKey:kJSONContactInfoKeyName];
        if([contactInfoArray objectForKey:kJSONUsernameKeyName] && ![[contactInfoArray objectForKey:kJSONUsernameKeyName] isEqual:[NSNull null]]){
            [fromDataLabel setText:[contactInfoArray objectForKey:kJSONUsernameKeyName]];
        }else{
            [fromDataLabel setText:[contactInfoArray objectForKey:kJSONEmailKeyName]];
        }
    }
    
    [fromDataLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [fromDataLabel setBackgroundColor:[UIColor clearColor]];
    [fromDataLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    fromDataLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    [fromDataLabel setFont:[UIFont systemFontOfSize:14.0]];
    [fromMessageBackgroundView addSubview:fromDataLabel];
    
    UILabel *itemDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 18, fromMessageBackgroundView.frame.size.width-50, 24)];
    [itemDataLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [itemDataLabel setBackgroundColor:[UIColor clearColor]];
    [itemDataLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    itemDataLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    [itemDataLabel setFont:[UIFont systemFontOfSize:14.0]];
    [fromMessageBackgroundView addSubview:itemDataLabel];
    
    if(![[messageDictionary objectForKey:kJSONSourceObjectDataKeyName] isEqual:[NSNull null]]){
        NSDictionary *sourceObjectDataDictionary = [messageDictionary objectForKey:kJSONSourceObjectDataKeyName];
        if(![[sourceObjectDataDictionary objectForKey:kJSONHashTagPrefixKeyName] isEqual:[NSNull null]]){
            [itemDataLabel setText:[sourceObjectDataDictionary objectForKey:kJSONHashTagPrefixKeyName]];
        }
    }
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(192, 0, 100, 24)];
    [dateLabel setText:[Utilities getElaspsedTimeFromPostDictionary:messageDictionary]];
    [dateLabel setTextAlignment:UITextAlignmentRight];
    [dateLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [dateLabel setBackgroundColor:[UIColor clearColor]];
    [dateLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    dateLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    [dateLabel setFont:[UIFont systemFontOfSize:kDateFontSize]];
    [fromMessageBackgroundView addSubview:dateLabel];
    
    UIImageView *loginTextLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"text_input_line.png"]];
    [loginTextLine setFrame:CGRectMake(4, 40, 292, 2)];
    [fromMessageBackgroundView addSubview:loginTextLine];
    
    UITextView *messageTextView = [[UITextView alloc] initWithFrame:CGRectMake(2, 41, fromMessageBackgroundView.frame.size.width-4, fromMessageBackgroundView.frame.size.height-40)];
    [messageTextView setScrollEnabled:YES];
    [messageTextView setFont:[UIFont systemFontOfSize:14.0]];
    [messageTextView setTextColor:[UIColor darkGrayColor]];
    [messageTextView setBackgroundColor:[UIColor clearColor]];
    [messageTextView setText:[messageDictionary objectForKey:kJSONBodyKeyName]];
    //[messageTextView setText:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nulla ipsum. Integer eu ante sem. Aliquam diam magna, auctor in adipiscing sagittis, tempus eu augue. In hac habitasse platea dictumst. Sed fermentum hendrerit viverra. Phasellus consectetur cursus blandit. Vivamus dignissim nullam. and on and on and I can't Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nulla ipsum. Integer eu ante sem. Aliquam diam magna, auctor in adipiscing sagittis, tempus eu augue. In hac habitasse platea dictumst. Sed fermentum hendrerit viverra. Phasellus consectetur cursus blandit. Vivamus dignissim nullam. and on and on and I can't"];
    [messageTextView setEditable:NO];
    [fromMessageBackgroundView addSubview:messageTextView];
    
    [holderView addSubview:fromMessageBackgroundView];
    
    UIButton *replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [replyButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
    [replyButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [replyButton setFrame:CGRectMake(20, 436, 280, 46)];
    [replyButton setTitle:@"Reply" forState:UIControlStateNormal];
    [replyButton addTarget:self action:@selector(replyButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [replyButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    replyButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [holderView addSubview:replyButton];

    [view addSubview:holderView];
    self.view = view;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Reply"];
    [self.navigationController.navigationBar setTintColor:COLOR_SLINGGIT_BLUE];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - My Methods
-(void)clearFirstResponders{
    [messageTextField resignFirstResponder];
}

-(void)replyButtonPressed{
    
    if([[messageDictionary objectForKey:kJSONSenderUserIdKeyName] isEqual:[NSNull null]]){
        NSDictionary *contactInfoArray = [messageDictionary objectForKey:kJSONContactInfoKeyName];
        if(contactInfoArray && ![contactInfoArray isEqual:[NSNull null]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@", [contactInfoArray objectForKey:kJSONEmailKeyName]]]];
        }
    }else{
        [UIView animateWithDuration:0.1 animations:^{
            [holderView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
            
        }];
        [messageTextField becomeFirstResponder];
        
        UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStylePlain target:self action:@selector(sendButtonPressed)];
        [sendButton setEnabled:NO];
        [self.navigationItem setRightBarButtonItem:sendButton];
    }
    
}

#pragma mark - UITextView Delegate Methods
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if([textView.text isEqualToString:@"Reply Message"]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        
    }
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        [textView setTextColor:[UIColor lightGrayColor]];
        [textView setText:@"Reply Message"];
        UIBarButtonItem *sendButton = [self.navigationItem rightBarButtonItem];
        [sendButton setEnabled:NO];
    }
    
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    UIBarButtonItem *sendButton = [self.navigationItem rightBarButtonItem];
    int maxLength = kSendMessageFieldLimit;
    if([[textView text] length] == 0 && text.length == 0){
        [textView setTextColor:[UIColor lightGrayColor]];
        [textView setText:@"Reply Message"];
        [textView resignFirstResponder];
        
        [sendButton setEnabled:NO];
    }else{

        [sendButton setEnabled:YES];
    }
    
    if(range.length > text.length){
        return YES;
    }else if([[textView text] length] + text.length > maxLength){
        return NO;
    }
    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView{
    if([[textView text] length] == 0){
        UIBarButtonItem *sendButton = [self.navigationItem rightBarButtonItem];
        [sendButton setEnabled:NO];
    }
}

#pragma mark - Button Presses
-(void)sendButtonPressed{
    
    [SVProgressHUD showWithStatus:@"Sending" maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *encodedMessage = [Utilities stringByEncodingHTMLEntities:self.messageTextField.text];
    
    NSURL *editedSendMessageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@", kReplyToMessageURL, [Utilities commonParams], kJSONParentMessageIdKeyName, [messageDictionary objectForKey:kJSONIdKeyName], kJSONBodyKeyName, encodedMessage]];
    
    if(kLoggingEnabled)NSLog(@"edited reply to message URL: %@", editedSendMessageUrl);
    NSMutableURLRequest *sendMessageRequest = [NSMutableURLRequest requestWithURL:editedSendMessageUrl];
    [sendMessageRequest setTimeoutInterval:kUrlRequestTimeout];
    [sendMessageRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *sendMessageOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:sendMessageRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"reply to message message response: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            if(kLoggingEnabled)NSLog(@"reply to message message response: %@", resultDictionary);
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary valueForKeyPath:kJSONFriendlyError];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kSendErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kSendErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"reply to message failure error:\n%@", error);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kSendErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
    }];
    
    [sendMessageOperation start];
    
}

#pragma mark - Gesture Recognizer Methods

-(void)backgroundTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    [self clearFirstResponders];
}

@end
