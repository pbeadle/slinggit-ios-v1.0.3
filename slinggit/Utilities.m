//
//  Utilities.m
//  slinggit
//
//  Created by Phil Beadle on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utilities.h"
#import "Constants.h"
#import "KeychainWrapper.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#include "Photo.h"
#include "AppDelegate.h"
#include "AFJSONRequestOperation.h"
#include "AFImageRequestOperation.h"

@implementation Utilities

+(NSString *)getDeviceID{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return uuidString;
}

+(NSString *)stringByEncodingHTMLEntities:(NSString *)text {
	return (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)text, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
}

+(NSString *) platformString{
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    //if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1st Gen";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2nd Gen";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3rd Gen";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4th Gen";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad-3G (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad-3G (4G)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad-3G (4G)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return @"iOS Device";
    
}

+(NSString *)getStateWithFullStateName:(NSString *)fullStateName{
    NSDictionary *stateDictionary = [NSDictionary dictionaryWithObjects:
                                     [NSArray arrayWithObjects:
                                      @"AL", 
                                      @"AK", 
                                      @"AZ", 
                                      @"AR", 
                                      @"CA", 
                                      @"CO", 
                                      @"CT", 
                                      @"DE", 
                                      @"DC", 
                                      @"FL", 
                                      
                                      @"GA", 
                                      @"HI", 
                                      @"ID", 
                                      @"IL", 
                                      @"IN", 
                                      @"IA", 
                                      @"KS", 
                                      @"KY", 
                                      @"LA", 
                                      @"ME", 
                                      
                                      @"MD", 
                                      @"MA", 
                                      @"MI", 
                                      @"MN", 
                                      @"MS", 
                                      @"MO", 
                                      @"MT", 
                                      @"NE", 
                                      @"NV", 
                                      @"NH", 
                                      
                                      @"NJ", 
                                      @"NM", 
                                      @"NY", 
                                      @"NC", 
                                      @"ND", 
                                      @"OH", 
                                      @"OK", 
                                      @"OR", 
                                      @"PA", 
                                      @"RI", 
                                      
                                      @"SC", 
                                      @"SD", 
                                      @"TN", 
                                      @"TX",
                                      @"UT",
                                      @"VT", 
                                      @"VA", 
                                      @"WA", 
                                      @"WV",
                                      @"WI",
                                      
                                      @"WY", 
                                      @"AS",
                                      @"GU", 
                                      @"MP", 
                                      @"PR", 
                                      @"VI", nil] 
                                     
                                                                forKeys:[NSArray arrayWithObjects:
                                                                         @"alabama", 
                                                                         @"alaska", 
                                                                         @"arizona", 
                                                                         @"arkansas", 
                                                                         @"california",
                                                                         @"colorado",
                                                                         @"connecticut",
                                                                         @"delaware",
                                                                         @"districtofcolumbia", 
                                                                         @"florida",
                                                                         
                                                                         @"georgia",
                                                                         @"hawaii", 
                                                                         @"idaho", 
                                                                         @"illinois", 
                                                                         @"indiana",
                                                                         @"iowa", 
                                                                         @"kansas",
                                                                         @"kentucky",
                                                                         @"louisiana",
                                                                         @"maine", 
                                                                         
                                                                         @"maryland",
                                                                         @"massachusetts", 
                                                                         @"michigan", 
                                                                         @"minnesota",
                                                                         @"mississippi", 
                                                                         @"missouri", 
                                                                         @"montana", 
                                                                         @"nebraska",
                                                                         @"nevada", 
                                                                         @"newhampshire",
                                                                         
                                                                         @"newjersey", 
                                                                         @"newmexico", 
                                                                         @"newyork", 
                                                                         @"northcarolina",
                                                                         @"northdakota",
                                                                         @"ohio", 
                                                                         @"oklahoma",
                                                                         @"oregon", 
                                                                         @"pennsylvania", 
                                                                         @"rhodeisland",
                                                                         
                                                                         @"southcarolina",
                                                                         @"southdakota",
                                                                         @"tennessee",
                                                                         @"texas", 
                                                                         @"utah", 
                                                                         @"vermont", 
                                                                         @"virginia",
                                                                         @"washington",
                                                                         @"westvirginia",
                                                                         @"wisconsin", 
                                                                         
                                                                         @"wyoming", 
                                                                         @"americansamoa", 
                                                                         @"guam", 
                                                                         @"northernmarianaislands",
                                                                         @"puertorico", 
                                                                         @"virginislands", nil]];
    
    NSString *stateString;
    if([stateDictionary objectForKey:[[fullStateName lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""]]){
        stateString = [NSString stringWithString:[stateDictionary objectForKey:[[fullStateName lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    }
    
    return stateString;
}

+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndPDFContext();
    return newImage;
}

+(NSString *)commonParams{
    NSString *state = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_UNIQUE_ID];
    NSString *encodedDeviceName = [self stringByEncodingHTMLEntities:[self platformString]];
    NSString *mobileAuthToken;
    if([KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_TOKEN_KEY]){
        mobileAuthToken = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_TOKEN_KEY];
    }else{
        mobileAuthToken = @"";
    }
    NSString *params = [NSString stringWithFormat:@"state=%@&device_name=%@&%@=%@&%@=%@", state, encodedDeviceName, kMobileAuthTokenKeyName, mobileAuthToken, kAccessTokenKeyName, kAccessToken];
    
    return params;
}



+ (NSString *)newStringInBase64FromData:(NSData *)data
{
    static char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
    NSMutableString *dest = [[NSMutableString alloc] initWithString:@""];
    unsigned char * working = (unsigned char *)[data bytes];
    int srcLen = [data length];
    
    // tackle the source in 3's as conveniently 4 Base64 nibbles fit into 3 bytes
    for (int i=0; i<srcLen; i += 3)
    {
        // for each output nibble
        for (int nib=0; nib<4; nib++)
        {
            // nibble:nib from char:byt
            int byt = (nib == 0)?0:nib-1;
            int ix = (nib+1)*2;
            
            if (i+byt >= srcLen) break;
            
            // extract the top bits of the nibble, if valid
            unsigned char curr = ((working[i+byt] << (8-ix)) & 0x3F);
            
            // extract the bottom bits of the nibble, if valid
            if (i+nib < srcLen) curr |= ((working[i+nib] >> ix) & 0x3F);
            
            [dest appendFormat:@"%c", base64[curr]];
        }
    }
    
    return dest;
    
}

+ (NSData *)dataFromBase64String: (NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true; 
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}

+(void) saveData:(NSString *)base64PhotoString withPostId:(NSNumber *)postId
{
    AppDelegate *appDelegate = 
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *photo = [NSEntityDescription
                              insertNewObjectForEntityForName:@"Photo"
                              inManagedObjectContext:context];
    [photo setValue:base64PhotoString forKey:@"base64"];
    [photo setValue:postId forKey:@"post_id"];
    [photo setValue:[NSDate date] forKey:@"creation_date"];
    
    NSError *error;
    [context save:&error];
    
}

+(int) numberOfPhotoRecords{
    AppDelegate *appDelegate = 
    [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = 
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = 
    [NSEntityDescription entityForName:@"Photo" 
                inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request 
                                              error:&error];
    return [objects count];
}

/*
 -(void)deleteRecordWithCreationDate:(NSString *)creationDate{
 AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
 NSManagedObjectContext *context = [appDelegate managedObjectContext];
 NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:context];
 NSFetchRequest *request = [[NSFetchRequest alloc] init];
 [request setEntity:entityDesc];
 NSPredicate *pred = [NSPredicate predicateWithFormat:@"(creation_date = %@)", creationDate];
 [request setPredicate:pred];
 NSManagedObject *matches = nil;
 NSError *error;
 NSArray *objects = [context executeFetchRequest:request error:&error];
 if ([objects count] != 0) {
 
 matches = [objects objectAtIndex:0];
 [context deleteObject:matches];
 
 NSError *error;
 [context save:&error];
 
 if(kLoggingEnabled)NSLog(@"deleted match data");
 }
 }
 */

+(void)deleteAllPhotoRecords{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];

    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if ([objects count] != 0) {
        
        for(NSManagedObject *photoObject in objects){
            [context deleteObject:photoObject];
            if(kLoggingEnabled)NSLog(@"deleted photo object");
        }
        
        NSError *error;
        [context save:&error];
        
        
    }
}

+(void)deleteOldestPhotoRecord{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:context];
    [request setEntity:entityDesc];
    
    [request setResultType:NSDictionaryResultType];
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath:@"creation_date"];

    NSExpression *minExpression = [NSExpression expressionForFunction:@"min:" arguments:[NSArray arrayWithObject:keyPathExpression]];
    
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];

    [expressionDescription setName:@"minDate"];
    [expressionDescription setExpression:minExpression];
    [expressionDescription setExpressionResultType:NSDateAttributeType];
    
    [request setPropertiesToFetch:[NSArray arrayWithObject:expressionDescription]];

    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        // error
    }
    else {
        if ([objects count] > 0) {
            if(kLoggingEnabled)NSLog(@"object count: %i & Minimum date: %@", [objects count], [[objects objectAtIndex:0] valueForKey:@"minDate"]);
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:context];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDesc];
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(creation_date = %@)", [[objects objectAtIndex:0] valueForKey:@"minDate"]];
            [request setPredicate:pred];
            NSManagedObject *matches = nil;
            NSError *error;
            NSArray *objects = [context executeFetchRequest:request error:&error];
            if ([objects count] != 0) {
                
                matches = [objects objectAtIndex:0];
                [context deleteObject:matches];
                
                NSError *error;
                [context save:&error];
                
                if(kLoggingEnabled)NSLog(@"deleted match data");
            }
        }
    }
}

+ (NSString *) findPhotoWithPostId:(NSNumber *)postId
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(post_id = %@)", postId];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if ([objects count] != 0) {
        matches = [objects objectAtIndex:0];
        NSString *photoBase64 = [matches valueForKey:@"base64"];
        //if(kLoggingEnabled)NSLog(@" found match data: %@", photoBase64);
        return photoBase64;
    }
    
    return nil;
    
}

+(NSString*)getElaspsedTimeFromPostDictionary:(NSDictionary *)postDictionary{
    
    NSDateFormatter *dateFormatterUTC = [[NSDateFormatter alloc] init];
    [dateFormatterUTC setDateFormat:@"MM-dd-yyyy HH:mm"];
    [dateFormatterUTC setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *createdAtDateUTC = [dateFormatterUTC dateFromString:[NSString stringWithFormat:@"%@ %@", [postDictionary objectForKey:@"created_at_date"], [postDictionary objectForKey:@"created_at_time"]]];
    
    NSDateFormatter *dateTimeFormatterLocal = [[NSDateFormatter alloc] init];
    [dateTimeFormatterLocal setTimeZone:[NSTimeZone systemTimeZone]];
    [dateTimeFormatterLocal setDateFormat:@"MMM d"];
    
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval postTimeDifference = [currentDate timeIntervalSinceDate:createdAtDateUTC];
    int hours = floor(postTimeDifference/3600);
    int minutes = floor(postTimeDifference/60);
    //if(kLoggingEnabled)NSLog(@"hours since post: %i", hours);
    if(hours == 0){
        return [NSString stringWithFormat:@"%im ago", minutes];
    }else if(hours < 12){
        return [NSString stringWithFormat:@"%ih ago", hours];
    }else{
        return [dateTimeFormatterLocal stringFromDate:createdAtDateUTC];
        
    }
}

+(UIEdgeInsets)getEdgeInsetsForImage:(UIImage *)image{
    return UIEdgeInsetsMake((image.size.height - 1)/2, (image.size.width - 1)/2, (image.size.height - 1)/2, (image.size.width - 1)/2);
}

+(void)markMessageRead:(NSString *)messageId{
    
    NSURL *editedMarkMessageReadUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kMarkMessageReadURL, [Utilities commonParams], kJSONMessageIdsKeyName, messageId]];
    
    if(kLoggingEnabled)NSLog(@"mark message read URL: %@", editedMarkMessageReadUrl);
    NSMutableURLRequest *markMessageReadRequest = [NSMutableURLRequest requestWithURL:editedMarkMessageReadUrl];
    [markMessageReadRequest setTimeoutInterval:kUrlRequestTimeout];
    [markMessageReadRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *markMessageReadOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:markMessageReadRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"mark message read response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
            [Utilities forceLoadMessageDataOnViewWillAppear];
            
        }else{
            //error with request
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if(kLoggingEnabled)NSLog(@"mark message read connection failed: %@", error);
        
    }];
    
    [markMessageReadOperation start];
    
}

+(void)downloadDefaultPlaceholderImage{
    
    NSURL *editedGetImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kGetPostImageURL, [Utilities commonParams], kJSONImageStyleKeyName, kJSONImageStyle300x300]];
    
    if(kLoggingEnabled)NSLog(@"get default image URL: %@", editedGetImageUrl);
    NSMutableURLRequest *getImageRequest = [NSMutableURLRequest requestWithURL:editedGetImageUrl];
    [getImageRequest setTimeoutInterval:kUrlRequestTimeout];
    [getImageRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getImageOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getImageRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"get default image response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSURL *editedDownloadImageUrl = [NSURL URLWithString:[resultDictionary objectForKey:kJSONImageUriKeyName]];
            if(kLoggingEnabled)NSLog(@"download image uri: %@", editedDownloadImageUrl);
            
            NSMutableURLRequest *downloadRequest = [NSMutableURLRequest requestWithURL:editedDownloadImageUrl];
            [downloadRequest setTimeoutInterval:kUrlRequestTimeout];
            [getImageRequest setHTTPMethod:@"POST"];
            
            AFImageRequestOperation *downloadOperation = [AFImageRequestOperation imageRequestOperationWithRequest:downloadRequest success:^(UIImage *image) {
                [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:NO];
                [Utilities saveDefaultImage:image];
                
            }];
            
            [downloadOperation start];
            
            
        }else{
            //error with request
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if(kLoggingEnabled)NSLog(@"get default image connection failed: %@", error);
        
    }];
    
    [getImageOperation start];
    
    
}

+ (void)saveDefaultImage:(UIImage*)image {
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"default8642.jpg"]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
}

+ (UIImage*)getDefaultImage {
    
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"default8642.jpg"]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}

/*
 +(void)getApiAccounts{
 NSURL *editedGetApiAccountsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kGetUserApiAccountsURL, [Utilities commonParams]]];
 if(kLoggingEnabled)NSLog(@"edited get api accounts url: %@", editedGetApiAccountsUrl);
 NSMutableURLRequest *getApiAccountsRequest = [NSMutableURLRequest requestWithURL:editedGetApiAccountsUrl];
 [getApiAccountsRequest setHTTPMethod:@"POST"];
 
 AFJSONRequestOperation *getApiAccountsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getApiAccountsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
 
 if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
 if(kLoggingEnabled)NSLog(@"load api accounts success response JSON: %@", JSON);
 NSDictionary *resultDictionary = [JSON valueForKey:kJSONResult];
 
 if([resultDictionary objectForKey:kJSONRowsFoundKeyName] && [[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue] > 0){
 
 NSMutableArray *sourceData = [NSMutableArray arrayWithArray:[resultDictionary objectForKey:kJSONApiAccountsKeyName]];
 
 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
 [prefs setObject:sourceData forKey:PREFS_API_ACCOUNTS_ARRAY];
 [prefs synchronize];
 
 if(kLoggingEnabled)NSLog(@"source data loaded: %@", sourceData);
 
 }
 
 }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
 if(kLoggingEnabled)NSLog(@"load api accounts failure JSON: %@", JSON);
 
 NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
 NSString *errorMessage;
 if([resultDictionary valueForKey:kJSONFriendlyError]){
 errorMessage = [NSString stringWithString:[resultDictionary valueForKey:kJSONFriendlyError]];
 }else{
 errorMessage = [NSString stringWithString:kConnectionErrorText];
 }
 
 }else{
 
 
 }
 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
 
 if(kLoggingEnabled)NSLog(@"load api accounts error: %@", error);
 
 }];
 
 [getApiAccountsOperation start];
 }
 */

+(void)forceLoadMessageDataOnViewWillAppear{
    AppDelegate *myAppDelegate = [[UIApplication sharedApplication] delegate];
    [myAppDelegate loadMessageDataOnViewWillAppear];
}

+(void)forceLoadPostDataOnViewWillAppearAndShowMyPosts:(bool)showMyPosts{
    AppDelegate *myAppDelegate = [[UIApplication sharedApplication] delegate];
    [myAppDelegate loadPostDataOnViewWillAppear:showMyPosts];
}

@end
