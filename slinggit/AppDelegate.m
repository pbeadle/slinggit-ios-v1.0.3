//
//  AppDelegate.m
//  slinggit
//
//  Created by Phil Beadle on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "FeedViewController.h"
#import "MessagesViewController.h"
#import "CreatePostViewController.h"
#import "SettingsViewController.h"
#import "Constants.h"
#import "AFJSONRequestOperation.h"
#import "Utilities.h"
#import "KeychainWrapper.h"
#import "LoadingViewController.h"

@implementation AppDelegate

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

@synthesize window = _window;
@synthesize mainTabBarController = _mainTabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UITabBarItem *feedTabBarItem = [[UITabBarItem alloc] initWithTitle:@"Posts" image:nil tag:0];
    [feedTabBarItem setTitlePositionAdjustment:UIOffsetMake(0.0, -1.5)];
    NSDictionary *textAttributes0 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1.0 alpha:0.4], UITextAttributeTextColor, [UIFont boldSystemFontOfSize:10.0], UITextAttributeFont, nil];
    [feedTabBarItem setTitleTextAttributes:textAttributes0 forState:UIControlStateNormal];
    NSDictionary *textAttributes1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, nil];
    [feedTabBarItem setTitleTextAttributes:textAttributes1 forState:UIControlStateSelected];
    [feedTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"posts_tab_icon_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"posts_tab_icon.png"]];
    [feedTabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -2.0)];
    [feedTabBarItem setImageInsets:UIEdgeInsetsMake(-1, 0, 1, 0)];
    //[feedTabBarItem setImage:[UIImage imageNamed:@"post_tab_icon.png"]];
    
    UITabBarItem *sellTabBarItem = [[UITabBarItem alloc] initWithTitle:@"Sell" image:nil tag:1];
    [sellTabBarItem setTitlePositionAdjustment:UIOffsetMake(0.0, -1.5)];
    NSDictionary *textAttributes2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1.0 alpha:0.4], UITextAttributeTextColor, [UIFont boldSystemFontOfSize:10.0], UITextAttributeFont, nil];
    [sellTabBarItem setTitleTextAttributes:textAttributes2 forState:UIControlStateNormal];
    NSDictionary *textAttributes3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, nil];
    [sellTabBarItem setTitleTextAttributes:textAttributes3 forState:UIControlStateSelected];
    [sellTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"sell_tab_icon_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"sell_tab_icon.png"]];
    [sellTabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -2.0)];
    [sellTabBarItem setImageInsets:UIEdgeInsetsMake(-1, 0, 1, 0)];
    
    UITabBarItem *messagesTabBarItem = [[UITabBarItem alloc] initWithTitle:@"Messages" image:nil tag:1];
    [messagesTabBarItem setTitlePositionAdjustment:UIOffsetMake(0.0, -1.5)];
    NSDictionary *textAttributes4 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1.0 alpha:0.4], UITextAttributeTextColor, [UIFont boldSystemFontOfSize:10.0], UITextAttributeFont, nil];
    [messagesTabBarItem setTitleTextAttributes:textAttributes4 forState:UIControlStateNormal];
    NSDictionary *textAttributes5 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, nil];
    [messagesTabBarItem setTitleTextAttributes:textAttributes5 forState:UIControlStateSelected];
    [messagesTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"messages_tab_icon_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"messages_tab_icon.png"]];
    [messagesTabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -2.0)];
    [messagesTabBarItem setImageInsets:UIEdgeInsetsMake(-1, 0, 1, 0)];
    
    UITabBarItem *settingsTabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:nil tag:2];
    [settingsTabBarItem setTitlePositionAdjustment:UIOffsetMake(0.0, -1.5)];
    NSDictionary *textAttributes6 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1.0 alpha:0.4], UITextAttributeTextColor, [UIFont boldSystemFontOfSize:10.0], UITextAttributeFont, nil];
    [settingsTabBarItem setTitleTextAttributes:textAttributes6 forState:UIControlStateNormal];
    NSDictionary *textAttributes7 = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, nil];
    [settingsTabBarItem setTitleTextAttributes:textAttributes7 forState:UIControlStateSelected];
    [settingsTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"settings_tab_icon_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"settings_tab_icon.png"]];
    [settingsTabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -2.0)];
    [settingsTabBarItem setImageInsets:UIEdgeInsetsMake(-1, 0, 1, 0)];
    //[settingsTabBarItem setImage:[UIImage imageNamed:@"settings_tab_icon.png"]];
    
    NSMutableArray *navigationControllerMutableArray = [[NSMutableArray alloc] init];
    
    FeedViewController *feedViewController = [[FeedViewController alloc] init];
    UINavigationController *feedNavigationController = [[UINavigationController alloc] initWithRootViewController:feedViewController];
    [feedNavigationController setTabBarItem:feedTabBarItem];
    [navigationControllerMutableArray addObject:feedNavigationController];
    
    CreatePostViewController *createPostViewController = [[CreatePostViewController alloc] init];
    UINavigationController *createPostNavigationController = [[UINavigationController alloc] initWithRootViewController:createPostViewController];
    [createPostNavigationController setTabBarItem:sellTabBarItem];
    [navigationControllerMutableArray addObject:createPostNavigationController];
    
    MessagesViewController *messagesViewController = [[MessagesViewController alloc] init];
    UINavigationController *messagesNavigationController = [[UINavigationController alloc] initWithRootViewController:messagesViewController];
    [messagesNavigationController setTabBarItem:messagesTabBarItem];
    [navigationControllerMutableArray addObject:messagesNavigationController];
    
    SettingsViewController *settingsViewController = [[SettingsViewController alloc] init];
    UINavigationController *settingsNavigationController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    [settingsNavigationController setTabBarItem:settingsTabBarItem];
    [navigationControllerMutableArray addObject:settingsNavigationController];
    
    self.mainTabBarController = [[UITabBarController alloc] init];
    //[self.mainTabBarController.tabBar setBackgroundImage:[UIImage imageNamed:@"tab_bar_background.png"]];
    [self.mainTabBarController.tabBar setTintColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
    //[self.mainTabBarController.tabBar setSelectionIndicatorImage:[[UIImage imageNamed:@"tab_selected_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"tab_selected_background.png"]]]];
    [self.mainTabBarController setViewControllers:navigationControllerMutableArray];
    
    [self.window addSubview:self.mainTabBarController.view];
    
    LoadingViewController *loadingViewController = [[LoadingViewController alloc] init];
    [loadingViewController.view setTag:30];
    [self.window addSubview:loadingViewController.view];
    
    [self.window makeKeyAndVisible];
    
    //set uuid as unique id to be used as "state" in url connections
    //save the value in the keychain so it is not synced between devices
    if(![KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_UNIQUE_ID]){
        [KeychainWrapper createKeychainValue:[Utilities getDeviceID] forIdentifier:PREFS_UNIQUE_ID];
    }
    
    if(![Utilities getDefaultImage]){
        [Utilities downloadDefaultPlaceholderImage];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if([KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_TOKEN_KEY]){
        [self userSignInStatus];
    }else{
        [self presentLoginViewController];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self saveContext];
}

#pragma mark - My AppDelegate Methods
-(void)presentLoginViewController{
    
    [[[[self.mainTabBarController tabBar] items] objectAtIndex:MESSAGE_NAVIGATION_CONTROLLER] setBadgeValue:nil];
    
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    UINavigationController *loginNavigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [self.mainTabBarController presentModalViewController:loginNavigationController animated:YES];
    
    [self.mainTabBarController setSelectedIndex:0];
    
    NSTimer *loadingViewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadUserInterface) userInfo:nil repeats:NO];
}

-(void)userSignInStatus{
    NSURL *editedLoginUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kSignInStatusURL, [Utilities commonParams]]];
    //if(kLoggingEnabled)NSLog(@"appdelegate edited login status URL: %@", editedLoginUrl);
    NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:editedLoginUrl];
    [loginRequest setTimeoutInterval:kUrlRequestTimeout];
    [loginRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *loginOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:loginRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"app delegate user sign in status response: %@, & JSON: %@", response, JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            if([resultDictionary objectForKey:@"logged_in"] && [[resultDictionary objectForKey:@"logged_in"] intValue] == 1){
                //else do nothing and let standard tabbar load
                //[self.mainTabBarController setSelectedIndex:0];
                [self getMessageData];
            }else{
                [self signOutUserAndPresentLoginViewController];
            }
            
            NSTimer *loadingViewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadUserInterface) userInfo:nil repeats:NO];
            
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            [self signOutUserAndPresentLoginViewController];
        }else{
            [self presentLoginViewController];
            NSTimer *loadingViewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadUserInterface) userInfo:nil repeats:NO];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(kLoggingEnabled)NSLog(@"appdelegate login status FAILURE: %@", error);
        [self presentLoginViewController];
        NSTimer *loadingViewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadUserInterface) userInfo:nil repeats:NO];
    }];
    
    [loginOperation start];
}

-(void)signOutUserAndPresentLoginViewController{
    if([KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_TOKEN_KEY]){
        [KeychainWrapper deleteItemFromKeychainWithIdentifier:PREFS_TOKEN_KEY];
    }
    [self presentLoginViewController];
}

-(void)loadUserInterface{
    [[self.window viewWithTag:30] removeFromSuperview];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();    
        } 
        
    }
}

-(void)popAllViewControllers{
    for(UINavigationController *navigationController in [self.mainTabBarController viewControllers]){
        [navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)loadMessageDataOnViewWillAppear{
    //[self popAllViewControllers];
    [(MessagesViewController *)[[(UINavigationController *)[[self.mainTabBarController viewControllers] objectAtIndex:MESSAGE_NAVIGATION_CONTROLLER] viewControllers] objectAtIndex:0] setShouldLoadDataOnViewWillAppear:YES];
}

-(void)loadPostDataOnViewWillAppear:(bool)showMyPosts{
    //[self popAllViewControllers];
    [(FeedViewController *)[[(UINavigationController *)[[self.mainTabBarController viewControllers] objectAtIndex:FEED_NAVIGATION_CONTROLLER] viewControllers] objectAtIndex:0] setShouldLoadDataOnViewWillAppear:YES];
    
    if(showMyPosts){
        [(FeedViewController *)[[(UINavigationController *)[[self.mainTabBarController viewControllers] objectAtIndex:FEED_NAVIGATION_CONTROLLER] viewControllers] objectAtIndex:0] setSearchByUsername:YES];
        [[(FeedViewController *)[[(UINavigationController *)[[self.mainTabBarController viewControllers] objectAtIndex:FEED_NAVIGATION_CONTROLLER] viewControllers] objectAtIndex:0] feedSegmentedControl] setSelectedSegmentIndex:1];
    }else{
        [[(FeedViewController *)[[(UINavigationController *)[[self.mainTabBarController viewControllers] objectAtIndex:FEED_NAVIGATION_CONTROLLER] viewControllers] objectAtIndex:0] feedSegmentedControl] setSelectedSegmentIndex:0];
    }
}

-(void)getMessageData{
    [(MessagesViewController *)[[(UINavigationController *)[[self.mainTabBarController viewControllers] objectAtIndex:MESSAGE_NAVIGATION_CONTROLLER] viewControllers] objectAtIndex:0] loadTableDataFromNewestPost];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PhotoDataModel" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"slinggit.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
