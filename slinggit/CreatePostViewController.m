#import "CreatePostViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "KeychainWrapper.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"
#import "UIImageView+AFNetworking.h"
#import "AFHTTPClient.h"
#import "Constants.h"
#import "Utilities.h"
#import "SVProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"

@interface CreatePostViewController(){
    //BOOL descriptionViewIsMovedUp;
    CLLocationManager *locationManager;
    UIImage *editedImageToUpload;
    UIButton *locationButton;
    UIView *chooseSocialNetworkBackground;
    UITableView *apiAccountsTableView;
    UILabel *noApiAccountsTextView;
    NSMutableArray *socialNetworkAccounts;
    UIButton *addSocialNetworksButton;
    UIActivityIndicatorView *loadingIndicator;
    NSMutableArray *selectedIndexes;
    UIButton *submitButton;
}

@end

@implementation CreatePostViewController

@synthesize itemNameTextField = _itemNameTextField;
@synthesize descriptionTextField = _descriptionTextField;
@synthesize priceTextField = _priceTextField;
@synthesize locationTextField = _locationTextField;
@synthesize imagePickerController = _imagePickerController;
@synthesize photoButton = _photoButton;

#define ITEM_NAME_TAG 0
#define PRICE_TAG 1
#define DESCRIPTION_TAG 2
#define LOCATION_TAG 3

#define ALERTVIEW_EMPTY_ITEM_NAME_TAG 0
#define ALERTVIEW_EMPTY_PRICE_TAG 1
#define ALERTVIEW_EMPTY_DESCRIPTION_TAG 2
#define ALERTVIEW_EMPTY_LOCATION_TAG 3

#define TAG_SPINNER_VIEW 10

#define kCellHeight 48

#define CELL_IMAGE_TAG 10
#define CELL_ACCOUNT_NAME_TAG 11
#define CELL_API_SOURCE_IMAGE_TAG 12

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [super loadView];
    
    socialNetworkAccounts = [[NSMutableArray alloc] init];
    selectedIndexes = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(clearBarButtonItemPressed)];
    [self.navigationItem setRightBarButtonItem:clearBarButtonItem];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.5];
    [view addSubview:backgroundImageView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    [singleTap setDelegate:self];
    [singleTap setCancelsTouchesInView:NO];
    [view addGestureRecognizer:singleTap];
    
    UIImageView *photoButtonBackground = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"photo_button_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"photo_button_background.png"]]]];
    [photoButtonBackground setFrame:CGRectMake(10, 58, 96, 96)];
    [view addSubview:photoButtonBackground];
    
    self.photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.photoButton setFrame:CGRectMake(14, 62, 88, 88)];
    [self.photoButton setBackgroundImage:[[UIImage imageNamed:@"gray_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"gray_button.png"]]] forState:UIControlStateNormal];
    [self.photoButton setImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
    [self.photoButton addTarget:self action:@selector(photoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.photoButton];
    
    UIView *itemNameBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 6, 300, 46)];
    [view addSubview:itemNameBackgroundView];
    
    UIImageView *itemNameImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [itemNameImageView setFrame:CGRectMake(0, 0, 300, 46)];
    [itemNameBackgroundView addSubview:itemNameImageView];
    
    UILabel *poundSymbolLabel = [[UILabel alloc] initWithFrame:CGRectMake(9, 2, 20, 40)];
    [poundSymbolLabel setText:@"#"];
    [poundSymbolLabel setBackgroundColor:[UIColor clearColor]];
    [poundSymbolLabel setFont:[UIFont boldSystemFontOfSize:24]];
    [poundSymbolLabel setTextColor:[UIColor lightGrayColor]];
    [poundSymbolLabel setTextAlignment:UITextAlignmentLeft];
    [itemNameBackgroundView addSubview:poundSymbolLabel];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(240, 5, 1, itemNameImageView.frame.size.height-8)];
    [lineView setBackgroundColor:[UIColor lightGrayColor]];
    [itemNameBackgroundView addSubview:lineView];
    
    UILabel *forSaleLabel = [[UILabel alloc] initWithFrame:CGRectMake(246, 3, 60, 40)];
    [forSaleLabel setText:@"forsale"];
    [forSaleLabel setBackgroundColor:[UIColor clearColor]];
    [forSaleLabel setFont:[UIFont systemFontOfSize:16]];
    [forSaleLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [forSaleLabel setTextAlignment:UITextAlignmentLeft];
    [itemNameBackgroundView addSubview:forSaleLabel];
    
    self.itemNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(22, -2, 218, 50)];
    [self.itemNameTextField setTag:ITEM_NAME_TAG];
    [self.itemNameTextField setDelegate:self];
    [self.itemNameTextField setReturnKeyType:UIReturnKeyNext];
    [self.itemNameTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.itemNameTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.itemNameTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.itemNameTextField setPlaceholder:@"youritem"];
    self.itemNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [itemNameBackgroundView addSubview:self.itemNameTextField];
    
    UIView *priceBackground = [[UIView alloc] initWithFrame:CGRectMake(120, 58, 190, 46)];
    [view addSubview:priceBackground];
    
    UIImageView *priceImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [priceImageView setFrame:CGRectMake(0, 0, 190, 46)];
    [priceBackground addSubview:priceImageView];
    
    UILabel *dollarSymbolLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 20, 40)];
    [dollarSymbolLabel setText:@"$"];
    [dollarSymbolLabel setBackgroundColor:[UIColor clearColor]];
    [dollarSymbolLabel setFont:[UIFont boldSystemFontOfSize:24]];
    [dollarSymbolLabel setTextColor:[UIColor lightGrayColor]];
    [dollarSymbolLabel setTextAlignment:UITextAlignmentLeft];
    [priceBackground addSubview:dollarSymbolLabel];
    
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(130, 5, 1, priceBackground.frame.size.height-8)];
    [lineView2 setBackgroundColor:[UIColor lightGrayColor]];
    [priceBackground addSubview:lineView2];
    
    UILabel *zerosLabel = [[UILabel alloc] initWithFrame:CGRectMake(134, 3, 60, 40)];
    [zerosLabel setText:@".00"];
    [zerosLabel setBackgroundColor:[UIColor clearColor]];
    [zerosLabel setFont:[UIFont systemFontOfSize:16]];
    [zerosLabel setTextColor:COLOR_SLINGGIT_BLUE];
    [zerosLabel setTextAlignment:UITextAlignmentLeft];
    [priceBackground addSubview:zerosLabel];
    
    self.priceTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, -2, 110, 50)];
    [self.priceTextField setTag:PRICE_TAG];
    [self.priceTextField setDelegate:self];
    [self.priceTextField setReturnKeyType:UIReturnKeyNext];
    [self.priceTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.priceTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.priceTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.priceTextField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [self.priceTextField setPlaceholder:@"Price"];
    self.priceTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [priceBackground addSubview:self.priceTextField];
    
    UIView *locationBackground = [[UIView alloc] initWithFrame:CGRectMake(120, 110, 190, 46)];
    [view addSubview:locationBackground];
    
    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [locationImageView setFrame:CGRectMake(0, 0, 190, 46)];
    [locationBackground addSubview:locationImageView];
    
    locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [locationButton setFrame:CGRectMake(0, 0, 37, 46)];
    [locationButton setBackgroundImage:[[UIImage imageNamed:@"location_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"location_button.png"]]]  forState:UIControlStateNormal];
    [locationButton setImage:[UIImage imageNamed:@"location_icon2.png"] forState:UIControlStateNormal];
    [locationButton setImage:[UIImage imageNamed:@"clear_image.png"] forState:UIControlStateDisabled];
    [locationButton addTarget:self action:@selector(locationButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [spinner setCenter:locationButton.center];
    [spinner setTag:TAG_SPINNER_VIEW];
    [locationButton addSubview:spinner];
    
    [locationBackground addSubview:locationButton];
    
    self.locationTextField = [[UITextField alloc] initWithFrame:CGRectMake(42, -2, 147, 50)];
    [self.locationTextField setTag:LOCATION_TAG];
    [self.locationTextField setDelegate:self];
    [self.locationTextField setReturnKeyType:UIReturnKeyNext];
    [self.locationTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.locationTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.locationTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    [self.locationTextField setPlaceholder:@"Location"];
    self.locationTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [locationBackground addSubview:self.locationTextField];
    
    UIView *descriptionBackground = [[UIView alloc] initWithFrame:CGRectMake(10, 163, 300, 110)];
    [view addSubview:descriptionBackground];
    
    UIImageView *descriptionImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [descriptionImageView setFrame:CGRectMake(0, 0, descriptionBackground.frame.size.width, descriptionBackground.frame.size.height)];
    [descriptionBackground addSubview:descriptionImageView];
    
    self.descriptionTextField = [[UITextView alloc] initWithFrame:CGRectMake(2, 2, descriptionBackground.frame.size.width-2, descriptionBackground.frame.size.height-4)];
    [self.descriptionTextField setTag:DESCRIPTION_TAG];
    [self.descriptionTextField setDelegate:self];
    [self.descriptionTextField setText:@"Short Description"];
    [self.descriptionTextField setTextColor:[UIColor lightGrayColor]];
    [self.descriptionTextField setBackgroundColor:[UIColor clearColor]];
    [self.descriptionTextField setFont:[UIFont systemFontOfSize:16.0]];
    //[self.descriptionTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [self.descriptionTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
    [self.descriptionTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [descriptionBackground addSubview:self.descriptionTextField];
    
    submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
    [submitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [submitButton setFrame:CGRectMake(20, 312, 280, 46)];
    [submitButton setTitle:@"Confirm Post" forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [submitButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    submitButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [submitButton setHidden:YES];
    [view addSubview:submitButton];
    
    addSocialNetworksButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addSocialNetworksButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
    [addSocialNetworksButton setTitle:@"Post" forState:UIControlStateNormal];
    [addSocialNetworksButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [addSocialNetworksButton setFrame:CGRectMake(20, 312, 280, 46)];
    [addSocialNetworksButton addTarget:self action:@selector(addSocialNetworksButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    addSocialNetworksButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [view addSubview:addSocialNetworksButton];
    
    chooseSocialNetworkBackground = [[UIView alloc] initWithFrame:CGRectMake((addSocialNetworksButton.frame.origin.x-10), addSocialNetworksButton.frame.origin.y-190, 300, 200)];
    
    [chooseSocialNetworkBackground setAutoresizesSubviews:YES];
    
    UIImageView *popoverBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, chooseSocialNetworkBackground.frame.size.width, chooseSocialNetworkBackground.frame.size.height)];
    [popoverBackground setImage:[[UIImage imageNamed:@"popup_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(([UIImage imageNamed:@"popup_background.png"].size.height - 1)/2, 48, ([UIImage imageNamed:@"popup_background.png"].size.height - 1)/2, ([UIImage imageNamed:@"popup_background.png"].size.width - 1)/2)]];
    [popoverBackground setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [chooseSocialNetworkBackground addSubview:popoverBackground];
    
    apiAccountsTableView = [[UITableView alloc] initWithFrame:CGRectMake(20, 20, chooseSocialNetworkBackground.frame.size.width-40, chooseSocialNetworkBackground.frame.size.height-40) style:UITableViewStylePlain];
    [apiAccountsTableView setBackgroundColor:[UIColor clearColor]];
    [apiAccountsTableView setDelegate:self];
	[apiAccountsTableView setDataSource:self];
    [chooseSocialNetworkBackground addSubview:apiAccountsTableView];
    
    noApiAccountsTextView = [[UILabel alloc] initWithFrame:CGRectMake((chooseSocialNetworkBackground.frame.size.width-200)/2, (chooseSocialNetworkBackground.frame.size.height-30)/2-14, 200, 30)];
    [noApiAccountsTextView setBackgroundColor:[UIColor clearColor]];
    [noApiAccountsTextView setText:@"No Networks"];
    [noApiAccountsTextView setTextColor:[UIColor darkTextColor]];
    [noApiAccountsTextView setTextAlignment:UITextAlignmentCenter];
    [noApiAccountsTextView setFont:[UIFont boldSystemFontOfSize:13.0]];
    [noApiAccountsTextView setHidden:YES];
    [chooseSocialNetworkBackground addSubview:noApiAccountsTextView];
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicator setCenter:CGPointMake((chooseSocialNetworkBackground.frame.size.width-loadingIndicator.frame.size.width)/2+4, (chooseSocialNetworkBackground.frame.size.height-loadingIndicator.frame.size.height)/2-4)];
    [chooseSocialNetworkBackground addSubview:loadingIndicator];
    
    [chooseSocialNetworkBackground setHidden:YES];
    [view addSubview:chooseSocialNetworkBackground];
    
    self.view = view;
    
    
}

- (void) viewWillAppear:(BOOL)animated{
    /*
     NSURL *editedGetImageUrl = [NSURL URLWithString:@"http://netobjects.com/assets/images/icon-image-bank.png"];
     
     if(kLoggingEnabled)NSLog(@"get image data URL: %@", editedGetImageUrl);
     NSMutableURLRequest *getImageRequest = [NSMutableURLRequest requestWithURL:editedGetImageUrl];
     [getImageRequest setHTTPMethod:@"GET"];
     
     AFImageRequestOperation *imageRequestOperation = [AFImageRequestOperation imageRequestOperationWithRequest:getImageRequest success:^(UIImage *image) {
     
     editedImageToUpload = image;
     }];
     
     [imageRequestOperation start];
     */
    [self.navigationController setNavigationBarHidden:NO];
    [super viewWillAppear:animated];
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.navigationItem setTitle:@"Post"];
    
    UIButton *titleViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleViewButton setBackgroundColor:[UIColor clearColor]];
    [titleViewButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20.0]];
    [titleViewButton setFrame:CGRectMake(0,0,200,30)];
    [titleViewButton setTitle:@"Post" forState:UIControlStateNormal];
    [titleViewButton addTarget:self action:@selector(clearFirstResponders) forControlEvents:UIControlEventTouchUpInside];
    [titleViewButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    titleViewButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [self.navigationItem setTitleView:titleViewButton];
    
    
    [self.navigationController.navigationBar setTintColor:COLOR_SLINGGIT_BLUE];
    //descriptionViewIsMovedUp = NO;
    
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - My Methods

-(void)descriptionFieldAnimateUp{
    
    [UIView animateWithDuration:0.1 animations:^{
        [self.view setFrame:CGRectMake(0, -160, kScreenWidth, kScreenHeightNoStatusBar)];
        
    }];
    
}

-(void)descriptionFieldAnimateDown{
    
    [UIView animateWithDuration:0.1 animations:^{
        [self.view setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
        
    }];
    
}

-(void)stopUpdatingLocation{
    [locationButton setEnabled:YES];
    [(UIActivityIndicatorView *)[locationButton viewWithTag:TAG_SPINNER_VIEW] stopAnimating];
    [locationManager stopUpdatingLocation];
    
}

-(void)clearFirstResponders{
    [self.itemNameTextField resignFirstResponder];
    [self.descriptionTextField resignFirstResponder];
    [self.priceTextField resignFirstResponder];
    [self.locationTextField resignFirstResponder];
}

-(void)validateFields{
    if(self.itemNameTextField.text == nil || self.itemNameTextField.text.length == 0){
        UIAlertView *emptyItemNameAlertView = [[UIAlertView alloc] initWithTitle:@"Item Name Missing" message:@"Please enter an item name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [emptyItemNameAlertView setTag:ALERTVIEW_EMPTY_ITEM_NAME_TAG];
        [emptyItemNameAlertView show];
        return;
    }else if (self.priceTextField.text.length == 0){
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Price Missing" message:@"Please enter a price." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView setTag:ALERTVIEW_EMPTY_PRICE_TAG];
        [errorAlertView show];
        return;
    }else if(self.descriptionTextField.text == nil || [self.descriptionTextField.text isEqualToString:@"Short Description"] ||  self.descriptionTextField.text.length == 0){
        UIAlertView *emptyDescriptionAlertView = [[UIAlertView alloc] initWithTitle:@"Description Missing" message:@"Please enter a description." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [emptyDescriptionAlertView setTag:ALERTVIEW_EMPTY_DESCRIPTION_TAG];
        [emptyDescriptionAlertView show];
        return;
    }else if(self.locationTextField.text == nil || self.locationTextField.text.length == 0){
        UIAlertView *emptyLocationAlertView = [[UIAlertView alloc] initWithTitle:@"Location Missing" message:@"Please enter a location." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [emptyLocationAlertView setTag:ALERTVIEW_EMPTY_LOCATION_TAG];
        [emptyLocationAlertView show];
        return;
    }
    
    [self checkPostLimitations];
    
}

-(void)enableAddSocialNetworksButton{
    [submitButton setHidden:YES];
    [addSocialNetworksButton setEnabled:YES];
    [addSocialNetworksButton setHidden:NO];
    [loadingIndicator stopAnimating];
    [chooseSocialNetworkBackground setHidden:YES];
    [selectedIndexes removeAllObjects];
    
}

-(void)checkPostLimitations{
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    [addSocialNetworksButton setEnabled:NO];
    
    NSString *limitationType = @"posts";
    NSURL *editedCheckPostLimitationsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kCheckPostLimitationsURL, [Utilities commonParams], kJSONLimitationType, limitationType]];
    
    NSMutableURLRequest *checkPostLimitationsRequest = [NSMutableURLRequest requestWithURL:editedCheckPostLimitationsUrl];
    [checkPostLimitationsRequest setTimeoutInterval:kUrlRequestTimeout];
    [checkPostLimitationsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *checkPostLimitationsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:checkPostLimitationsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"check limitations response json: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
            if([[resultDictionary objectForKey:kJSONLimitationPass] intValue] == 1){
                
                [self clearFirstResponders];
                
                /*
                 [UIView animateWithDuration:1.0 animations:^{
                 
                 
                 [chooseSocialNetworkBackground setFrame:CGRectMake(chooseSocialNetworkBackground.frame.origin.x, chooseSocialNetworkBackground.frame.origin.y, 300, 200)];
                 
                 }];*/
                
                [self loadApiAccounts];
                
            }else {
                [SVProgressHUD dismiss];
                [self enableAddSocialNetworksButton];
                NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
                UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [loginErrorAlertView show];
            }
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            [SVProgressHUD dismiss];
            [self enableAddSocialNetworksButton];
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginErrorAlertView show];
        }else{
            [SVProgressHUD dismiss];
            [self enableAddSocialNetworksButton];
            UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginErrorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        [self enableAddSocialNetworksButton];
        if(kLoggingEnabled)NSLog(@"check limitations failure error: \n%@", error);
        
        UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [loginErrorAlertView show];
    }];
    
    [checkPostLimitationsOperation start];
    
    
}

-(void)submitPostWithPhoto:(BOOL)hasPhoto{
    [chooseSocialNetworkBackground setHidden:YES];
    [SVProgressHUD showWithStatus:@"Posting" maskType:SVProgressHUDMaskTypeGradient];
    NSMutableString *apiAccountIdsString = [[NSMutableString alloc] init];
    if([selectedIndexes count] > 0){
        for(int i=0; i<[selectedIndexes count]; i++){
            NSDictionary *socialNetwork = [socialNetworkAccounts objectAtIndex:[[selectedIndexes objectAtIndex:i] intValue]];
            if(i == 0){
                
                [apiAccountIdsString appendFormat:@"%@", [socialNetwork objectForKey:kJSONIdKeyName]];
            }else{
                [apiAccountIdsString appendFormat:@",%@", [socialNetwork objectForKey:kJSONIdKeyName]];
            }
        }
    }
    
    NSURL *editedCreatePostUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", kCreatePostURL, [Utilities commonParams], kJSONHashTagPrefixKeyName, self.itemNameTextField.text, kJSONPriceKeyName, [self.priceTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""], kJSONLocationKeyName, self.locationTextField.text, kJSONContentKeyName, [Utilities stringByEncodingHTMLEntities:self.descriptionTextField.text], kJSONApiAccountIdsKeyName, apiAccountIdsString]];
    
    NSString *pathToCreatePostUrl = [NSString stringWithString:[NSString stringWithFormat:@"?%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", [Utilities commonParams], kJSONHashTagPrefixKeyName, self.itemNameTextField.text, kJSONPriceKeyName, [self.priceTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""], kJSONLocationKeyName, self.locationTextField.text, kJSONContentKeyName, [Utilities stringByEncodingHTMLEntities:self.descriptionTextField.text], kJSONApiAccountIdsKeyName, apiAccountIdsString]];
    if(kLoggingEnabled)NSLog(@"edited post URL: %@", editedCreatePostUrl);
    
    NSMutableURLRequest *createPostRequest;
    if(hasPhoto){
        AFHTTPClient *myClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kCreatePostURL]];
        
        NSData *tempImageData = UIImageJPEGRepresentation(editedImageToUpload, 1.0);
        NSString *base64EncodedString = [Utilities newStringInBase64FromData:tempImageData];
        //if(kLoggingEnabled)NSLog(@"regular data: %@\n\nbase64 image data: %@", tempImageData, base64EncodedString);
        
        createPostRequest = [myClient multipartFormRequestWithMethod:@"POST" path:pathToCreatePostUrl parameters:[NSDictionary dictionaryWithObject:base64EncodedString forKey:@"image_data"] 
                                           constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                               
                                           }];
        [createPostRequest setTimeoutInterval:10.0];
        AFJSONRequestOperation *createPostOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:createPostRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [self enableAddSocialNetworksButton];
            [SVProgressHUD dismiss];
            [submitButton setEnabled:YES];
            if(kLoggingEnabled)NSLog(@"create post response json: %@", JSON);
            if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
                [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:YES];
                [self clearBarButtonItemPressed];
                [[[self.tabBarController viewControllers] objectAtIndex:FEED_NAVIGATION_CONTROLLER] popToRootViewControllerAnimated:NO];
                [self.tabBarController setSelectedIndex:0];
                
            }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
                
                NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
                NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
                UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [loginErrorAlertView show];
            }else{
                
                UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [loginErrorAlertView show];
            }
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [self enableAddSocialNetworksButton];
            
            [SVProgressHUD dismiss];
            [submitButton setEnabled:YES];
            if(kLoggingEnabled)NSLog(@"create post failure error: \n%@", error);
            
            UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginErrorAlertView show];
        }];
        
        
        [createPostOperation start];
        
    }else{
        
        createPostRequest = [NSMutableURLRequest requestWithURL:editedCreatePostUrl];
        [createPostRequest setTimeoutInterval:kUrlRequestTimeout];
        [createPostRequest setHTTPMethod:@"POST"];
        
        AFJSONRequestOperation *createPostOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:createPostRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [SVProgressHUD dismiss];
            [self enableAddSocialNetworksButton];
            if(kLoggingEnabled)NSLog(@"create post response json: %@", JSON);
            if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
                [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:YES];
                [self clearBarButtonItemPressed];
                [[[self.tabBarController viewControllers] objectAtIndex:FEED_NAVIGATION_CONTROLLER] popToRootViewControllerAnimated:NO];
                [self.tabBarController setSelectedIndex:0];
            }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
                
                NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
                NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
                UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [loginErrorAlertView show];
            }else{
                
                UIAlertView *postErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [postErrorAlertView show];
            }
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [self enableAddSocialNetworksButton];
            
            [SVProgressHUD dismiss];
            [submitButton setEnabled:YES];
            if(kLoggingEnabled)NSLog(@"create post failure error: JSON: \n%@", JSON);
            
            UIAlertView *postErrorAlertView = [[UIAlertView alloc] initWithTitle:kPostErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [postErrorAlertView show];
        }];
        
        [createPostOperation start];
    }
}

-(void)loadApiAccounts{
    NSURL *editedGetApiAccountsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kGetUserApiAccountsURL, [Utilities commonParams]]];
    if(kLoggingEnabled)NSLog(@"edited get api accounts url: %@", editedGetApiAccountsUrl);
    NSMutableURLRequest *getApiAccountsRequest = [NSMutableURLRequest requestWithURL:editedGetApiAccountsUrl];
    [getApiAccountsRequest setTimeoutInterval:kUrlRequestTimeout];
    [getApiAccountsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getApiAccountsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getApiAccountsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [loadingIndicator stopAnimating];
        [SVProgressHUD dismiss];
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            if(kLoggingEnabled)NSLog(@"load api accounts success response JSON: %@", JSON);
            
            
            NSDictionary *resultDictionary = [JSON valueForKey:kJSONResult];
            
            if([resultDictionary objectForKey:kJSONRowsFoundKeyName]){
                
                if([[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue] > 0){
                    
                    [chooseSocialNetworkBackground setHidden:NO];
                    [addSocialNetworksButton setHidden:YES];
                    [addSocialNetworksButton setEnabled:YES];
                    [submitButton setHidden:NO];
                    [submitButton setEnabled:YES];
                    
                    socialNetworkAccounts = [NSMutableArray arrayWithArray:[resultDictionary objectForKey:kJSONApiAccountsKeyName]];
                    
                    if(kLoggingEnabled)NSLog(@"api source data loaded: %@", socialNetworkAccounts);
                    
                    [apiAccountsTableView reloadData];
                    [apiAccountsTableView setHidden:NO];
                    [self loadImagesForOnscreenRows];
                }else{
                    [self submitButtonPressed];
                }
                
            }else{
                [self enableAddSocialNetworksButton];
            }
            
        }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            if(kLoggingEnabled)NSLog(@"load api accounts failure JSON: %@", JSON);
            [self enableAddSocialNetworksButton];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage;
            if([resultDictionary valueForKey:kJSONFriendlyError]){
                errorMessage = [NSString stringWithString:[resultDictionary valueForKey:kJSONFriendlyError]];
            }else{
                errorMessage = [NSString stringWithString:kConnectionErrorText];
            }
            
            UIAlertView *getApiAccountsErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [getApiAccountsErrorAlertView show];
        }else{
            [self enableAddSocialNetworksButton];
            
            UIAlertView *getApiAccountsErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [getApiAccountsErrorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [loadingIndicator stopAnimating];
        [SVProgressHUD dismiss];
        [self enableAddSocialNetworksButton];
        if(kLoggingEnabled)NSLog(@"load api accounts error: %@", error);
        UIAlertView *getApiAccountsErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [getApiAccountsErrorAlertView show];
    }];
    
    [getApiAccountsOperation start];
}

-(void)loadImagesForOnscreenRows{
    
    if(socialNetworkAccounts && [socialNetworkAccounts count] > 0){
        for(NSIndexPath *indexPath in [apiAccountsTableView indexPathsForVisibleRows]){
            if(indexPath.row < [socialNetworkAccounts count]){
                UITableViewCell *cell = [apiAccountsTableView cellForRowAtIndexPath:indexPath];
                
                if(cell.imageView.image == nil){
                    
                    if(![[[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONImageUrlKeyName] isEqual:[NSNull null]]){
                        
                        NSURL *editedGetImageUrl = [NSURL URLWithString:[[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONImageUrlKeyName]];
                        
                        if([[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONApiSourceKeyName]){
                            if([[[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONApiSourceKeyName] isEqual:kJSONFacebook]){
                                [(UIImageView *)[cell.contentView viewWithTag:CELL_API_SOURCE_IMAGE_TAG] setImage:[UIImage imageNamed:@"facebook_icon.png"]];
                            }else if([[[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONApiSourceKeyName] isEqual:kJSONTwitter]){
                                [(UIImageView *)[cell.contentView viewWithTag:CELL_API_SOURCE_IMAGE_TAG] setImage:[UIImage imageNamed:@"twitter_icon.png"]];
                            }
                        }
                        
                        [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImageWithURL:editedGetImageUrl];
                        //[cell.imageView setImage:[UIImage imageNamed:@"arrow.png"]];
                        
                        if(kLoggingEnabled)NSLog(@"get image data URL: %@", editedGetImageUrl);
                    }
                    
                }
            }
        }
    }
}

/*
 -(void)animateTextFieldMoveUp:(BOOL)moveUp{
 
 int dy = 156;
 
 float duration = 0.3f;
 
 int movement = (moveUp ? -dy : dy);
 
 [UIView beginAnimations:@"anim" context:nil];
 [UIView setAnimationBeginsFromCurrentState:YES];
 [UIView setAnimationDuration:duration];
 self.view.frame = CGRectOffset(self.view.frame, 0, movement);
 [UIView commitAnimations];
 }
 */

#pragma mark - Button Presses

-(void)locationButtonPressed{
    [locationButton setEnabled:NO];
    [(UIActivityIndicatorView *)[locationButton viewWithTag:TAG_SPINNER_VIEW] startAnimating];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    
    [locationManager startUpdatingLocation];
    [self performSelector:@selector(stopUpdatingLocation) withObject:nil afterDelay:10.0];
    
}

-(void)clearBarButtonItemPressed{
    [self clearFirstResponders];
    [self.itemNameTextField setText:@""];
    [self.descriptionTextField setText:@"Short Description"];
    [self.descriptionTextField setTextColor:[UIColor lightGrayColor]];
    [self.priceTextField setText:@""];
    [self.locationTextField setText:@""];
    [self.photoButton.imageView.layer setBorderWidth:0.0];
    [self.photoButton.imageView.layer setBorderColor:[[UIColor clearColor] CGColor]];
    [self.photoButton.imageView.layer setCornerRadius:0.0];
    [self.photoButton setBackgroundImage:[[UIImage imageNamed:@"gray_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"gray_button.png"]]] forState:UIControlStateNormal];
    [self.photoButton setImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
    editedImageToUpload = nil;
    
    [self enableAddSocialNetworksButton];
    
}

-(void)addSocialNetworksButtonPressed:(UIButton *)button{
    [self clearFirstResponders];
    [self validateFields];
    
}

-(void)submitButtonPressed{
    [self clearFirstResponders];
    [submitButton setEnabled:NO];
    if(editedImageToUpload){
        [self submitPostWithPhoto:YES];
    }else{
        [self submitPostWithPhoto:NO];
    }
}

-(void)photoButtonPressed:(UIButton *)button{
    [self clearFirstResponders];
    
    UIActionSheet *getPhotoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Library", nil];
    [getPhotoActionSheet showInView:self.tabBarController.view];
    
}

#pragma mark - Location Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 5.0) return;
    
    if (newLocation.horizontalAccuracy < 0) return;
    
    if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy) {
        
        [locationManager stopUpdatingLocation];
        
        CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error){
            
            for(CLPlacemark *placemark in placemarks){
                NSLog(@"placemark info***: subadminarea %@ & sublocality %@ & area: %@", [placemark administrativeArea], [placemark subLocality], [placemark administrativeArea]);
                
                NSString *stateAbrev = [Utilities getStateWithFullStateName:[placemark administrativeArea]];
                
                int stateCharNumber = 2;
                if(stateAbrev == nil){
                    stateCharNumber = 0;
                }
                
                NSString *locationString;
                if([placemark locality].length > kLocationFieldLimit-stateCharNumber){
                    locationString = [NSString stringWithString:[[[placemark locality] stringByReplacingOccurrencesOfString:@" " withString:@""] substringToIndex:kLocationFieldLimit-stateCharNumber]];
                }else{
                    locationString = [NSString stringWithString:[[placemark locality] stringByReplacingOccurrencesOfString:@" " withString:@""]];
                }
                
                if(stateAbrev != nil){
                    locationString = [locationString stringByAppendingString:stateAbrev];
                }
                
                [locationButton setEnabled:YES];
                [(UIActivityIndicatorView *)[locationButton viewWithTag:TAG_SPINNER_VIEW] stopAnimating];
                [self.locationTextField setText:locationString];
            }
            
        }];
    }
    
}

#pragma mark - UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePickerController = [[UIImagePickerController alloc] init];
            [self.imagePickerController setAllowsEditing:YES];
            [self.imagePickerController setDelegate:self];
            [self.imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
            
            [self presentModalViewController:self.imagePickerController animated:YES];
        }else{
            UIAlertView *noCameraAlertView = [[UIAlertView alloc] initWithTitle:kNoCameraErrorTitle message:kNoCameraError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [noCameraAlertView show];
        }
    }else if(buttonIndex == 1){
        NSLog(@"button index 1");
        self.imagePickerController = [[UIImagePickerController alloc] init];
        [self.imagePickerController setAllowsEditing:YES];
        [self.imagePickerController setDelegate:self];
        [self.imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        [self presentModalViewController:self.imagePickerController animated:YES];
    }
}

#pragma mark - UIImagePickerController Delegate Methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    // Access the cropped image from info dictionary
    UIImage *returnedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageWriteToSavedPhotosAlbum(returnedImage, nil, nil, nil);
    editedImageToUpload = [Utilities imageWithImage:returnedImage scaledToSize:CGSizeMake(300, 300)];
    UIImage *buttonScaledImage = [Utilities imageWithImage:returnedImage scaledToSize:CGSizeMake(88, 88)];
    
    [self.photoButton setImage:buttonScaledImage forState:UIControlStateNormal];
    //[self.photoButton setImageEdgeInsets:UIEdgeInsetsMake(1.0, 0.5, 0, 0)];
    [self.photoButton.imageView.layer setBorderWidth:1.0];
    [self.photoButton.imageView.layer setBorderColor:[[UIColor colorWithWhite:0.0 alpha:0.3] CGColor]];
    [self.photoButton.imageView.layer setCornerRadius:3.0];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Gesture Recognizer Methods

-(void)backgroundTapped:(UITapGestureRecognizer *)gestureRecognizer{
    [self clearFirstResponders];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isKindOfClass:[UIControl class]]){
        return NO;
    }
    return YES;
}

#pragma mark - UITextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //[self clearFirstResponders];
    switch (textField.tag) {
        case ITEM_NAME_TAG:
            [self.priceTextField becomeFirstResponder];
            break;
        case PRICE_TAG:
            [self.locationTextField becomeFirstResponder];
            break;
            
        case LOCATION_TAG:
            [self.descriptionTextField becomeFirstResponder];
            break;
        default:
            return YES;
    }
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField.tag == PRICE_TAG){
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        self.priceTextField.text = [[textField.text componentsSeparatedByCharactersInSet:nonNumberSet] componentsJoinedByString:@""];
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(textField.tag == PRICE_TAG){
        if([textField.text length] > 3){
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [numberFormatter setPositiveFormat:@"#,###"];
            self.priceTextField.text = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSNumber numberWithInt:[textField.text intValue]]]];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField.tag == PRICE_TAG){
        
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if(([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""]){
            
            int maxLength = kPriceFieldLimit;
            NSUInteger oldLength = [textField.text length];
            NSUInteger replacementLength = [string length];
            NSUInteger rangeLength = range.length;
            
            NSUInteger newLength = oldLength - rangeLength + replacementLength;
            
            return newLength <= maxLength;
        }
        
        return NO;
        
    }else if(textField.tag == ITEM_NAME_TAG){
        NSCharacterSet *alphaNumericSet = [NSCharacterSet alphanumericCharacterSet];
        if(([string stringByTrimmingCharactersInSet:alphaNumericSet].length == 0) || [string isEqualToString:@""]){
            NSRange spaceRange = [string rangeOfString:@" "];
            if (spaceRange.location != NSNotFound)
            {
                return NO;
            } 
            int maxLength = kItemNameFieldLimit;
            NSUInteger oldLength = [textField.text length];
            NSUInteger replacementLength = [string length];
            NSUInteger rangeLength = range.length;
            
            NSUInteger newLength = oldLength - rangeLength + replacementLength;
            
            return newLength <= maxLength;
        }
        return NO;
    }else if(textField.tag == LOCATION_TAG){
        NSCharacterSet *alphaNumericSet = [NSCharacterSet alphanumericCharacterSet];
        if(([string stringByTrimmingCharactersInSet:alphaNumericSet].length == 0) || [string isEqualToString:@""]){
            
            NSRange spaceRange = [string rangeOfString:@" "];
            if (spaceRange.location != NSNotFound)
            {
                return NO;
            } 
            int maxLength = kLocationFieldLimit;
            NSUInteger oldLength = [textField.text length];
            NSUInteger replacementLength = [string length];
            NSUInteger rangeLength = range.length;
            
            NSUInteger newLength = oldLength - rangeLength + replacementLength;
            
            return newLength <= maxLength;
        }
        
        return NO;
    }
    
    return YES;
}


#pragma mark - UITextView Delegate Methods
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@"Short Description"]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        
    }
    
    /*
     if(textView.tag == DESCRIPTION_TAG){
     if(!descriptionViewIsMovedUp){
     [self animateTextFieldMoveUp:YES];
     descriptionViewIsMovedUp = YES;
     }
     }
     */
    
    [self descriptionFieldAnimateUp];
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        [textView setTextColor:[UIColor lightGrayColor]];
        [textView setText:@"Short Description"];
    }
    /*
     if(textView.tag == DESCRIPTION_TAG){
     if(descriptionViewIsMovedUp){
     [self animateTextFieldMoveUp:NO];
     descriptionViewIsMovedUp = NO;
     }
     }
     */
    
    [self descriptionFieldAnimateDown];
    
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    /*
     if([text isEqualToString:@"\n"]){
     [textView resignFirstResponder];
     return NO;
     }
     */
    int maxLength = kDescriptionFieldLimit;
    if([[textView text] length] == 0 && text.length == 0){
        [textView setTextColor:[UIColor lightGrayColor]];
        [textView setText:@"Short Description"];
        [textView resignFirstResponder];
    }
    
    if(range.length > text.length){
        return YES;
    }else if([[textView text] length] + text.length > maxLength){
        return NO;
    }
    
    return YES;
}

#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case ALERTVIEW_EMPTY_ITEM_NAME_TAG:
            [self.itemNameTextField becomeFirstResponder];
            break;
        case ALERTVIEW_EMPTY_PRICE_TAG:
            [self.priceTextField becomeFirstResponder];
            break;
        case ALERTVIEW_EMPTY_DESCRIPTION_TAG:
            [self.descriptionTextField becomeFirstResponder];
            break;
        case ALERTVIEW_EMPTY_LOCATION_TAG:
            [self.locationTextField becomeFirstResponder];
            break;
            
        default:
            break;
    }
    
}

#pragma mark - UITableview Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [socialNetworkAccounts count];
    
}

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [self getCellContentView:CellIdentifier];
    }
    
    [cell setSelectionStyle:UITableViewCellEditingStyleNone];
    
    [selectedIndexes removeAllObjects];
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    [selectedIndexes addObject:[NSNumber numberWithInt:indexPath.row]];
    /*
    for (int i = 0; i < selectedIndexes.count; i++) {
        NSUInteger num = [[selectedIndexes objectAtIndex:i] intValue];
        
        if (num == indexPath.row) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            break;
        }
    }
     */
    
    if(![[[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONApiRealNameKeyName] isEqual:[NSNull null]]){
        [(UILabel *)[cell.contentView viewWithTag:CELL_ACCOUNT_NAME_TAG] setText:[[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONApiRealNameKeyName]];
    }
    
    
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {	
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [selectedIndexes removeObject:[NSNumber numberWithInt:indexPath.row]];
    }else{
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [selectedIndexes addObject:[NSNumber numberWithInt:indexPath.row]];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *apiAccountId = [[socialNetworkAccounts objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName];
    
    [socialNetworkAccounts removeObjectAtIndex:indexPath.row];
    [apiAccountsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
    if([socialNetworkAccounts count] == 0){
        [apiAccountsTableView setHidden:YES];
    }
    
    NSURL *editedDeleteApiAccountUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kDeleteApiAccountURL, [Utilities commonParams], kJSONApiAccountIdKeyName, apiAccountId]];
    if(kLoggingEnabled)NSLog(@"edited get api accounts url: %@", editedDeleteApiAccountUrl);
    NSMutableURLRequest *deleteApiAccountRequest = [NSMutableURLRequest requestWithURL:editedDeleteApiAccountUrl];
    [deleteApiAccountRequest setTimeoutInterval:kUrlRequestTimeout];
    [deleteApiAccountRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *deleteApiAccountOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:deleteApiAccountRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            if(kLoggingEnabled)NSLog(@"delete api account success response JSON: %@", JSON);
            
            
        }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            if(kLoggingEnabled)NSLog(@"load api accounts failure JSON: %@", JSON);
            
            
        }else{
            
            
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(kLoggingEnabled)NSLog(@"load api accounts failure error: %@", error);
    }];
    
    [deleteApiAccountOperation start];
    
}

-(UITableViewCell *)getCellContentView:(NSString *)CellIdentifier{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(11, 3, 38, 38)];
    [image.layer setMasksToBounds:YES];
    [image.layer setCornerRadius:3.0];
    [image setTag:CELL_IMAGE_TAG];
    [cell.contentView addSubview:image];
    
    UIImageView *apiSourceImage = [[UIImageView alloc] initWithFrame:CGRectMake(6, 32, 14, 14)];
    [apiSourceImage.layer setMasksToBounds:YES];
    [apiSourceImage setTag:CELL_API_SOURCE_IMAGE_TAG];
    [cell.contentView addSubview:apiSourceImage];
    
    UILabel *accountName = [[UILabel alloc] initWithFrame:CGRectMake(56, 0, 200, kCellHeight)];
    [accountName setBackgroundColor:[UIColor clearColor]];
    [accountName setTextAlignment:UITextAlignmentLeft];
    [accountName setTag:CELL_ACCOUNT_NAME_TAG];
    [accountName setTextColor:[UIColor darkTextColor]];
    [accountName setFont:[UIFont boldSystemFontOfSize:20]];
    [cell.contentView addSubview:accountName];
    
    return cell;
}

#pragma mark -
#pragma mark ScrollView Overrides

// Load images for all onscreen rows when scrolling is finished

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(![apiAccountsTableView isDragging]){
        [self loadImagesForOnscreenRows];
    }
}


@end
