//
//  AppDelegate.h
//  slinggit
//
//  Created by Phil Beadle on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LoginViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    enum tabBarViewControllerTypes
    {
        FEED_NAVIGATION_CONTROLLER,
        SELL_NAVIGATION_CONTROLLER,
        MESSAGE_NAVIGATION_CONTROLLER,
        SETTINGS_NAVIGATION_CONTROLLER
    } TabBarViewControllerTypes;
}

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) LoginViewController *loginViewController;
//@property (strong, nonatomic) UINavigationController *mainNavController;
@property (strong, nonatomic) UITabBarController *mainTabBarController;

-(void)presentLoginViewController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)popAllViewControllers;
- (void)loadMessageDataOnViewWillAppear;
- (void)loadPostDataOnViewWillAppear:(bool)showMyPosts;
- (void)getMessageData;

@end
