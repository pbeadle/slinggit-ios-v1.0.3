//
//  Utilities.h
//  slinggit
//
//  Created by Phil Beadle on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@interface Utilities : NSObject{
    
}

+(NSString *)getDeviceID;
+(NSString *)stringByEncodingHTMLEntities:(NSString *)text;
+(NSString *) platformString;
+(NSString *)getStateWithFullStateName:(NSString *)fullStateName;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(NSString *)commonParams;
+(NSString *)newStringInBase64FromData:(NSData *)data;
+(NSData *)dataFromBase64String: (NSString *)string;
+(void) saveData:(NSString *)base64PhotoString withPostId:(NSNumber *)postId;
+(int) numberOfPhotoRecords;
+(void)deleteAllPhotoRecords;
+(void)deleteOldestPhotoRecord;
+(NSString *) findPhotoWithPostId:(NSNumber *)postId;
+(NSString *)getElaspsedTimeFromPostDictionary:(NSDictionary *)postDictionary;
+(UIEdgeInsets)getEdgeInsetsForImage:(UIImage *)image;
+(void)markMessageRead:(NSNumber *)messageId;
+(void)downloadDefaultPlaceholderImage;
+(UIImage*)getDefaultImage;
//+(void)getApiAccounts;
+(void)forceLoadMessageDataOnViewWillAppear;
+(void)forceLoadPostDataOnViewWillAppearAndShowMyPosts:(bool)showMyPosts;

@end