//
//  Constants.h
//  slinggit
//
//  Created by Phil Beadle on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

extern BOOL const kLoggingEnabled;

extern NSString *const kMobileAuthTokenKeyName;
extern NSString *const kAccessTokenKeyName;
extern NSString *const kAccessToken;
extern NSString *const versionNumber;

extern int const kScreenWidth;
extern int const kScreenHeightNoStatusBar;
extern int const kTabBarHeight;
extern int const kNavBarHeight;

//API ACCOUNT TYPES
#define API_ACCOUNT_TYPE_TWITTER 0
#define API_ACCOUNT_TYPE_FACEBOOK 1

//JSON STRINGS
extern NSString *const kJSONStatus;
extern NSString *const kJSONStatusSuccess;
extern NSString *const kJSONStatusError;
extern NSString *const kJSONResult;
extern NSString *const kJSONFriendlyError;
extern NSString *const kJSONLimitationType;
extern NSString *const kJSONLimitationPass;
extern NSString *const kJSONOffsetKeyName;
extern NSString *const kJSONStartingPostId;
extern NSString *const kJSONLimitKeyName;
extern NSString *const kJSONUsernameKeyName;
extern NSString *const kJSONUserIdKeyName;
extern NSString *const kJSONSearchTermKeyName;
extern NSString *const kJSONHashTagPrefixKeyName;
extern NSString *const kJSONPriceKeyName;
extern NSString *const kJSONPostId;
extern NSString *const kJSONPostContentKeyName;
extern NSString *const kJSONTwitterAccessTokenKeyName;
extern NSString *const kJSONTwitterAccessTokenSecretKeyName;
extern NSString *const kJSONRowsFoundKeyName;
extern NSString *const kJSONApiAccountsKeyName;
extern NSString *const kJSONApiRealNameKeyName;
extern NSString *const kJSONApiAccountIdKeyName;
extern NSString *const kJSONIdKeyName;
extern NSString *const kJSONCommentsKeyName;
extern NSString *const kJSONBodyKeyName;
extern NSString *const kJSONCommentBodyKeyName;
extern NSString *const kJSONCommentIdKeyName;
extern NSString *const kJSONLocationKeyName;
extern NSString *const kJSONMessageId;
extern NSString *const kJSONStartingMessageId;
extern NSString *const kJSONRecipientUserIdKeyName;
extern NSString *const kJSONNewEmailKeyName;
extern NSString *const kJSONOldPasswordKeyName;
extern NSString *const kJSONNewPasswordKeyName;
extern NSString *const kJSONEmailOrUsernameKeyName;
extern NSString *const kJSONEmailKeyName;
extern NSString *const kJSONMessageIdsKeyName;
extern NSString *const kJSONSourceIdKeyName;
extern NSString *const kJSONImageUriKeyName;
extern NSString *const kJSONImageStyleKeyName;
extern NSString *const kJSONImageStyle300x300;
extern NSString *const kJSONSourceObjectDataKeyName;
extern NSString *const kJSONParentMessageIdKeyName;
extern NSString *const kJSONSenderUserIdKeyName;
extern NSString *const kJSONContactInfoKeyName;
extern NSString *const kJSONSourceKeyName;
extern NSString *const kJSONPostKeyName;
extern NSString *const kJSONImageStyleMedium;
extern NSString *const kJSONApiAccountIdsKeyName;
extern NSString *const kJSONContentKeyName;
extern NSString *const kJSONApiIdKeyName;
extern NSString *const kJSONMesagesNumberUnread;
extern NSString *const kJSONApiSourceKeyName;
extern NSString *const kJSONTwitter;
extern NSString *const kJSONFacebook;
extern NSString *const kJSONStartingCommentId;
extern NSString *const kJSONImageUrlKeyName;
extern NSString *const kJSONRecipientStatusKeyName;

//JSON STATUS CODES
extern NSString *const kJSONStatusUnread;
extern NSString *const kJSONStatusRead;
extern NSString *const kJSONStatusDelete;


//ERROR TEXT
extern NSString *const kGeneralProblemErrorTitle;
extern NSString *const kConnectionErrorTitle;
extern NSString *const kConnectionErrorText;
extern NSString *const kMissingEmailError;
extern NSString *const kMissingPasswordError;
extern NSString *const kPostErrorTitle;
extern NSString *const kPostErrorText;
extern NSString *const kLoginErrorTitle;
extern NSString *const kChangeErrorTitle;
extern NSString *const kSignUpErrorTitle;
extern NSString *const kResetPasswordErrorTitle;
extern NSString *const kResetPasswordError;
extern NSString *const kInvalidUsernameErrorTitle;
extern NSString *const kInvalidUsernameError;
extern NSString *const kAddAccountErrorTitle;
extern NSString *const kDeleteAccountErrorTitle;
extern NSString *const kSendErrorTitle;
extern NSString *const kInvalidPasswordErrorTitle;
extern NSString *const kInvalidPasswordError;
extern NSString *const kPasswordsDoNotMatchError;
extern NSString *const kPasswordsMatchError;
extern NSString *const kInvalidEmailErrorTitle;
extern NSString *const kInvalidEmailError;
extern NSString *const kNoCameraErrorTitle;
extern NSString *const kNoCameraError;
extern NSString *const kDeleteErrorTitle;
extern NSString *const kCloseErrorTitle;
extern NSString *const kReportAbuseErrorTitle;
extern NSString *const kAddCommentErrorTitle;
extern NSString *const kAddWatchPostErrorTitle;
extern NSString *const kRemoveWatchPostErrorTitle;

//LIMITS
extern int kItemNameFieldLimit;
extern int kPriceFieldLimit;
extern int kLocationFieldLimit;
extern int kDescriptionFieldLimit;
extern int kCommentFieldLimit;
extern int kSendMessageFieldLimit;
extern int kFeedShowLimit;
extern int kMessageShowLimit;
extern int kCommentShowLimit;
extern int kPhotoDownloadStorageNumberLimit;
extern int kUrlRequestTimeout;

//FONT SIZES
extern int kDateFontSize;

//URLS
extern NSString *const kSignInURL;
extern NSString *const kSignInStatusURL;
extern NSString *const kResetPasswordURL;
extern NSString *const kSignUpURL;
extern NSString *const kSignOutURL;
extern NSString *const kGetSlinggitPostDataURL;
extern NSString *const kGetWatchedPostsURL;
extern NSString *const kCheckPostLimitationsURL;
extern NSString *const kCreatePostURL;
extern NSString *const kAddTwitterAccountURL;
extern NSString *const kAddFacebookAccountURL;
extern NSString *const kGetUserApiAccountsURL;
extern NSString *const kDeleteApiAccountURL;
extern NSString *const kGetSingleSlinggitPostDataURL;
extern NSString *const kCreatePostCommentURL;
extern NSString *const kDeletePostCommentURL;
extern NSString *const kGetMessagesURL;
extern NSString *const kSendMessageURL;
extern NSString *const kDeleteMessageURL;
extern NSString *const kChangePasswordURL;
extern NSString *const kChangeEmailURL;
extern NSString *const kMarkMessageReadURL;
extern NSString *const kGetPostImageURL;
extern NSString *const kReplyToMessageURL;
extern NSString *const kReportAbuseURL;
extern NSString *const kClosePostURL;
extern NSString *const kDeletePostURL;
extern NSString *const kGetPostCommentsURL;
extern NSString *const kTermsOfServiceURL;
extern NSString *const kAddWatchedPostURL;
extern NSString *const kRemoveWatchedPostURL;

#define PREFS_UNIQUE_ID @"state"
#define PREFS_TOKEN_KEY @"token"
#define PREFS_USERNAME @"user_name"
#define PREFS_USER_ID @"user_id"

#define TEXT_BACKGROUND_COLOR [UIColor colorWithRed:199/255.0 green:227/255.0 blue:241/255.0 alpha:0.6]

#define COLOR_SLINGGIT_RED [UIColor colorWithRed:253/255.0 green:65/255.0 blue:40/255.0 alpha:1.0]
#define COLOR_SLINGGIT_BLUE [UIColor colorWithRed:72/255.0 green:141/255.0 blue:226/255.0 alpha:1.0]
#define COLOR_SLINGGIT_BLUE_FADED [UIColor colorWithRed:72/255.0 green:141/255.0 blue:226/255.0 alpha:0.1]
#define COLOR_SLINGGIT_GREEN [UIColor colorWithRed:75/255.0 green:192/255.0 blue:75/255.0 alpha:1.0]
#define COLOR_SLINGGIT_YELLOW [UIColor colorWithRed:255/255.0 green:183/255.0 blue:40/255.0 alpha:1.0]
#define COLOR_SLINGGIT_GRAY [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0]
#define COLOR_SLINGGIT_LIGHT_GRAY [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1.0]
#define COLOR_SLINGGIT_WHITE [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]