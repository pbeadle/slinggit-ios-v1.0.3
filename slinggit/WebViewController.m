//
//  WebViewController.m
//  slinggit
//
//  Created by Phil Beadle on 5/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WebViewController.h"
#import "Constants.h"
#import "Utilities.h"
#import "KeychainWrapper.h"
#import "SVProgressHUD.h"

@interface WebViewController (){
    UIActivityIndicatorView *indicatorView;
}

@end

@implementation WebViewController

@synthesize accountType;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    [super loadView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar - kNavBarHeight)];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicatorView setFrame:CGRectMake(0, 0, 100, 100)];
    [indicatorView startAnimating];
    [view addSubview:indicatorView];
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:view.frame];
    [webView setDelegate:self];
    [view addSubview:webView];
    
    NSString *username = @"";
    if([KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USERNAME]){
        username = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USERNAME];
    }
    
    NSString *accountTypeURL;
    switch (accountType) {
        case API_ACCOUNT_TYPE_TWITTER:
            accountTypeURL = kAddTwitterAccountURL;
            break;
        case API_ACCOUNT_TYPE_FACEBOOK:
            accountTypeURL = kAddFacebookAccountURL;
            break;
        default:
            break;
    }
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", accountTypeURL, [Utilities commonParams], kJSONUsernameKeyName, username]]];
    if(kLoggingEnabled)NSLog(@"webview url to load initially: %@", [urlRequest URL]);
    [urlRequest setHTTPMethod:@"POST"];
    
    [webView loadRequest:urlRequest];
    
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setTitle:@"Add Account"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{                           
    NSString *urlString = [[request URL] absoluteString];
    if(kLoggingEnabled)NSLog(@"webview url to load via redirect: %@", urlString);
    [SVProgressHUD showWithStatus:@"Loading"];
    if([urlString rangeOfString:@"finalize_add_twitter_account"].location != NSNotFound){
        
        NSString *parameters = [urlString substringFromIndex:([urlString rangeOfString:@"?"].location+1)];
                                                              
        NSMutableDictionary *twitterParams = [[NSMutableDictionary alloc] init];
        NSArray *arrParameters = [parameters componentsSeparatedByString:@"&"];
        for (int i = 0; i < [arrParameters count]; i++) {
            NSArray *arrKeyValue = [[arrParameters objectAtIndex:i] componentsSeparatedByString:@"="];
            if ([arrKeyValue count] >= 2) {
                NSMutableString *strKey = [NSMutableString stringWithCapacity:0];
                [strKey setString:[[[arrKeyValue objectAtIndex:0] lowercaseString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSMutableString *strValue   = [NSMutableString stringWithCapacity:0];
                [strValue setString:[[[arrKeyValue objectAtIndex:1]  stringByReplacingOccurrencesOfString:@"+" withString:@" "] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                if (strKey.length > 0) [twitterParams setObject:strValue forKey:strKey];
            }
        }
        [twitterParams setObject:@"twitter" forKey:@"account_type"];
        if(kLoggingEnabled)NSLog(@"twitter parameters: %@", twitterParams);
        
        [self.delegate webViewController:self didFinishAddingAccountWithInformation:twitterParams];
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        
        return NO;
        
    }else if([urlString rangeOfString:@"finalize_add_facebook_account"].location != NSNotFound){
        
        NSString *parameters = [urlString substringFromIndex:([urlString rangeOfString:@"?"].location+1)];
        
        NSMutableDictionary *facebookParams = [[NSMutableDictionary alloc] init];
        NSArray *arrParameters = [parameters componentsSeparatedByString:@"&"];
        for (int i = 0; i < [arrParameters count]; i++) {
            NSArray *arrKeyValue = [[arrParameters objectAtIndex:i] componentsSeparatedByString:@"="];
            if ([arrKeyValue count] >= 2) {
                NSMutableString *strKey = [NSMutableString stringWithCapacity:0];
                [strKey setString:[[[arrKeyValue objectAtIndex:0] lowercaseString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSMutableString *strValue   = [NSMutableString stringWithCapacity:0];
                [strValue setString:[[[arrKeyValue objectAtIndex:1]  stringByReplacingOccurrencesOfString:@"+" withString:@" "] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                if (strKey.length > 0) [facebookParams setObject:strValue forKey:strKey];
            }
        }
        [facebookParams setObject:@"facebook" forKey:@"account_type"];
        if(kLoggingEnabled)NSLog(@"\n\n\n*************************facebook parameters: %@", facebookParams);
        
        [self.delegate webViewController:self didFinishAddingAccountWithInformation:facebookParams];
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        
        return NO;
    }
    
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss]; 
}


@end
