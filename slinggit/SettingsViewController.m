//
//  SettingsViewController.m
//  slinggit
//
//  Created by Phil Beadle on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "AFJSONRequestOperation.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "KeychainWrapper.h"
#import "Utilities.h"
#import "ApiAccountsViewController.h"
#import "SVProgressHUD.h"

#define TAG_CHANGE_EMAIL_ALERTVIEW 10
#define TAG_CHANGE_PASSWORD_ALERTVIEW 11
#define TAG_INVALID_PASSWORD_ALERTVIEW 12
#define TAG_INVALID_EMAIL_ALERTVIEW 13
#define TAG_PASSWORDS_MATCH_ALERTVIEW 14

@interface SettingsViewController (){
    NSString *state;
}
@end

@implementation SettingsViewController

@synthesize settingsTableView = _settingsTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    [super loadView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.5];
    [view addSubview:backgroundImageView];
    
    self.settingsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 411) style:UITableViewStyleGrouped];
    [self.settingsTableView setDelegate:self];
    [self.settingsTableView setDataSource:self];
    [self.settingsTableView setBackgroundColor:[UIColor clearColor]];
    [view addSubview:self.settingsTableView];
    
    state = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_UNIQUE_ID];
    
    self.view = view;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setTitle:@"Settings"];
    [self.navigationController.navigationBar setTintColor:COLOR_SLINGGIT_BLUE];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.settingsTableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableview Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 4;
            break;
        case 1:
            return 1;
        default:
            break;
    }
    return 0;
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UILabel *accountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [accountLabel setText:@"   Account"];
        [accountLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
        [accountLabel setTextColor:[UIColor whiteColor]];
        [accountLabel setShadowColor:[UIColor darkGrayColor]];
        [accountLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
        accountLabel.textAlignment = UITextAlignmentLeft;
        [accountLabel setBackgroundColor:[UIColor clearColor]];
        return accountLabel;
    }
	
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 40.0;
    }
    
    return 0;
}
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        if(indexPath.section == 1){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
        
    }
    
    switch (indexPath.section) {
        case 0:
            
            switch (indexPath.row) {
                case 0:
                    
                    [cell.textLabel setText:@"Username"];
                    [cell.detailTextLabel setText:[KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USERNAME]];
                    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    break;
                case 1:
                    [cell.textLabel setText:@"Social Networks"];
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    break;
                    
                case 2:
                    [cell.textLabel setText:@"Change Email"];
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    break;
                    
                case 3:
                    [cell.textLabel setText:@"Change Password"];
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    break;
                default:
                    break;
            }
            
            break;
            
        case 1:
            
            cell.textLabel.text = @"Sign Out";
            cell.textLabel.textAlignment = UITextAlignmentCenter;
            
        default:
            break;
    }
    

    
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {	
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    
                    break;
                case 1:
                    [self loadApiAccountsView];
                    break;
                case 2:
                    [self showChangeEmailAlertView];
                    break;
                case 3:
                    [self showChangePasswordAlertView];
                    break;
                default:
                    break;
            }
            
            break;
        case 1:
            [self signOutUser];
            
            break;

        default:
            break;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section == 1){
        return 50;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if(section == 1){
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 100)];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 68, 300, 20)];
        label1.backgroundColor = [UIColor clearColor];
		label1.font = [UIFont systemFontOfSize:14.0];
        [label1 setTextColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
        [label1 setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        [label1 setShadowOffset:CGSizeMake(0.0, 1.0)];
        label1.textAlignment = UITextAlignmentCenter;
        label1.lineBreakMode = UILineBreakModeWordWrap;
        label1.numberOfLines = 0;
        label1.text = [NSString stringWithFormat:[NSString stringWithFormat:@"%@ v%@", @"Slinggit", versionNumber]];
        [footerView addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 88, 300, 20)];
        label2.backgroundColor = [UIColor clearColor];
		label2.font = [UIFont systemFontOfSize:14.0];
        [label2 setTextColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
        [label2 setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        [label2 setShadowOffset:CGSizeMake(0.0, 1.0)];
        label2.textAlignment = UITextAlignmentCenter;
        label2.lineBreakMode = UILineBreakModeWordWrap;
        label2.numberOfLines = 0;
        label2.text = [NSString stringWithFormat:@"Copyright © 2012 Slinggit LLC\n\n"];
        [footerView addSubview:label2];
        
        return footerView;
        
    }
    
    return nil;
}

#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == TAG_CHANGE_PASSWORD_ALERTVIEW){
        if(buttonIndex == 1){
            if([alertView textFieldAtIndex:0].text != nil && [[alertView textFieldAtIndex:0].text length] >= 6 && [alertView textFieldAtIndex:1].text != nil && [[alertView textFieldAtIndex:1].text length] >= 6){
                if(![[alertView textFieldAtIndex:0].text isEqualToString:[alertView textFieldAtIndex:1].text]){
                    [self changeOldPassword:[alertView textFieldAtIndex:0].text withNewPassword:[alertView textFieldAtIndex:1].text];
                }else{
                    UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kInvalidPasswordErrorTitle message:kPasswordsMatchError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [errorAlertView setTag:TAG_PASSWORDS_MATCH_ALERTVIEW];
                    [errorAlertView show];
                }
            }else{
                UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kInvalidPasswordErrorTitle message:kInvalidPasswordError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [errorAlertView setTag:TAG_INVALID_PASSWORD_ALERTVIEW];
                [errorAlertView show];
            }
        }
    }else if(alertView.tag == TAG_INVALID_PASSWORD_ALERTVIEW || alertView.tag == TAG_PASSWORDS_MATCH_ALERTVIEW){
        [self showChangePasswordAlertView];
    
    }else if(alertView.tag == TAG_CHANGE_EMAIL_ALERTVIEW){
        if(buttonIndex == 1){
            if([alertView textFieldAtIndex:0].text != nil && [[alertView textFieldAtIndex:0].text length] > 0){
                [self changeEmail:[alertView textFieldAtIndex:0].text];
            }else{
                UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kInvalidEmailErrorTitle message:kInvalidEmailError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [errorAlertView setTag:TAG_INVALID_EMAIL_ALERTVIEW];
                [errorAlertView show];
            }
            
        }
    }else if(alertView.tag == TAG_INVALID_EMAIL_ALERTVIEW){
        [self showChangeEmailAlertView];
    }
}

-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    if(alertView.tag == TAG_CHANGE_PASSWORD_ALERTVIEW){
        UITextField *textField = [alertView textFieldAtIndex:0];
        UITextField *textField2 = [alertView textFieldAtIndex:1];
        if([textField.text length] < 6 || [textField2.text length] < 6){
            return NO;
        }else{
            return YES;
        }
        
    }else if(alertView.tag == TAG_CHANGE_EMAIL_ALERTVIEW){
        UITextField *textField = [alertView textFieldAtIndex:0];
        return([self emailValidated:textField.text]);
    }
    
    return YES;
}

#pragma mark - My Methods

-(void)showChangeEmailAlertView{
    UIAlertView *resetPasswordAlertView = [[UIAlertView alloc] initWithTitle:@"Change Email" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [resetPasswordAlertView setTag:TAG_CHANGE_EMAIL_ALERTVIEW];
    [resetPasswordAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[resetPasswordAlertView textFieldAtIndex:0] setPlaceholder:@"New Email"];
    [resetPasswordAlertView show];
}

-(void)showChangePasswordAlertView{
    UIAlertView *resetPasswordAlertView = [[UIAlertView alloc] initWithTitle:@"Change Password" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [resetPasswordAlertView setTag:TAG_CHANGE_PASSWORD_ALERTVIEW];
    [resetPasswordAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[resetPasswordAlertView textFieldAtIndex:0] setPlaceholder:@"Old Existing Password"];
    [[resetPasswordAlertView textFieldAtIndex:0] setSecureTextEntry:YES];
    [[resetPasswordAlertView textFieldAtIndex:1] setPlaceholder:@"New Password"];
    [[resetPasswordAlertView textFieldAtIndex:1] setSecureTextEntry:YES];
    [resetPasswordAlertView show];
}

-(void)signOutUser{
    [SVProgressHUD showWithStatus:@"Signing Out"];
    [Utilities deleteAllPhotoRecords];
    NSURL *editedLogoutUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kSignOutURL, [Utilities commonParams]]];
    if(kLoggingEnabled)NSLog(@"logut editedURL: %@", editedLogoutUrl);
    NSMutableURLRequest *logoutRequest = [NSMutableURLRequest requestWithURL:editedLogoutUrl];
    [logoutRequest setTimeoutInterval:5];
    [logoutRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *logoutOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:logoutRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"logout successful: %@", JSON);
        
        AppDelegate *myAppDelegate = [[UIApplication sharedApplication] delegate];
        [myAppDelegate popAllViewControllers];
        [myAppDelegate presentLoginViewController];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"logout failure error: %@ \n\n JSON: %@", error, JSON);
        
        AppDelegate *myAppDelegate = [[UIApplication sharedApplication] delegate];
        [myAppDelegate popAllViewControllers];
        [myAppDelegate presentLoginViewController];
    }];
    
    [logoutOperation start];
    
    [KeychainWrapper deleteItemFromKeychainWithIdentifier:PREFS_USERNAME];
    [KeychainWrapper deleteItemFromKeychainWithIdentifier:PREFS_TOKEN_KEY];
}

-(void)loadApiAccountsView{
    ApiAccountsViewController *apiAccountsViewController = [[ApiAccountsViewController alloc] init];
    [self.navigationController pushViewController:apiAccountsViewController animated:YES];
}

-(void)changeEmail:(NSString *)newEmail{
    
    [SVProgressHUD showWithStatus:@"Changing"];
    
    NSString *encodedEmail = [Utilities stringByEncodingHTMLEntities:newEmail];
    
    NSURL *editedChangeEmailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kChangeEmailURL, [Utilities commonParams], kJSONNewEmailKeyName, encodedEmail]];
    
    if(kLoggingEnabled)NSLog(@"edited change email URL: %@", editedChangeEmailUrl);
    NSMutableURLRequest *changeEmailRequest = [NSMutableURLRequest requestWithURL:editedChangeEmailUrl];
    [changeEmailRequest setTimeoutInterval:kUrlRequestTimeout];
    [changeEmailRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *changeEmailOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:changeEmailRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"change email response: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            if(kLoggingEnabled)NSLog(@"change email response: %@", resultDictionary);
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary valueForKeyPath:kJSONFriendlyError];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kChangeErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kChangeErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"change email failure error: JSON: \n%@", JSON);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kChangeErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
    }];
    
    [changeEmailOperation start];
    
}

-(void)changeOldPassword:(NSString *)oldPassword withNewPassword:(NSString *)newPassword{
    
    [SVProgressHUD showWithStatus:@"Changing"];
    
    NSString *encodedOldPassword = [Utilities stringByEncodingHTMLEntities:oldPassword];
    NSString *encodedNewPassword = [Utilities stringByEncodingHTMLEntities:newPassword];
    
    NSURL *editedChangePasswordUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@", kChangePasswordURL, [Utilities commonParams], kJSONOldPasswordKeyName, encodedOldPassword, kJSONNewPasswordKeyName, encodedNewPassword]];
    
    if(kLoggingEnabled)NSLog(@"edited change p URL: %@", editedChangePasswordUrl);
    NSMutableURLRequest *changePasswordRequest = [NSMutableURLRequest requestWithURL:editedChangePasswordUrl];
    [changePasswordRequest setTimeoutInterval:kUrlRequestTimeout];
    [changePasswordRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *changePasswordOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:changePasswordRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"change p response: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            if(kLoggingEnabled)NSLog(@"change p response: %@", resultDictionary);
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary valueForKeyPath:kJSONFriendlyError];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kChangeErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kChangeErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"change p failure error: JSON: \n%@", JSON);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kChangeErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
    }];
    
    [changePasswordOperation start];
    
}

-(BOOL)emailValidated:(NSString *)emailText{
    bool emailErrorFound = YES;

    if(emailText.length > 0){
        NSError *error = NULL;
        NSRegularExpression *emailRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" options:NSRegularExpressionCaseInsensitive error:&error];
        
        //if(kLoggingEnabled)NSLog(@"about to hit email check");
        NSTextCheckingResult *emailTextCheckResult = [emailRegex firstMatchInString:emailText options:0 range:NSMakeRange(0, emailText.length)];
        
        if(emailTextCheckResult){
            //NSRange matchRange = [textCheckingResult rangeAtIndex:0];
            //NSString *match = [emailText substringWithRange:matchRange];
            //if(kLoggingEnabled)NSLog(@"email field definitely is valid: %@", match);
            emailErrorFound = NO;
            
        }
    }
    if(emailErrorFound){
        
        return NO;
    }
    
    return YES;
}


@end
