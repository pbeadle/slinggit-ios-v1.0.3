#import <UIKit/UIKit.h>
#import "WebViewController.h"
#import "MyTextField.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate, UIGestureRecognizerDelegate, WebViewControllerDelegate, UIAlertViewDelegate> {
    
    
    
}

@property (nonatomic, strong) MyTextField *usernameTextField;
@property (nonatomic, strong) MyTextField *emailTextField;
@property (nonatomic, strong) MyTextField *passwordTextField;



@end