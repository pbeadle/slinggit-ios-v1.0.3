#import <UIKit/UIKit.h>
#import "MyTextField.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate> {
    
    
    
}

@property (nonatomic, strong) MyTextField *emailTextField;
@property (nonatomic, strong) MyTextField *passwordTextField;

@end