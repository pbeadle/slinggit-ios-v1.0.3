//
//  SendMessageViewController.m
//  slinggit
//
//  Created by Phil Beadle on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SendMessageViewController.h"
#import "Constants.h"
#import "Utilities.h"
#import "SVProgressHUD.h"
#import "AFJSONRequestOperation.h"

#define MESSAGE_PLACEHOLDER_TEXT @"Message to seller"

@interface SendMessageViewController ()

@end

@implementation SendMessageViewController

@synthesize messageTextField;
@synthesize recipientUserId;
@synthesize postId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    UIView *view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.5];
    [view addSubview:backgroundImageView];
    
    UIView *messageBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 180)];
    
    
    UIImageView *textInputImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [textInputImageView setFrame:CGRectMake(0, 0, messageBackgroundView.frame.size.width, messageBackgroundView.frame.size.height)];
    [messageBackgroundView addSubview:textInputImageView];
    
    self.messageTextField = [[UITextView alloc] initWithFrame:CGRectMake(2, 2, messageBackgroundView.frame.size.width-2, messageBackgroundView.frame.size.height-4)];
    [self.messageTextField setDelegate:self];
    [self.messageTextField setText:MESSAGE_PLACEHOLDER_TEXT];
    [self.messageTextField setTextColor:[UIColor lightGrayColor]];
    [self.messageTextField setBackgroundColor:[UIColor clearColor]];
    [self.messageTextField setFont:[UIFont systemFontOfSize:14.0]];
    [self.messageTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
    [self.messageTextField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [messageBackgroundView addSubview:self.messageTextField];
    
    [view addSubview:messageBackgroundView];
    
    self.view = view;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Send Message"];
    [self.navigationController.navigationBar setTintColor:COLOR_SLINGGIT_BLUE];
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStylePlain target:self action:@selector(sendButtonPressed)];
    [sendButton setEnabled:NO];
    [self.navigationItem setRightBarButtonItem:sendButton];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITextView Delegate Methods
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:MESSAGE_PLACEHOLDER_TEXT]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        
    }
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        [textView setTextColor:[UIColor lightGrayColor]];
        [textView setText:MESSAGE_PLACEHOLDER_TEXT];
        [[self.navigationItem rightBarButtonItem] setEnabled:NO];
    }

    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    int maxLength = kSendMessageFieldLimit;
    if([[textView text] length] == 0 && text.length == 0){
        [textView setTextColor:[UIColor lightGrayColor]];
        [textView setText:MESSAGE_PLACEHOLDER_TEXT];
        [textView resignFirstResponder];
        [[self.navigationItem rightBarButtonItem] setEnabled:NO];
    }else{
        [[self.navigationItem rightBarButtonItem] setEnabled:YES];
    }
    
    if([[textView text] length] + text.length > maxLength){
        [[self.navigationItem rightBarButtonItem] setEnabled:NO];
        return NO;
    }
    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView{
    if([[textView text] length] == 0){
        [[self.navigationItem rightBarButtonItem] setEnabled:NO];
    }else{
        [[self.navigationItem rightBarButtonItem] setEnabled:YES];
    }
}

#pragma mark - Button Presses
-(void)sendButtonPressed{
    
    [SVProgressHUD showWithStatus:@"Sending"];
    
    NSString *encodedMessage = [Utilities stringByEncodingHTMLEntities:self.messageTextField.text];
    
     NSURL *editedSendMessageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@&%@=%@", kSendMessageURL, [Utilities commonParams], kJSONPostId, self.postId, kJSONRecipientUserIdKeyName, recipientUserId, kJSONBodyKeyName, encodedMessage]];
    
    if(kLoggingEnabled)NSLog(@"edited send message URL: %@", editedSendMessageUrl);
    NSMutableURLRequest *sendMessageRequest = [NSMutableURLRequest requestWithURL:editedSendMessageUrl];
    [sendMessageRequest setTimeoutInterval:kUrlRequestTimeout];
    [sendMessageRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *sendMessageOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:sendMessageRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            if(kLoggingEnabled)NSLog(@"send message response: %@", resultDictionary);
            
            [self.navigationController popViewControllerAnimated:YES];
        
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            if(kLoggingEnabled)NSLog(@"send message error response: %@", resultDictionary);

            NSString *errorMessage = [resultDictionary valueForKeyPath:kJSONFriendlyError];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kSendErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kSendErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"send message failure error: JSON: \n%@", JSON);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kSendErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
    }];
    
    [sendMessageOperation start];
    
}

@end
