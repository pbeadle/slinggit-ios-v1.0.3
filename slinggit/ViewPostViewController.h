//
//  ViewPostViewController.h
//  slinggit
//
//  Created by Phil Beadle on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"

@interface ViewPostViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate, UITextFieldDelegate, PullToRefreshViewDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate>


@property(nonatomic, strong) NSDictionary *postDictionary;
@property(nonatomic, assign) bool shouldShowWatchPostButton;

@end
