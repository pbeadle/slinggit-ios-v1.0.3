//
//  FeedViewController.m
//  slinggit
//
//  Created by Phil Beadle on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessagesViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "PullToRefreshView.h"
#import "Utilities.h"
#import "KeychainWrapper.h"
#import "AFJSONRequestOperation.h"
#import "ReplyToMessageViewController.h"

#define CELL_IMAGE_TAG 10
#define CELL_TITLE_TAG 11
#define CELL_SUBTITLE_TAG 12
#define CELL_PRICE_TAG 13
#define CELL_DATE_TAG 14
#define CELL_EMAIL_TAG 15

#define TAG_LOADING_ACTIVITY_INDICATOR 16
#define TAG_LOADING_LABEL 17

#define kCellHeight 70
#define kShowMoreCellHeight 50

#define kReadTextColor [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0]

@interface MessagesViewController (){
    UITableView *messagesTableView;
    BOOL reloading;
    BOOL checkForRefresh;
    PullToRefreshView *pullRefreshView;
    NSMutableArray *sourceData;
    BOOL shouldShowMoreButton;
    UIBarButtonItem *editButton;
    //UILabel *noMessagesLabel;
    UIView *noMessagesView;
}
@end

@implementation MessagesViewController

@synthesize shouldLoadDataOnViewWillAppear;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    [super loadView];
    
    shouldLoadDataOnViewWillAppear = YES;
    
    sourceData = [[NSMutableArray alloc] init];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"light_gray_background.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar)];
    [backgroundImageView setAlpha:0.3];
    [view addSubview:backgroundImageView];
    
    
    messagesTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight) style:UITableViewStylePlain];
    [messagesTableView setBackgroundColor:[UIColor clearColor]];
    [messagesTableView setDelegate:self];
	[messagesTableView setDataSource:self];
    pullRefreshView = [[PullToRefreshView alloc] initWithScrollView:(UIScrollView *)messagesTableView];
    [pullRefreshView setDelegate:self];
    [messagesTableView setAllowsMultipleSelectionDuringEditing:YES];
    [messagesTableView addSubview:pullRefreshView];
	[view addSubview:messagesTableView];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    [v setBackgroundColor:[UIColor clearColor]];
    [messagesTableView setTableFooterView:v];
    
    noMessagesView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, kScreenWidth, 100)];
    [noMessagesView setUserInteractionEnabled:NO];
    [noMessagesView setHidden:YES];
    
    UIImageView *messageIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"messages_tab_icon"]];
    messageIcon.contentMode = UIViewContentModeScaleAspectFit;
    [messageIcon setFrame:CGRectMake((kScreenWidth-48)/2, 30, 48, 33)];
    [messageIcon setAlpha:0.6];
    [noMessagesView addSubview:messageIcon];
    
    UILabel *noMessagesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, kScreenWidth, 20)];
    [noMessagesLabel setBackgroundColor:[UIColor clearColor]];
    [noMessagesLabel setFont:[UIFont systemFontOfSize:18]];
    [noMessagesLabel setTextAlignment:UITextAlignmentCenter];
    [noMessagesLabel setText:@"No Messages"];
    [noMessagesLabel setTextColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
    [noMessagesLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    [noMessagesLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [noMessagesView addSubview:noMessagesLabel];
    
    [view addSubview:noMessagesView];
    

    
    self.view = view;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationController.navigationBar.tintColor = COLOR_SLINGGIT_BLUE;
    [self setTitle:@"Messages"];
    
    editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:editButton];
    
    UIBarButtonItem *deleteMessagesButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete(0)" style:UIBarButtonItemStyleDone target:self action:@selector(deleteMessagesButtonPressed)];
    [deleteMessagesButton setEnabled:NO];
    
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [self setToolbarItems:[NSArray arrayWithObjects:deleteMessagesButton, nil] animated:YES];
    
     [self loadTableDataFromNewestPost];
   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //if(self.shouldLoadDataOnViewWillAppear){
        [self loadTableDataFromNewestPost];
    //}
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Button Presses

-(void)showMoreButtonPressed{

    if(sourceData && [sourceData count] > 0){
        
        NSString *startingMessageId = [[sourceData objectAtIndex:([sourceData count]-1)] objectForKey:kJSONIdKeyName];
        
        [self reloadTableViewDataSourceWithOffset:[NSNumber numberWithInt:0] withStartingMessageId:startingMessageId withLimit:[NSNumber numberWithInt:kMessageShowLimit] shouldClearSourceData:NO];
    }
}

-(void)editButtonPressed:(UIBarButtonItem *)button{
    if([messagesTableView isEditing]){
        [self turnEditModeOff];
       
    }else{
        [button setTitle:@"Cancel"];
        //[messagesTableView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight-44)]; 
        [messagesTableView setEditing:YES animated:YES];
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
}

-(void)deleteMessagesButtonPressed{
    [self.navigationController setToolbarHidden:YES animated:YES];
    [[[self.navigationController.toolbar items] objectAtIndex:0] setTitle:@"Delete(0)"];
    [[[self.navigationController.toolbar items] objectAtIndex:0] setEnabled:NO];
    
    [self deleteSelectedMessages];
}

#pragma mark - My Methods

-(void)turnEditModeOff{
    [editButton setTitle:@"Edit"];
    //[messagesTableView setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight)]; 
    [messagesTableView setEditing:NO animated:YES];
    [self.navigationController setToolbarHidden:YES animated:YES];
    [[[self.navigationController.toolbar items] objectAtIndex:0] setTitle:@"Delete(0)"];
    [[[self.navigationController.toolbar items] objectAtIndex:0] setEnabled:NO];
}

-(void)deleteSelectedMessages{
    NSMutableString *messageIds = [[NSMutableString alloc] init];
    NSMutableIndexSet *rowIndexSet = [NSMutableIndexSet indexSet];
    
    for(NSIndexPath *indexPath in [messagesTableView indexPathsForSelectedRows]){
        [messageIds appendString:[NSString stringWithFormat:@"%@,", [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName]]];
        [rowIndexSet addIndex:indexPath.row];
    }
    
    [sourceData removeObjectsAtIndexes:rowIndexSet];
    [messagesTableView deleteRowsAtIndexPaths:[messagesTableView indexPathsForSelectedRows] withRowAnimation:UITableViewRowAnimationRight];
    
    if([sourceData count] == 0){
        [noMessagesView setHidden:NO];  
        shouldShowMoreButton = NO;
    }
    
    [self deleteMessagesWithIds:messageIds shouldRefreshView:NO];
    
}

-(void)deleteMessagesWithIds:(NSString *)messageIds shouldRefreshView:(BOOL)shouldRefresh{
    
    NSURL *editedDeleteMessagesUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kDeleteMessageURL, [Utilities commonParams], kJSONMessageIdsKeyName, messageIds]];
    
    if(kLoggingEnabled)NSLog(@"delete messages URL: %@", editedDeleteMessagesUrl);
    NSMutableURLRequest *deleteMessagesRequest = [NSMutableURLRequest requestWithURL:editedDeleteMessagesUrl];
    [deleteMessagesRequest setTimeoutInterval:kUrlRequestTimeout];
    [deleteMessagesRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *deleteMessagesOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:deleteMessagesRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"delete message response: %@", JSON);
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];

            if(shouldRefresh){
                [self loadTableDataFromNewestPost];
            }
            
        }else{
            //error with request
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if(kLoggingEnabled)NSLog(@"delete message connection failed: %@", error);
        
    }];
    
    [deleteMessagesOperation start];
    
    if([messagesTableView isEditing]){
        [self turnEditModeOff];
    }
    [messagesTableView reloadData];
}

-(void)loadTableDataFromNewestPost{
    
    [self reloadTableViewDataSourceWithOffset:[NSNumber numberWithInt:0] withStartingMessageId:@"" withLimit:[NSNumber numberWithInt:kMessageShowLimit] shouldClearSourceData:YES];
}


- (void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view{
    
    [self loadTableDataFromNewestPost];
}

- (void) reloadTableViewDataSourceWithOffset:offset withStartingMessageId:startingMessageId withLimit:limit shouldClearSourceData:(BOOL)shouldClearSourceData{
    if([messagesTableView isEditing]){
        [self turnEditModeOff];
    }
    NSURL *editedGetMessagesUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@&%@=%@", kGetMessagesURL, [Utilities commonParams], kJSONOffsetKeyName, offset, kJSONLimitKeyName, limit, kJSONStartingMessageId, startingMessageId]];
    
    if(kLoggingEnabled)NSLog(@"get messages URL: %@", editedGetMessagesUrl);
    NSMutableURLRequest *getMessagesRequest = [NSMutableURLRequest requestWithURL:editedGetMessagesUrl];
    [getMessagesRequest setTimeoutInterval:kUrlRequestTimeout];
    [getMessagesRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getMessagesOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getMessagesRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"get messages response: %@", JSON);
        [self stopLoadingAnimation];
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            self.shouldLoadDataOnViewWillAppear = NO;
            if(shouldClearSourceData){
                [sourceData removeAllObjects];
            }
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            
            NSString *badgeValue = nil;
            if([[resultDictionary objectForKey:kJSONMesagesNumberUnread] intValue] > 0){
                badgeValue = [NSString stringWithFormat:@"%i", [[resultDictionary objectForKey:kJSONMesagesNumberUnread] intValue]];
            }else{
                badgeValue = nil;
            }
            
            [[[[[self tabBarController] tabBar] items] objectAtIndex:MESSAGE_NAVIGATION_CONTROLLER] setBadgeValue:badgeValue];
        
            int rowsFound = [[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue];
            if(rowsFound > 0){

                [noMessagesView setHidden:YES];
                
                for(NSDictionary *messageDictionary in [resultDictionary objectForKey:@"messages"]){
                    if(![[messageDictionary objectForKey:kJSONRecipientStatusKeyName] isEqual:kJSONStatusDelete]){
                        [sourceData addObject:messageDictionary];
                    }
                }
        
                if(rowsFound < kMessageShowLimit){
                    shouldShowMoreButton = NO;
                }else{
                    shouldShowMoreButton = YES;
                }
                
            }else{
                
                if([sourceData count] == 0){
                    [noMessagesView setHidden:NO];  
                }else{
                    [noMessagesView setHidden:YES];
                }
                shouldShowMoreButton = NO;
                
            }
            
            [messagesTableView reloadData];
            
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            shouldShowMoreButton = NO;
            [noMessagesView setHidden:NO];
            [messagesTableView reloadData];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary objectForKey:kJSONFriendlyError];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kGeneralProblemErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }else{
            shouldShowMoreButton = NO;
            [noMessagesView setHidden:NO];
            [messagesTableView reloadData];
            
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [self stopLoadingAnimation];
        if(kLoggingEnabled)NSLog(@"reload table connection failed: %@", error);
        shouldShowMoreButton = NO;
        [noMessagesView setHidden:NO];
        [sourceData removeAllObjects];
        [messagesTableView reloadData];
        [self stopLoadingAnimation];
        if(kLoggingEnabled)NSLog(@"reload table connection failed: %@", error);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kConnectionErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [getMessagesOperation start];
    
}

-(void)stopLoadingAnimation{
    [pullRefreshView finishedLoading]; 
}

#pragma mark - UITableview Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(sourceData){
        
        if([sourceData count] > 0){
            [[self.navigationItem rightBarButtonItem] setEnabled:YES];
        }else{
            [[self.navigationItem rightBarButtonItem] setEnabled:NO];
        }
        
        if(shouldShowMoreButton){
            return [sourceData count]+1;
        }else{
            return [sourceData count];
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([sourceData count] > 0){
        if(indexPath.row == [sourceData count]){
            return kShowMoreCellHeight;
        }
        return kCellHeight;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier;
    if(indexPath.row < [sourceData count]){
        CellIdentifier  = @"NormalCell";
    }else{
        CellIdentifier = @"FooterCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [self getCellContentView:CellIdentifier];
        
    }
    
    if(indexPath.row < [sourceData count]){
        NSDictionary *postDictionary = [sourceData objectAtIndex:indexPath.row];
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_DATE_TAG] setText:[Utilities getElaspsedTimeFromPostDictionary:postDictionary]];
        
        if([postDictionary objectForKey:kJSONContactInfoKeyName] && ![[postDictionary objectForKey:kJSONContactInfoKeyName] isEqual:[NSNull null]]){
            NSDictionary *contactInfoArray = [postDictionary objectForKey:kJSONContactInfoKeyName];
            if([contactInfoArray objectForKey:kJSONUsernameKeyName] && ![[contactInfoArray objectForKey:kJSONUsernameKeyName] isEqual:[NSNull null]]){
                [(UILabel *)[cell.contentView viewWithTag:CELL_EMAIL_TAG] setText:[contactInfoArray objectForKey:kJSONUsernameKeyName]];
            }else{
                [(UILabel *)[cell.contentView viewWithTag:CELL_EMAIL_TAG] setText:[contactInfoArray objectForKey:kJSONEmailKeyName]];
            }
        }
        [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setText:@"Message about your Slinggit post"];
        
        if(![[postDictionary objectForKey:kJSONSourceObjectDataKeyName] isEqual:[NSNull null]]){
            NSDictionary *sourceObjectDataDictionary = [postDictionary objectForKey:kJSONSourceObjectDataKeyName];
            if(![[sourceObjectDataDictionary objectForKey:kJSONHashTagPrefixKeyName] isEqual:[NSNull null]]){
                [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setText:[sourceObjectDataDictionary objectForKey:kJSONHashTagPrefixKeyName]];
            }
        }
        
        if([[postDictionary objectForKey:kJSONRecipientStatusKeyName] isEqual:kJSONStatusRead]){
            [(UILabel *)[cell.contentView viewWithTag:CELL_EMAIL_TAG] setTextColor:kReadTextColor];
            [(UILabel *)[cell.contentView viewWithTag:CELL_DATE_TAG] setTextColor:kReadTextColor];
            [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setTextColor:kReadTextColor];
            [(UILabel *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setTextColor:kReadTextColor];
        }else{
            [(UILabel *)[cell.contentView viewWithTag:CELL_EMAIL_TAG] setTextColor:COLOR_SLINGGIT_BLUE];
            [(UILabel *)[cell.contentView viewWithTag:CELL_DATE_TAG] setTextColor:COLOR_SLINGGIT_BLUE];
            [(UILabel *)[cell.contentView viewWithTag:CELL_TITLE_TAG] setTextColor:[UIColor darkTextColor]];
            [(UILabel *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setTextColor:[UIColor darkGrayColor]];
        }
        
        [(UILabel *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setText:[postDictionary objectForKey:kJSONBodyKeyName]];
    }else{
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
        if(indexPath.row == [sourceData count] && shouldShowMoreButton && !pullRefreshView.isLoading){
            [pullRefreshView beginLoading];
            [self showMoreButtonPressed];
            if(kLoggingEnabled)NSLog(@"shore more...");
        }
    }
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {	
	
    if(![messagesTableView isEditing] || indexPath.row == [sourceData count]){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if(indexPath.row == [sourceData count]){
            [self showMoreButtonPressed];
        }else{
            if(kLoggingEnabled)NSLog(@"message selected status: %@", [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONRecipientStatusKeyName]);
            if([[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONRecipientStatusKeyName] isEqual:kJSONStatusUnread]){
                [Utilities markMessageRead:[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName]];
            }
            
            ReplyToMessageViewController *replyToMessageViewController = [[ReplyToMessageViewController alloc] init];
            [replyToMessageViewController setMessageDictionary:[sourceData objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:replyToMessageViewController animated:YES];
        }
        
    }else if([messagesTableView isEditing]){
        [[[self.navigationController.toolbar items] objectAtIndex:0] setTitle:[NSString stringWithFormat:@"Delete(%i)", [[messagesTableView indexPathsForSelectedRows] count]]];
        
        if([[messagesTableView indexPathsForSelectedRows] count] > 0){
            [[[self.navigationController.toolbar items] objectAtIndex:0] setEnabled:YES];
        }else{
            [[[self.navigationController.toolbar items] objectAtIndex:0] setEnabled:NO];
        }
    }
    

}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([messagesTableView isEditing]){
        [[[self.navigationController.toolbar items] objectAtIndex:0] setTitle:[NSString stringWithFormat:@"Delete(%i)", [[messagesTableView indexPathsForSelectedRows] count]]];
        
        if([[messagesTableView indexPathsForSelectedRows] count] > 0){
            [[[self.navigationController.toolbar items] objectAtIndex:0] setEnabled:YES];
        }else{
            [[[self.navigationController.toolbar items] objectAtIndex:0] setEnabled:NO];
        }
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row < [sourceData count]){
        UITableViewCell *cell = [messagesTableView cellForRowAtIndexPath:indexPath];
        if([messagesTableView isEditing]){
            [(UILabel *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setFrame:CGRectMake(10, 30, 276, 40)];
            
        }else{
            [(UILabel *)[cell.contentView viewWithTag:CELL_SUBTITLE_TAG] setFrame:CGRectMake(10, 30, 300, 40)];
        }
        return YES;
    }
    return NO;
}


-(UITableViewCell *)getCellContentView:(NSString *)CellIdentifier{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    if([CellIdentifier isEqualToString:@"NormalCell"]){
        
        UILabel *email = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 200, 18)];
        [email setBackgroundColor:[UIColor clearColor]];
        [email setTextAlignment:UITextAlignmentLeft];
        [email setTag:CELL_EMAIL_TAG];
        [email setTextColor:COLOR_SLINGGIT_BLUE];
        [email setShadowColor:[UIColor whiteColor]];
        [email setShadowOffset:CGSizeMake(0.0, 1.0)];
        [email setFont:[UIFont systemFontOfSize:14]];
        [cell.contentView addSubview:email];
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(170, 0, 112, 18)];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setTextAlignment:UITextAlignmentRight];
        [date setTag:CELL_DATE_TAG];
        [date setTextColor:COLOR_SLINGGIT_BLUE];
        [date setShadowColor:[UIColor whiteColor]];
        date.shadowOffset = CGSizeMake(0.0, 1.0);
        [date setFont:[UIFont systemFontOfSize:kDateFontSize]];
        [cell.contentView addSubview:date];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(8, 17, 300, 20)];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTag:CELL_TITLE_TAG];
        [title setTextColor:[UIColor darkTextColor]];
        [title setShadowColor:[UIColor whiteColor]];
        [title setShadowOffset:CGSizeMake(0.0, 1.0)];
        [title setFont:[UIFont boldSystemFontOfSize:16]];
        [cell.contentView addSubview:title];
        
        UILabel *subtitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 30, 310, 40)];
        [subtitle setBackgroundColor:[UIColor clearColor]];
        [subtitle setNumberOfLines:3];
        [subtitle setTag:CELL_SUBTITLE_TAG];
        [subtitle setFont:[UIFont systemFontOfSize:13]];
        [subtitle setTextColor:[UIColor darkGrayColor]];
        [cell.contentView addSubview:subtitle];
        
    }else{
        
        UIActivityIndicatorView *loadingActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [loadingActivityIndicator setFrame:CGRectMake(100, 0, 50, kShowMoreCellHeight)];
        [loadingActivityIndicator setTag:TAG_LOADING_ACTIVITY_INDICATOR];
        [loadingActivityIndicator startAnimating];
        [cell.contentView addSubview:loadingActivityIndicator];
        
        UILabel *showMoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kShowMoreCellHeight)];
        [showMoreLabel setTag:TAG_LOADING_LABEL];
        [showMoreLabel setTextAlignment:UITextAlignmentCenter];
        [showMoreLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
        [showMoreLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
        showMoreLabel.shadowOffset = CGSizeMake(0.0, 1.0);
        [showMoreLabel setTextColor:COLOR_SLINGGIT_BLUE];
        [showMoreLabel setText:@"Loading"];
        [showMoreLabel setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:showMoreLabel];
    }
    
    return cell;
}

#pragma mark - ViewMessageDelegate methods
-(void)deleteMessageWithId:(NSString *)messageId{
    
    [self deleteMessagesWithIds:messageId shouldRefreshView:YES];
}

@end
