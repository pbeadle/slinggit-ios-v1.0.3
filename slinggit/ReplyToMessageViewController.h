//
//  ReplyToMessageViewController.h
//  slinggit
//
//  Created by Phil Beadle on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReplyToMessageViewController : UIViewController<UITextViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate>{
    
}

@property (nonatomic, strong) UITextView *messageTextField;
@property (nonatomic, strong) NSDictionary *messageDictionary;

@end