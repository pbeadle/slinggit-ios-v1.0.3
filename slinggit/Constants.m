//
//  Constants.h
//  slinggit
//
//  Created by Phil Beadle on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Constants.h"

BOOL const kLoggingEnabled = YES;

NSString *const kMobileAuthTokenKeyName = @"mobile_auth_token";
NSString *const kAccessTokenKeyName = @"slinggit_access_token";
NSString *const kAccessToken = @"b5d777f8d1f379f4976bc3945e231d34b406d624";
NSString *const versionNumber = @"1.0.4";

int const kScreenWidth = 320;
int const kScreenHeightNoStatusBar = 460;
int const kTabBarHeight = 49;
int const kNavBarHeight = 44;

//JSON STRINGS
NSString *const kJSONStatus = @"status";
NSString *const kJSONStatusSuccess = @"success";
NSString *const kJSONStatusError = @"error";
NSString *const kJSONResult = @"result";
NSString *const kJSONFriendlyError = @"friendly_error";
NSString *const kJSONLimitationType = @"limitation_type";
NSString *const kJSONLimitationPass = @"pass";
NSString *const kJSONOffsetKeyName = @"offset";
NSString *const kJSONStartingPostId = @"starting_post_id";
NSString *const kJSONLimitKeyName = @"limit";
NSString *const kJSONUsernameKeyName = @"user_name";
NSString *const kJSONUserIdKeyName = @"user_id";
NSString *const kJSONSearchTermKeyName = @"search_term";
NSString *const kJSONHashTagPrefixKeyName = @"hashtag_prefix";
NSString *const kJSONPriceKeyName = @"price";
NSString *const kJSONPostId = @"post_id";
NSString *const kJSONPostContentKeyName = @"content";
NSString *const kJSONTwitterAccessTokenKeyName = @"access_token";
NSString *const kJSONTwitterAccessTokenSecretKeyName = @"access_token_secret";
NSString *const kJSONRowsFoundKeyName = @"rows_found";
NSString *const kJSONApiAccountsKeyName = @"api_accounts";
NSString *const kJSONApiRealNameKeyName = @"real_name";
NSString *const kJSONApiAccountIdKeyName = @"api_account_id";
NSString *const kJSONIdKeyName = @"id";
NSString *const kJSONCommentsKeyName = @"comments";
NSString *const kJSONBodyKeyName = @"body";
NSString *const kJSONCommentBodyKeyName = @"comment_body";
NSString *const kJSONCommentIdKeyName = @"comment_id";
NSString *const kJSONLocationKeyName = @"location";
NSString *const kJSONMessageId = @"message_id";
NSString *const kJSONStartingMessageId = @"starting_message_id";
NSString *const kJSONRecipientUserIdKeyName = @"recipient_user_id";
NSString *const kJSONEmailOrUsernameKeyName = @"email_or_username";
NSString *const kJSONNewEmailKeyName = @"new_email";
NSString *const kJSONOldPasswordKeyName = @"old_password";
NSString *const kJSONNewPasswordKeyName = @"new_password";
NSString *const kJSONEmailKeyName = @"email";
NSString *const kJSONMessageIdsKeyName = @"message_ids";
NSString *const kJSONSourceIdKeyName = @"source_id";
NSString *const kJSONImageUriKeyName = @"image_uri";
NSString *const kJSONImageStyleKeyName = @"image_style";
NSString *const kJSONImageStyle300x300 = @"noPhoto_300x300";
NSString *const kJSONSourceObjectDataKeyName = @"source_object_data";
NSString *const kJSONParentMessageIdKeyName = @"parent_message_id";
NSString *const kJSONSenderUserIdKeyName = @"sender_user_id";
NSString *const kJSONContactInfoKeyName = @"contact_info_json";
NSString *const kJSONSourceKeyName = @"source";
NSString *const kJSONPostKeyName = @"post";
NSString *const kJSONImageStyleMedium = @"medium";
NSString *const kJSONApiAccountIdsKeyName = @"api_account_ids";
NSString *const kJSONContentKeyName = @"content";
NSString *const kJSONApiIdKeyName = @"api_id";
NSString *const kJSONMesagesNumberUnread = @"number_unread";
NSString *const kJSONApiSourceKeyName = @"api_source";
NSString *const kJSONTwitter = @"twitter";
NSString *const kJSONFacebook = @"facebook";
NSString *const kJSONStartingCommentId = @"starting_comment_id";
NSString *const kJSONImageUrlKeyName = @"image_url";
NSString *const kJSONRecipientStatusKeyName = @"recipient_status";

//JSON STATUS CODES
NSString *const kJSONStatusUnread = @"UNR";
NSString *const kJSONStatusRead = @"RED";
NSString *const kJSONStatusDelete = @"DEL";

//ERROR TEXT
NSString *const kGeneralProblemErrorTitle = @"Problem";
NSString *const kConnectionErrorTitle = @"Could Not Connect";
NSString *const kConnectionErrorText = @"Oops, we couldn't connect to Slinggit. Please try again later.";
NSString *const kMissingEmailError = @"You must enter an email address";
NSString *const kMissingPasswordError = @"You must enter a password";
NSString *const kPostErrorTitle = @"Could Not Post";
NSString *const kLoginErrorTitle = @"Could Not Login";
NSString *const kChangeErrorTitle = @"Could Not Change";
NSString *const kSignUpErrorTitle = @"Could Not Sign Up";
NSString *const kResetPasswordErrorTitle = @"Could Not Reset";
NSString *const kResetPasswordError = @"Oops, we couldn't reset your password. Please try the website.";
NSString *const kInvalidUsernameErrorTitle = @"Invalid Username or Email";
NSString *const kInvalidUsernameError = @"Oops, you must enter a username or email to reset.";
NSString *const kAddAccountErrorTitle = @"Could Not Add Account";
NSString *const kDeleteAccountErrorTitle = @"Could Not Delete Account";
NSString *const kSendErrorTitle = @"Could Not Send";
NSString *const kInvalidPasswordErrorTitle = @"Invalid Password";
NSString *const kInvalidPasswordError = @"Oops, your passwords must be 6 characters or more in length.";
NSString *const kPasswordsDoNotMatchError = @"Oops, your passwords do not match.";
NSString *const kPasswordsMatchError = @"Oops, your new password cannot be the same as the old one.";
NSString *const kInvalidEmailErrorTitle = @"Invalid email";
NSString *const kInvalidEmailError = @"Oops, you must enter a valid email address.";
NSString *const kNoCameraErrorTitle = @"No Camera";
NSString *const kNoCameraError = @"Oops, we couldn't find your camera.";
NSString *const kDeleteErrorTitle = @"Could Not Delete";
NSString *const kCloseErrorTitle = @"Could Not Close";
NSString *const kReportAbuseErrorTitle = @"Could Not Report";
NSString *const kAddCommentErrorTitle = @"Could Not Comment";
NSString *const kAddWatchPostErrorTitle = @"Could Not Watch";
NSString *const kRemoveWatchPostErrorTitle = @"Could Not Remove";

//LIMITS
int kItemNameFieldLimit = 15;
int kPriceFieldLimit = 6;
int kLocationFieldLimit = 16;
int kDescriptionFieldLimit = 300;
int kCommentFieldLimit = 300;
int kSendMessageFieldLimit = 1000;
int kFeedShowLimit = 20;
int kMessageShowLimit = 20;
int kCommentShowLimit = 20;
int kPhotoDownloadStorageNumberLimit = 100;
int kUrlRequestTimeout = 10;

//FONT SIZES
int kDateFontSize = 14;

//LOCALHOST URLS
/*
NSString *const kSignInURL                      = @"http://localhost:3000/mobile/user_login";
NSString *const kSignInStatusURL                = @"http://localhost:3000/mobile/user_login_status";
NSString *const kResetPasswordURL               = @"http://localhost:3000/mobile/password_reset";
NSString *const kSignUpURL                      = @"http://localhost:3000/mobile/user_signup";
NSString *const kSignOutURL                     = @"http://localhost:3000/mobile/user_logout";
NSString *const kGetSlinggitPostDataURL         = @"http://localhost:3000/mobile/get_slinggit_post_data";
NSString *const kGetWatchedPostsURL             = @"http://localhost:3000/mobile/get_watchedposts";
NSString *const kCheckPostLimitationsURL        = @"http://localhost:3000/mobile/check_limitations";
NSString *const kCreatePostURL                  = @"http://localhost:3000/mobile/create_post";
NSString *const kAddTwitterAccountURL           = @"http://localhost:3000/mobile/add_twitter_account";
NSString *const kAddFacebookAccountURL          = @"http://localhost:3000/mobile/add_facebook_account";
NSString *const kGetUserApiAccountsURL          = @"http://localhost:3000/mobile/get_user_api_accounts";
NSString *const kDeleteApiAccountURL            = @"http://localhost:3000/mobile/delete_twitter_account";
NSString *const kGetSingleSlinggitPostDataURL   = @"http://localhost:3000/mobile/get_single_slinggit_post_data";
NSString *const kCreatePostCommentURL           = @"http://localhost:3000/mobile/create_post_comment";
NSString *const kDeletePostCommentURL           = @"http://localhost:3000/mobile/delete_post_comment";
NSString *const kGetMessagesURL                 = @"http://localhost:3000/mobile/get_messages";
NSString *const kSendMessageURL                 = @"http://localhost:3000/mobile/send_message";
NSString *const kDeleteMessageURL               = @"http://localhost:3000/mobile/delete_message";
NSString *const kChangePasswordURL              = @"http://localhost:3000/mobile/change_password";
NSString *const kChangeEmailURL                 = @"http://localhost:3000/mobile/change_email";
NSString *const kMarkMessageReadURL             = @"http://localhost:3000/mobile/mark_message_read";
NSString *const kGetPostImageURL                = @"http://localhost:3000/mobile/get_post_image";
NSString *const kReplyToMessageURL              = @"http://localhost:3000/mobile/reply_to_message";
NSString *const kReportAbuseURL                 = @"http://localhost:3000/mobile/report_abuse";
NSString *const kClosePostURL                   = @"http://localhost:3000/mobile/close_post";
NSString *const kDeletePostURL                  = @"http://localhost:3000/mobile/delete_post";
NSString *const kGetPostCommentsURL             = @"http://localhost:3000/mobile/get_post_comments";
NSString *const kTermsOfServiceURL              = @"http://localhost:3000/terms_of_service";
NSString *const kAddWatchedPostURL              = @"http://localhost:3000/mobile/add_post_to_watch_list";
NSString *const kRemoveWatchedPostURL           = @"http://localhost:3000/mobile/remove_post_from_watch_list";
*/
//STAGE URLS
/**/
NSString *const kSignInURL                      = @"https://integ.slinggit.com/mobile/user_login";
NSString *const kSignInStatusURL                = @"https://integ.slinggit.com/mobile/user_login_status";
NSString *const kResetPasswordURL               = @"https://integ.slinggit.com/mobile/password_reset";
NSString *const kSignUpURL                      = @"https://integ.slinggit.com/mobile/user_signup";
NSString *const kSignOutURL                     = @"https://integ.slinggit.com/mobile/user_logout";
NSString *const kGetSlinggitPostDataURL         = @"https://integ.slinggit.com/mobile/get_slinggit_post_data";
NSString *const kGetWatchedPostsURL             = @"https://integ.slinggit.com/mobile/get_watchedposts";
NSString *const kCheckPostLimitationsURL        = @"https://integ.slinggit.com/mobile/check_limitations";
NSString *const kCreatePostURL                  = @"https://integ.slinggit.com/mobile/create_post";
NSString *const kAddTwitterAccountURL           = @"https://integ.slinggit.com/mobile/add_twitter_account";
NSString *const kAddFacebookAccountURL          = @"https://integ.slinggit.com/mobile/add_facebook_account";
NSString *const kGetUserApiAccountsURL          = @"https://integ.slinggit.com/mobile/get_user_api_accounts";
NSString *const kDeleteApiAccountURL            = @"https://integ.slinggit.com/mobile/delete_twitter_account";
NSString *const kGetSingleSlinggitPostDataURL   = @"https://integ.slinggit.com/mobile/get_single_slinggit_post_data";
NSString *const kCreatePostCommentURL           = @"https://integ.slinggit.com/mobile/create_post_comment";
NSString *const kDeletePostCommentURL           = @"https://integ.slinggit.com/mobile/delete_post_comment";
NSString *const kGetMessagesURL                 = @"https://integ.slinggit.com/mobile/get_messages";
NSString *const kSendMessageURL                 = @"https://integ.slinggit.com/mobile/send_message";
NSString *const kDeleteMessageURL               = @"https://integ.slinggit.com/mobile/delete_message";
NSString *const kChangePasswordURL              = @"https://integ.slinggit.com/mobile/change_password";
NSString *const kChangeEmailURL                 = @"https://integ.slinggit.com/mobile/change_email";
NSString *const kMarkMessageReadURL             = @"https://integ.slinggit.com/mobile/mark_message_read";
NSString *const kGetPostImageURL                = @"https://integ.slinggit.com/mobile/get_post_image";
NSString *const kReplyToMessageURL              = @"https://integ.slinggit.com/mobile/reply_to_message";
NSString *const kReportAbuseURL                 = @"https://integ.slinggit.com/mobile/report_abuse";
NSString *const kClosePostURL                   = @"https://integ.slinggit.com/mobile/close_post";
NSString *const kDeletePostURL                  = @"https://integ.slinggit.com/mobile/delete_post";
NSString *const kGetPostCommentsURL             = @"https://integ.slinggit.com/mobile/get_post_comments";
NSString *const kTermsOfServiceURL              = @"https://integ.slinggit.com/terms_of_service";
NSString *const kAddWatchedPostURL              = @"https://integ.slinggit.com/mobile/add_post_to_watch_list";
NSString *const kRemoveWatchedPostURL           = @"https://integ.slinggit.com/mobile/remove_post_from_watch_list";

//PRODUCTION URLS
/*
NSString *const kSignInURL                      = @"https://slinggit.com/mobile/user_login";
NSString *const kSignInStatusURL                = @"https://slinggit.com/mobile/user_login_status";
NSString *const kResetPasswordURL               = @"https://slinggit.com/mobile/password_reset";
NSString *const kSignUpURL                      = @"https://slinggit.com/mobile/user_signup";
NSString *const kSignOutURL                     = @"https://slinggit.com/mobile/user_logout";
NSString *const kGetSlinggitPostDataURL         = @"https://slinggit.com/mobile/get_slinggit_post_data";
NSString *const kGetWatchedPostsURL             = @"https://slinggit.com/mobile/get_watchedposts";
NSString *const kCheckPostLimitationsURL        = @"https://slinggit.com/mobile/check_limitations";
NSString *const kCreatePostURL                  = @"https://slinggit.com/mobile/create_post";
NSString *const kAddTwitterAccountURL           = @"https://slinggit.com/mobile/add_twitter_account";
NSString *const kAddFacebookAccountURL          = @"https://slinggit.com/mobile/add_facebook_account";
NSString *const kGetUserApiAccountsURL          = @"https://slinggit.com/mobile/get_user_api_accounts";
NSString *const kDeleteApiAccountURL            = @"https://slinggit.com/mobile/delete_twitter_account";
NSString *const kGetSingleSlinggitPostDataURL   = @"https://slinggit.com/mobile/get_single_slinggit_post_data";
NSString *const kCreatePostCommentURL           = @"https://slinggit.com/mobile/create_post_comment";
NSString *const kDeletePostCommentURL           = @"https://slinggit.com/mobile/delete_post_comment";
NSString *const kGetMessagesURL                 = @"https://slinggit.com/mobile/get_messages";
NSString *const kSendMessageURL                 = @"https://slinggit.com/mobile/send_message";
NSString *const kDeleteMessageURL               = @"https://slinggit.com/mobile/delete_message";
NSString *const kChangePasswordURL              = @"https://slinggit.com/mobile/change_password";
NSString *const kChangeEmailURL                 = @"https://slinggit.com/mobile/change_email";
NSString *const kMarkMessageReadURL             = @"https://slinggit.com/mobile/mark_message_read";
NSString *const kGetPostImageURL                = @"https://slinggit.com/mobile/get_post_image";
NSString *const kReplyToMessageURL              = @"https://slinggit.com/mobile/reply_to_message";
NSString *const kReportAbuseURL                 = @"https://slinggit.com/mobile/report_abuse";
NSString *const kClosePostURL                   = @"https://slinggit.com/mobile/close_post";
NSString *const kDeletePostURL                  = @"https://slinggit.com/mobile/delete_post";
NSString *const kGetPostCommentsURL             = @"https://slinggit.com/mobile/get_post_comments";
NSString *const kTermsOfServiceURL              = @"https://slinggit.com/terms_of_service";
NSString *const kAddWatchedPostURL              = @"https://slinggit.com/mobile/add_post_to_watch_list";
NSString *const kRemoveWatchedPostURL           = @"https://slinggit.com/mobile/remove_post_from_watch_list";
*/
