//
//  FeedViewController.h
//  slinggit
//
//  Created by Phil Beadle on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"

@interface FeedViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, PullToRefreshViewDelegate> {
    
    
    
}

@property (nonatomic, strong) UITableView *feedTableView;
@property (nonatomic, strong) UISegmentedControl *feedSegmentedControl;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, assign) BOOL shouldLoadDataOnViewWillAppear;
@property (nonatomic, assign) BOOL searchByUsername;


@end
