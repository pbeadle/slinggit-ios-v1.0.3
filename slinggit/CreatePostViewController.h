#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CreatePostViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate> {
    
    
    
}

@property (nonatomic, strong) UITextField *itemNameTextField;
@property (nonatomic, strong) UITextView *descriptionTextField;
@property (nonatomic, strong) UITextField *priceTextField;
@property (nonatomic, strong) UITextField *locationTextField;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) UIButton *photoButton;

@end