//
//  MessagesViewController.h
//  slinggit
//
//  Created by Phil Beadle on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"

@interface MessagesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, PullToRefreshViewDelegate>

@property (nonatomic, assign) BOOL shouldLoadDataOnViewWillAppear;

-(void)loadTableDataFromNewestPost;

@end
