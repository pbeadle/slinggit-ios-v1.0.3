#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "CreatePostViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "AFJSONRequestOperation.h"
#import "AFJSONUtilities.h"
#import "AppDelegate.h"
#import "Utilities.h"
#import "KeychainWrapper.h"
#import "SVProgressHUD.h"

#define TAG_RESET_PASSWORD_ALERTVIEW 10
#define TAG_INVALID_USERNAME_ALERTVIEW 11

@interface LoginViewController (){
    
    NSString *state;
    NSString *encodedDeviceName;
    
    UIView *loginFieldsView;
    UIView *buttonsView;
}
@end

@implementation LoginViewController

@synthesize emailTextField = _emailTextField;
@synthesize passwordTextField = _passwordTextField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [super loadView];
    
    UIView *view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default.png"]];
    [backgroundImageView setFrame:CGRectMake(0, -20, kScreenWidth, 480)];
    [backgroundImageView setContentMode:UIViewContentModeScaleAspectFill];
    [view addSubview:backgroundImageView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    [singleTap setDelegate:self];
    [singleTap setCancelsTouchesInView:NO];
    [view addGestureRecognizer:singleTap];
    
    loginFieldsView = [[UIView alloc] initWithFrame:CGRectMake(0, 170, kScreenWidth, 160)];
    [loginFieldsView setHidden:YES];
    
    UIImageView *loginTextBackground = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"text_input_background.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"text_input_background.png"]]]];
    [loginTextBackground setFrame:CGRectMake(10, 0, 300, 100)];
    [loginFieldsView addSubview:loginTextBackground];
    
    UIImageView *loginTextLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"text_input_line.png"]];
    [loginTextLine setFrame:CGRectMake(12, 49, 296, 2)];
    [loginFieldsView addSubview:loginTextLine];
    
    self.emailTextField = [[MyTextField alloc] initWithFrame:CGRectMake(20, 0, 280, 50)];
    //[self.emailTextField setText:@"p4@4.com"];
    [self.emailTextField setTag:0];
    [self.emailTextField setDelegate:self];
    [self.emailTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.emailTextField setReturnKeyType:UIReturnKeyNext];
    [self.emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.emailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.emailTextField setPlaceholder:@"email"];
    [loginFieldsView addSubview:self.emailTextField];
    
    self.passwordTextField = [[MyTextField alloc] initWithFrame:CGRectMake(20, 50, 280, 50)];
    //[self.passwordTextField setText:@"p4@4.com"];
    [self.passwordTextField setTag:1];
    [self.passwordTextField setDelegate:self];
    [self.passwordTextField setReturnKeyType:UIReturnKeyGo];
    [self.passwordTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.passwordTextField setSecureTextEntry:YES];
    [self.passwordTextField setReturnKeyType:UIReturnKeyGo];
    [self.passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.passwordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.passwordTextField setPlaceholder:@"password"];
    [loginFieldsView addSubview:self.passwordTextField];
    
    [view addSubview:loginFieldsView];
    
    buttonsView = [[UIView alloc] initWithFrame:CGRectMake(0, 170, kScreenWidth, 120)];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginButton setBackgroundImage:[[UIImage imageNamed:@"green_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"green_button.png"]]] forState:UIControlStateNormal];
    [loginButton setFrame:CGRectMake(20, 0, 280, 51)];
    [loginButton setTitle:@"Sign In" forState:UIControlStateNormal];
    [loginButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [loginButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    loginButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [loginButton addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsView addSubview:loginButton];
    
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [signUpButton setBackgroundImage:[[UIImage imageNamed:@"black_button.png"] resizableImageWithCapInsets:[Utilities getEdgeInsetsForImage:[UIImage imageNamed:@"black_button.png"]]] forState:UIControlStateNormal];
    [signUpButton setTitle:@"Create Account" forState:UIControlStateNormal];
    [signUpButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [signUpButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    signUpButton.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    [signUpButton setFrame:CGRectMake(20, 62, 280, 47)];
    [signUpButton addTarget:self action:@selector(signUpButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsView addSubview:signUpButton];
    
    [view addSubview:buttonsView];
    
    UIButton *resetPasswordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [resetPasswordButton setBackgroundColor:[UIColor clearColor]];
    [resetPasswordButton setFrame:CGRectMake((kScreenWidth-166)/2, 402, 166, 50)];
    [resetPasswordButton setTitle:@"Reset Password" forState:UIControlStateNormal];
    [resetPasswordButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
    [resetPasswordButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [resetPasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resetPasswordButton setTitleShadowColor:[UIColor colorWithWhite:0.0 alpha:0.4] forState:UIControlStateNormal];
    [resetPasswordButton.titleLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [resetPasswordButton setReversesTitleShadowWhenHighlighted:YES];
    [resetPasswordButton addTarget:self action:@selector(resetPasswordButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:resetPasswordButton];
    
    self.view = view;

}

- (void) viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:animated];
    
    if([KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_TOKEN_KEY] != nil){
        [self userSignInStatus];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
    [super viewWillDisappear:animated];
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    state = [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_UNIQUE_ID];
    encodedDeviceName = [Utilities stringByEncodingHTMLEntities:[Utilities platformString]];
    [self setTitle:@"Sign In"];
    [super viewDidLoad];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - My Methods
-(void)clearFirstResponders{
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

-(void)validateSignInFields{
    
    NSString *errorMessage;
    if([self.emailTextField.text length] == 0){
        errorMessage = kMissingEmailError;
    }
    
    if([self.passwordTextField.text length] == 0){
        errorMessage = kMissingPasswordError;
    }
    
    if(errorMessage){
        UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kLoginErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [loginErrorAlertView show];
        return;
    }
    
    [self clearFirstResponders];
    [self signInUser];
    
    
}

-(void)loginButtonPressed:(UIButton *)button{
    [self clearFirstResponders];
    if([loginFieldsView isHidden]){
        [self animateFields];
    }else{
        [self validateSignInFields];
    }
}

-(void)signUpButtonPressed:(UIButton *)button{
    [self clearFirstResponders];
    SignUpViewController *signUpViewController = [[SignUpViewController alloc] init];
    [self.navigationController pushViewController:signUpViewController animated:YES];
}

-(void)resetPasswordButtonPressed:(UIButton *)button{
    [self clearFirstResponders];
    [self showResetPasswordAlertView];
    
}

-(void)showResetPasswordAlertView{
    UIAlertView *resetPasswordAlertView = [[UIAlertView alloc] initWithTitle:@"Reset Password" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send", nil];
    [resetPasswordAlertView setTag:TAG_RESET_PASSWORD_ALERTVIEW];
    [resetPasswordAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[resetPasswordAlertView textFieldAtIndex:0] setPlaceholder:@"Username or Email"];
    [resetPasswordAlertView show];
}

-(void)signInUser{
    
    [SVProgressHUD showWithStatus:@"Signing In"];
    
    NSString *encodedUser = [Utilities stringByEncodingHTMLEntities:self.emailTextField.text];
    NSString *encodedPassword = [Utilities stringByEncodingHTMLEntities:self.passwordTextField.text];

    NSURL *editedLoginUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?state=%@&device_name=%@&%@=%@&email=%@&password=%@", kSignInURL, state, encodedDeviceName, kAccessTokenKeyName, kAccessToken, encodedUser, encodedPassword]];
    
    if(kLoggingEnabled)NSLog(@"editedURL: %@", editedLoginUrl);
    NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:editedLoginUrl];
    [loginRequest setTimeoutInterval:kUrlRequestTimeout];
    [loginRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *loginOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:loginRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"login response: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *usernameString = [resultDictionary objectForKey:kJSONUsernameKeyName];
            if(kLoggingEnabled)NSLog(@"stored username: %@", usernameString);
            [KeychainWrapper createKeychainValue:usernameString forIdentifier:PREFS_USERNAME];
            [KeychainWrapper createKeychainValue:[[resultDictionary valueForKey:kJSONUserIdKeyName] stringValue] forIdentifier:PREFS_USER_ID];
            NSString *mobileAuthToken = [[NSString alloc] initWithString:[resultDictionary valueForKeyPath:kMobileAuthTokenKeyName]];
            [KeychainWrapper createKeychainValue:mobileAuthToken forIdentifier:PREFS_TOKEN_KEY];
            
            AppDelegate *myAppDelegate = [[UIApplication sharedApplication] delegate];
            [myAppDelegate getMessageData];
            
            [Utilities forceLoadMessageDataOnViewWillAppear];
            [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:NO];
            
            [self dismissModalViewControllerAnimated:YES];
        }else if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage = [resultDictionary valueForKeyPath:kJSONFriendlyError];

            UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kLoginErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginErrorAlertView show];
        }else{
            UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kLoginErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginErrorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"login failure error: JSON: \n%@", JSON);
        
        UIAlertView *loginErrorAlertView = [[UIAlertView alloc] initWithTitle:kLoginErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [loginErrorAlertView show];
    }];
    
    [loginOperation start];
    
    /*************TEST****************
    [self dismissModalViewControllerAnimated:YES];
     */
}

-(void)userSignInStatus{
    
    NSURL *editedLoginUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kSignInStatusURL, [Utilities commonParams]]];
    if(kLoggingEnabled)NSLog(@"edited loging status URL: %@", editedLoginUrl);
    NSMutableURLRequest *signInStatusRequest = [NSMutableURLRequest requestWithURL:editedLoginUrl];
    [signInStatusRequest setTimeoutInterval:kUrlRequestTimeout];
    [signInStatusRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *signInOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:signInStatusRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if(kLoggingEnabled)NSLog(@"user sign in status response: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *usernameString = [resultDictionary objectForKey:kJSONUsernameKeyName];
            if(kLoggingEnabled)NSLog(@"stored username: %@", usernameString);
            [KeychainWrapper createKeychainValue:usernameString forIdentifier:PREFS_USERNAME];
            if(![[resultDictionary valueForKey:kJSONUserIdKeyName] isEqual:[NSNull null]]){
                [KeychainWrapper createKeychainValue:[[resultDictionary valueForKey:kJSONUserIdKeyName] stringValue] forIdentifier:PREFS_USER_ID];
            }
            if([resultDictionary objectForKey:@"logged_in"] && [[resultDictionary objectForKey:@"logged_in"] intValue] == 1){
                [Utilities forceLoadMessageDataOnViewWillAppear];
                [Utilities forceLoadPostDataOnViewWillAppearAndShowMyPosts:NO];
                [self dismissModalViewControllerAnimated:YES];
            }
            
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(kLoggingEnabled)NSLog(@"user sign in status failure error: JSON: \n%@", JSON);
        
    }];
    
    [signInOperation start];
    
    /*************TEST****************
    [self dismissModalViewControllerAnimated:YES];
     */
}

-(void)resetPasswordWithUsername:(NSString *)username{
    [SVProgressHUD showWithStatus:@"Sending"];
    NSURL *editedResetPasswordUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kResetPasswordURL, [Utilities commonParams], kJSONEmailOrUsernameKeyName, username]];
    if(kLoggingEnabled)NSLog(@"edited reset password URL: %@", editedResetPasswordUrl);
    NSMutableURLRequest *resetPasswordRequest = [NSMutableURLRequest requestWithURL:editedResetPasswordUrl];
    [resetPasswordRequest setTimeoutInterval:kUrlRequestTimeout];
    [resetPasswordRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *resetPasswordOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:resetPasswordRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"reset password response: %@", JSON);
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            //NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            UIAlertView *resetAlertView = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"An email was sent to your address with reset instructions." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [resetAlertView show];
            
        }else{
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kResetPasswordErrorTitle message:kResetPasswordError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        if(kLoggingEnabled)NSLog(@"reset password failure error: \n%@", error);
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kResetPasswordErrorTitle message:kResetPasswordError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
        
    }];
    
    [resetPasswordOperation start];
}

-(void)animateFields{
    [UIView animateWithDuration:0.1 animations:^{
        [loginFieldsView setHidden:NO];
        [loginFieldsView setFrame:CGRectMake(0, 120, kScreenWidth, 160)];
        [buttonsView setFrame:CGRectMake(0, 236, kScreenWidth, 120)];
    }];
}

#pragma mark - Gesture Recognizer Methods

-(void)backgroundTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    [self clearFirstResponders];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isKindOfClass:[UIControl class]]){
        return NO;
    }
    return YES;
}

#pragma mark - UITextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag == 0){
        [self clearFirstResponders];
        [self.passwordTextField becomeFirstResponder]; 
    }else{
        [self validateSignInFields];
    }
    return YES;
}

#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == TAG_RESET_PASSWORD_ALERTVIEW){
        if(buttonIndex == 1){
            if([alertView textFieldAtIndex:0].text != nil && [[alertView textFieldAtIndex:0].text length] > 0){
                [self resetPasswordWithUsername:[alertView textFieldAtIndex:0].text];
            }else{
                UIAlertView *resetPasswordErrorAlertView = [[UIAlertView alloc] initWithTitle:kInvalidUsernameErrorTitle message:kInvalidUsernameError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [resetPasswordErrorAlertView setTag:TAG_INVALID_USERNAME_ALERTVIEW];
                [resetPasswordErrorAlertView show];
            }
        }
    }else if(alertView.tag == TAG_INVALID_USERNAME_ALERTVIEW){
        [self showResetPasswordAlertView];
    }
}

-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    if(alertView.tag == TAG_RESET_PASSWORD_ALERTVIEW){
        if([alertView textFieldAtIndex:0].text != nil && [[alertView textFieldAtIndex:0].text length] > 0){
            return YES;
        }else{
            return NO;
        }
    }
    
    return YES;
}



@end
