//
//  Photo.h
//  slinggit
//
//  Created by Phil Beadle on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Photo : NSManagedObject

@property (nonatomic, retain) NSNumber *post_id;
@property (nonatomic, retain) NSString *base64;
@property (nonatomic, retain) NSDate *creation_date;

@end
