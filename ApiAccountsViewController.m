//
//  ApiAccountsViewController.m
//  slinggit
//
//  Created by Phil Beadle on 5/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ApiAccountsViewController.h"
#import "WebViewController.h"
#import "Constants.h"
#import "KeychainWrapper.h"
#import "AFJSONRequestOperation.h"
#import "Utilities.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>

#define CELL_IMAGE_TAG 10
#define CELL_ACCOUNT_NAME_TAG 11
#define CELL_API_SOURCE_IMAGE_TAG 12

#define kCellHeight 48

@interface ApiAccountsViewController (){
    
    //NSDictionary *twitterAccountParameters;
    NSMutableArray *sourceData;
    UIActivityIndicatorView *loadingIndicator;
    UILabel *addAccountsLabel;
}

@end

@implementation ApiAccountsViewController

@synthesize apiAccountsTableView = _apiAccountsTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    [super loadView];
    
    sourceData = [[NSMutableArray alloc] init];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    [view setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    self.apiAccountsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeightNoStatusBar-kNavBarHeight-kTabBarHeight) style:UITableViewStylePlain];
    [self.apiAccountsTableView setEditing:YES];
    [self.apiAccountsTableView setDelegate:self];
	[self.apiAccountsTableView setDataSource:self];
    [self.apiAccountsTableView setBackgroundColor:[UIColor clearColor]];
    [view addSubview:self.apiAccountsTableView];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    [v setBackgroundColor:[UIColor clearColor]];
    [self.apiAccountsTableView setTableFooterView:v];
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicator setCenter:CGPointMake(kScreenWidth/2, 166)];
    [view addSubview:loadingIndicator];
    [loadingIndicator startAnimating];
    
    addAccountsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 170, 320, 40)];
    [addAccountsLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [addAccountsLabel setText:@"No Networks Found"];
    [addAccountsLabel setBackgroundColor:[UIColor clearColor]];
    [addAccountsLabel setTextAlignment:UITextAlignmentCenter];
    [addAccountsLabel setTextColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
    [addAccountsLabel setShadowColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    [addAccountsLabel setShadowOffset:CGSizeMake(0.0, 1.0)];
    [addAccountsLabel setHidden:YES];
    [view addSubview:addAccountsLabel];
    
    
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIBarButtonItem *addApiAccountButton = [[UIBarButtonItem alloc] 
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addApiAccountButtonPressed)];
    
    [self.navigationItem setRightBarButtonItem: addApiAccountButton];
    [self setTitle:@"Social Networks"];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Networks" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [sourceData removeAllObjects];
    [self loadApiAccounts];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Button Presses

-(void)addApiAccountButtonPressed{
    UIActionSheet *apiAccountChoiceActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Twitter", @"Facebook", nil];
    [apiAccountChoiceActionSheet showInView:self.tabBarController.view];
}

#pragma mark - My Methods

-(void)loadAddApiAccountWebviewWithType:(NSUInteger)accountType{
    WebViewController *webView = [[WebViewController alloc] init];
    [webView setAccountType:accountType];
    [webView setDelegate:self];
    [self.navigationController pushViewController:webView animated:YES];
}

/*
 -(void)addTwitterAccount{
 
 NSString *twitterAccessToken = @"";
 NSString *twitterAccessTokenSecret = @"";
 if(twitterAccountParameters){
 twitterAccessToken = [twitterAccountParameters objectForKey:kJSONTwitterAccessTokenKeyName];
 twitterAccessTokenSecret = [twitterAccountParameters objectForKey:kJSONTwitterAccessTokenSecretKeyName];
 }
 NSURL *editedAddTwitterAccountUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@&%@=%@&%@=%@", kAddTwitterAccountURL, [Utilities commonParams], kJSONUsernameKeyName, [KeychainWrapper keychainStringFromMatchingIdentifier:PREFS_USERNAME], kJSONTwitterAccessTokenKeyName, twitterAccessToken, kJSONTwitterAccessTokenSecretKeyName, twitterAccessTokenSecret]];
 if(kLoggingEnabled)NSLog(@"edited add twitter up url: %@", editedAddTwitterAccountUrl);
 NSMutableURLRequest *addTwitterAccountRequest = [NSMutableURLRequest requestWithURL:editedAddTwitterAccountUrl];
 [addTwitterAccountRequest setHTTPMethod:@"POST"];
 
 AFJSONRequestOperation *addTwitterAccountOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:addTwitterAccountRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
 
 if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
 if(kLoggingEnabled)NSLog(@"add twitter account success response JSON: %@", JSON);
 
 [self loadApiAccounts];
 
 }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
 if(kLoggingEnabled)NSLog(@"add twitter acount failure JSON: %@", JSON);
 
 NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
 NSString *errorMessage;
 if([resultDictionary valueForKey:kJSONFriendlyError]){
 errorMessage = [NSString stringWithString:[resultDictionary valueForKey:kJSONFriendlyError]];
 }else{
 errorMessage = [NSString stringWithString:kConnectionErrorText];
 }
 
 UIAlertView *addTwitterAccountErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [addTwitterAccountErrorAlertView show];
 }else{
 
 UIAlertView *addTwitterAccountErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [addTwitterAccountErrorAlertView show];
 }
 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
 if(kLoggingEnabled)NSLog(@"add twitter account error: %@", error);
 UIAlertView *addTwitterAccountErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [addTwitterAccountErrorAlertView show];
 }];
 
 [addTwitterAccountOperation start];
 
 }
 */
-(void)loadApiAccounts{
    NSURL *editedGetApiAccountsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", kGetUserApiAccountsURL, [Utilities commonParams]]];
    if(kLoggingEnabled)NSLog(@"edited get api accounts url: %@", editedGetApiAccountsUrl);
    NSMutableURLRequest *getApiAccountsRequest = [NSMutableURLRequest requestWithURL:editedGetApiAccountsUrl];
    [getApiAccountsRequest setTimeoutInterval:kUrlRequestTimeout];
    [getApiAccountsRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *getApiAccountsOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:getApiAccountsRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [loadingIndicator stopAnimating];
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            if(kLoggingEnabled)NSLog(@"load api accounts success response JSON: %@", JSON);
            NSDictionary *resultDictionary = [JSON valueForKey:kJSONResult];
            
            if([resultDictionary objectForKey:kJSONRowsFoundKeyName] && [[resultDictionary objectForKey:kJSONRowsFoundKeyName] intValue] > 0){
                
                NSArray *apiAccountsArray = [NSMutableArray arrayWithArray:[resultDictionary objectForKey:kJSONApiAccountsKeyName]];
                
                for(NSDictionary *apiAccount in apiAccountsArray){
                    if(![[apiAccount objectForKey:kJSONUserIdKeyName] isEqual:[NSNull null]] && [[apiAccount objectForKey:kJSONUserIdKeyName] intValue] != 0){
                        if(kLoggingEnabled)NSLog(@"user_id = %@", [apiAccount objectForKey:kJSONUserIdKeyName]);
                        [sourceData addObject:apiAccount];
                    }
                }
                
                if(kLoggingEnabled)NSLog(@"source data loaded: %@", sourceData);
                
                if([sourceData count] == 0){
                    [addAccountsLabel setHidden:NO];
                }else{
                    [addAccountsLabel setHidden:YES];
                }
                
                [self.apiAccountsTableView reloadData];
                [self loadImagesForOnscreenRows];
            }else{
                [addAccountsLabel setHidden:NO];
            }
            
        }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            if(kLoggingEnabled)NSLog(@"load api accounts failure JSON: %@", JSON);
            [addAccountsLabel setHidden:NO];
            
            NSDictionary *resultDictionary = [JSON valueForKeyPath:kJSONResult];
            NSString *errorMessage;
            if([resultDictionary valueForKey:kJSONFriendlyError]){
                errorMessage = [NSString stringWithString:[resultDictionary valueForKey:kJSONFriendlyError]];
            }else{
                errorMessage = [NSString stringWithString:kConnectionErrorText];
            }
            
            UIAlertView *getApiAccountsErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [getApiAccountsErrorAlertView show];
        }else{
            
            [addAccountsLabel setHidden:NO];
            
            UIAlertView *getApiAccountsErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [getApiAccountsErrorAlertView show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [loadingIndicator stopAnimating];
        
        [addAccountsLabel setHidden:NO];
        
        if(kLoggingEnabled)NSLog(@"load api accounts error: %@", error);
        UIAlertView *getApiAccountsErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [getApiAccountsErrorAlertView show];
    }];
    
    [getApiAccountsOperation start];
}

-(void)loadImagesForOnscreenRows{
    
    if(sourceData){
        for(NSIndexPath *indexPath in [self.apiAccountsTableView indexPathsForVisibleRows]){
            if(indexPath.row < [sourceData count]){
                UITableViewCell *cell = [self.apiAccountsTableView cellForRowAtIndexPath:indexPath];
                
                if(cell.imageView.image == nil){
                    
                    if(![[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONImageUrlKeyName] isEqual:[NSNull null]]){
                        
                        NSURL *editedGetImageUrl = [NSURL URLWithString:[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONImageUrlKeyName]];
                        
                        [(UIImageView *)[cell.contentView viewWithTag:CELL_IMAGE_TAG] setImageWithURL:editedGetImageUrl];
                        
                        if([[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONApiSourceKeyName]){
                            if([[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONApiSourceKeyName] isEqual:kJSONFacebook]){
                                [(UIImageView *)[cell.contentView viewWithTag:CELL_API_SOURCE_IMAGE_TAG] setImage:[UIImage imageNamed:@"facebook_icon.png"]];
                            }else if([[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONApiSourceKeyName] isEqual:kJSONTwitter]){
                                [(UIImageView *)[cell.contentView viewWithTag:CELL_API_SOURCE_IMAGE_TAG] setImage:[UIImage imageNamed:@"twitter_icon.png"]];
                            }
                        }
                        
                        if(kLoggingEnabled)NSLog(@"get image data URL: %@", editedGetImageUrl);
                        
                    }
                    
                }
            }
        }
    }
}

#pragma mark - UITableview Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [sourceData count];
    
}

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [self getCellContentView:CellIdentifier];
        
    }
    
    if(![[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONApiRealNameKeyName] isEqual:[NSNull null]]){
        [(UILabel *)[cell.contentView viewWithTag:CELL_ACCOUNT_NAME_TAG] setText:[[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONApiRealNameKeyName]];
    }
    
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {	
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *apiAccountId = [[sourceData objectAtIndex:indexPath.row] objectForKey:kJSONIdKeyName];
    
    NSURL *editedDeleteApiAccountUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@&%@=%@", kDeleteApiAccountURL, [Utilities commonParams], kJSONApiAccountIdKeyName, apiAccountId]];
    if(kLoggingEnabled)NSLog(@"edited get api accounts url: %@", editedDeleteApiAccountUrl);
    NSMutableURLRequest *deleteApiAccountRequest = [NSMutableURLRequest requestWithURL:editedDeleteApiAccountUrl];
    [deleteApiAccountRequest setTimeoutInterval:kUrlRequestTimeout];
    [deleteApiAccountRequest setHTTPMethod:@"POST"];
    
    AFJSONRequestOperation *deleteApiAccountOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:deleteApiAccountRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        if([JSON valueForKeyPath:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusSuccess]){
            if(kLoggingEnabled)NSLog(@"delete api account success response JSON: %@", JSON);
            
            [sourceData removeObjectAtIndex:indexPath.row];
            [self.apiAccountsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
            
            if([sourceData count] == 0){
                [addAccountsLabel setHidden:NO];
            }
            
        }else if([JSON valueForKey:kJSONStatus] && [[JSON valueForKeyPath:kJSONStatus] isEqualToString:kJSONStatusError]){
            if(kLoggingEnabled)NSLog(@"load api accounts failure JSON: %@", JSON);
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kDeleteAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
            
        }else{
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kDeleteAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlertView show];
            
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(kLoggingEnabled)NSLog(@"load api accounts failure error: %@", error);
        
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:kDeleteAccountErrorTitle message:kConnectionErrorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlertView show];
    }];
    
    [deleteApiAccountOperation start];
    
}

-(UITableViewCell *)getCellContentView:(NSString *)CellIdentifier{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setBackgroundColor:COLOR_SLINGGIT_WHITE];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(11, 3, 38, 38)];
    [image.layer setMasksToBounds:YES];
    [image.layer setCornerRadius:3.0];
    [image setTag:CELL_IMAGE_TAG];
    [cell.contentView addSubview:image];
    
    UIImageView *apiSourceImage = [[UIImageView alloc] initWithFrame:CGRectMake(6, 32, 14, 14)];
    [apiSourceImage.layer setMasksToBounds:YES];
    [apiSourceImage setTag:CELL_API_SOURCE_IMAGE_TAG];
    [cell.contentView addSubview:apiSourceImage];
    
    UILabel *accountName = [[UILabel alloc] initWithFrame:CGRectMake(56, 0, 200, kCellHeight)];
    [accountName setBackgroundColor:[UIColor clearColor]];
    [accountName setTextAlignment:UITextAlignmentLeft];
    [accountName setTag:CELL_ACCOUNT_NAME_TAG];
    [accountName setTextColor:[UIColor darkTextColor]];
    [accountName setFont:[UIFont boldSystemFontOfSize:20]];
    [cell.contentView addSubview:accountName];
    
    return cell;
}


#pragma mark -
#pragma mark ScrollView Overrides

// Load images for all onscreen rows when scrolling is finished

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(![self.apiAccountsTableView isDragging]){
        [self loadImagesForOnscreenRows];
    }
}

#pragma mark - WebViewDelegate Methods
- (void)webViewController:(UIViewController *)controller didFinishAddingAccountWithInformation:(NSDictionary *)accountParams{
    
    if([[accountParams objectForKey:@"result_status"] isEqualToString:kJSONStatusSuccess]){
        if(kLoggingEnabled)NSLog(@"account account added successfully");
        //twitterAccountParameters = [NSDictionary dictionaryWithDictionary:twitterParams];
        //[self addTwitterAccount];
    }else if([[accountParams objectForKey:@"result_status"] isEqualToString:kJSONStatusError]){
        
        UIAlertView *authenticateTwitterAccountErrorAlertView = [[UIAlertView alloc] initWithTitle:kAddAccountErrorTitle message:[accountParams objectForKey:kJSONFriendlyError] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [authenticateTwitterAccountErrorAlertView show];
    }
    
}

#pragma mark - ActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            //twitter add account
            [self loadAddApiAccountWebviewWithType:API_ACCOUNT_TYPE_TWITTER];
            break;
        case 1:
            //facebook add account
            [self loadAddApiAccountWebviewWithType:API_ACCOUNT_TYPE_FACEBOOK];
            break;
        default:
            break;
    }
}

@end
