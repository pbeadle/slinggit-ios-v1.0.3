//
//  ApiAccountsViewController.h
//  slinggit
//
//  Created by Phil Beadle on 5/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"

@interface ApiAccountsViewController : UIViewController<WebViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIActionSheetDelegate>


@property (nonatomic, strong) UITableView *apiAccountsTableView;

@end
